- [x] Probar mombi3 en los problemas maf para saber si se implementaron bien
  - [ ] revisar el numero de `vars` por cada instancia de `m_objs`
  - [ ] revisar el `pop_size` de cada instancia de `m_objs`
- [ ] Implementar ipbi y vads (tomar la implementacion de hughes)
- [ ] Reemplazar el conjutno de  fevs de mombi por el de mss
- [ ] Evaluar mombi3 con el nuevo conjunto
- [ ] Organizar los objs que devuelve mombi para procesarlos con el hv despues











- [ ] Crear `emo_fevs.c` con los codigos de `vads`, `vads2` y `pbi`, `ws`, `te`, `asf`
- [ ] Revisar un caso de ejemplo
- [ ] Comparar la salida con python

- [ ] Revisar como adaptar este codigo

```
/* R2 ranking algorithm of the population */
void EMO_MOMBI3_r2_ranking(EMO_MOMBI3 *alg, EMO_Utility *utl, double *data, int *filter, int size) {
  int i, j, k, h, w, x, y, i1, i2, r;
  //char str[_MAXCHAR];
  double v, uant;
  int count, start, mu;

  static const EMO_UtilityFunction fdic[] = { 
                                             EMO_Utility_ewc,
                                             EMO_Utility_aasf,
                                             EMO_Utility_wpo2,
                                             EMO_Utility_wn2,
                                             EMO_Utility_asf,
                                             EMO_Utility_che,
                                             EMO_Utility_ws,
                                             NULL
                                          };
```



**luego de probar las fevs con `emo_fevs.c/py`**, debemos hacer esto:

- [ ] Tengo que pasar `z_nadir` a `EMO_Utility_ipbi_local` para que devuelva lo mismo que mi version en python.
- [ ] Tengo que pasar mi version de `vads` en `c`.









- [ ] Crear copia de `mombi3.c`
  - [ ] `mombi3a.c` para case a
  - [ ] `mombi3b.c` para case b
- [ ] Probar `mombi3a` y `mombi3b` en `emo_moea`
- [ ] Agregar la funcion `ipbi`





- [ ] Continua agregando la lista de funciones fevs en `mombi3a` y `mombi3b`.