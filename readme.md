# About this repository

This repository contains a copy of `emo_project` developed by Raquel Hernández.



## Compilation

```
cd emo_project
make clean; make
```



## Demo

```
cd emo_project/demo
mkdir output
./emo_moea MOMBI3 input/Param_02D.cfg zdt1 1    # run zdt1 m_objs=2

# other options
./emo_moea MOMBI3 input/Param_03D.cfg dtlz1 1
./emo_moea MOMBI3 input/Param_03D.cfg maf1 1
./emo_moea MOMBI3 input/Param_03D.cfg maf2 1
./emo_moea MOMBI3 input/Param_03D.cfg maf3 1
./emo_moea MOMBI3 input/Param_03D.cfg maf4 1
./emo_moea MOMBI3 input/Param_03D.cfg maf5 1
./emo_moea MOMBI3 input/Param_03D.cfg inv_dtlz1 1
```

Output:

```
log tmp MOMBI3_zdt1_0.log
Random inicialization of population.
mu 100, var 30, obj 2
Codificacion real
```

These files are stored in `output` after execution:

```
MOMBI3_ZDT1_02D_R01.pos      Pareto set (2 objectives, run 1)
MOMBI3_ZDT1_02D_R01.pof      Pareto front (2 objectives, run 1)

MOMBI3_ZDT1_02D.log          Log information
MOMBI3_ZDT1_02D.sum          Summary (number of runs, function evaluations, spent time, etc.)
```

Plot output:

```
cd emo_project/demo
python3 plot_pops.py -i output/MOMBI3_ZDT1_02D_R01.pof
python3 plot_pops.py -i output/MOMBI3_DTLZ1_03D_R01.pof
```

This is the output:

![](demo/figure_mombi3.png)

You can also use these commands to evaluate more algorithms:

```
./emo_moea SMS_EMOA input/Param_02D.cfg zdt1 1
./emo_moea NSGA2 input/Param_02D.cfg zdt1 1
./emo_moea NSGA3 input/Param_02D.cfg zdt1 1
./emo_moea MOMBI2 input/Param_02D.cfg zdt1 1
./emo_moea MOMBI3 input/Param_02D.cfg zdt1 1
./emo_moea HV_IBEA input/Param_02D.cfg zdt1 1
./emo_moea EPS_IBEA input/Param_02D.cfg zdt1 1
./emo_moea R2_IBEA input/Param_02D.cfg zdt1 1
./emo_moea MOEAD input/Param_02D.cfg zdt1 1
./emo_moea SPEA2 input/Param_02D.cfg zdt1 1
./emo_moea HYPE input/Param_02D.cfg zdt1 1
./emo_moea MOVAP input/Param_02D.cfg zdt1 1
```


