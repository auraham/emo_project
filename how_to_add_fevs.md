# How to add fevs

Check `utility.c`. 



Your utility (ie scalarizing) function must follow this header:

```c
double EMO_Utility_name(EMO_Utility *u, double *w, double *x);
```

For instance, consider the code of the weighted sum:

```c
// Gass and Saaty (1995), Zadeh (1963)
// Miettinen99, p 78
// wi >= 0, sum wi = 1
// x = fi-z* 
double EMO_Utility_ws(EMO_Utility *u, double *w, double *x) {
  double t = 0;
  int i;

  for(i = u->nobj - 1; i > -1; i--)
    t += w[i] * x[i];
  return t;
}
```



In this example, we show how to add two new functions: `vads` and `ipbi`:



1. Add the implementation of the function at the end of `utility.c`:

```c
// Hughes
double EMO_Utility_vads(EMO_Utility *u, double *w, double *x) {
  double t = 0;
  int i;

  ...
  return t;
}
```

```c
// Sato
double EMO_Utility_ipbi(EMO_Utility *u, double *w, double *x) {
  double t = 0;
  int i;

  ...
  return t;
}
```

2. Add the headers of the new functions at the end of `utility.h`:

```c
double EMO_Utility_vads(EMO_Utility *u, double *w, double *x);
double EMO_Utility_ipbi(EMO_Utility *u, double *w, double *x);
```

