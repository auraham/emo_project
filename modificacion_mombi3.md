# Modificacion de MOMBI-III

**Note** Ultima revision antes de estos cambios: `e8810a9`.

**Note** La revisión con `mombi3a` es `ebd1131`.

**Note** La versión con `mombi3b` es `8b985b0`.

**Nota** La rev `71cf068` contiene a `mombi3c` con `ipbi`.



Se modificarán los siguientes archivos:

```
src
	Makefile			Para agregar mombi3a a la compilacion
	moea.c   			Para agregar mombi3a a la lista de moeas
	moea.h 				Para agregar mombi3a a la union alg
	mombi3a.c/h   		Copia de mombi3.c/h
	
include
	emo.h				Para agregar struct MOMBI3A y para agregar mombi3a a la union alg
```



## Procedimiento

1. Agregamos una copia de `mombi3.c` y `mombi3.h` llamada `mombi3a.c` y `mombi3a.h`, respectivamente.

```
(dev) auraham@rocket:~/git/emo_project/src$ cp mombi3.c mombi3a.c
(dev) auraham@rocket:~/git/emo_project/src$ cp mombi3.h mombi3a.h
```

2. Luego, actualizamos `mombi3a.c` para reemplazar las ocurrencias de `EMO_MOMBI3` por `EMO_MOMBI3A`. Hacemos lo mismo en `mombi3a.h`. Agrega `#include "mombi3.h"` en `mombi3a.c`.

```c
// mombi3a.c

#include "mombi3a.h"     // reemplaza "mombi3.h" por "mombi3a.h"

// reemplaza 
// EMO_MOMBI3_      por  EMO_MOMBI3A_
// EMO_MOMBI3 *alg  por  EMO_MOMBI3A *alg
void EMO_MOMBI3A_load_param(EMO_MOMBI3 *alg, ...) { ... }
```

```c
// mombi3a.h

// actualiza este bloque
#ifndef _MOMBI3A_
#define _MOMBI3A_

// actualiza esta struct
typedef struct {
} EMO_MOMBI3A;

// renombra las funciones y el primer argumento
void EMO_MOMBI3A_alloc(EMO_MOMBI3A *alg, EMO_Param *param, EMO_Population *pop, EMO_MOP *mop);
void EMO_MOMBI3A_free(EMO_MOMBI3A *alg);
void EMO_MOMBI3A_run(EMO_MOMBI3A *alg, EMO_Param *param, EMO_Population *pop, EMO_MOP *mop);
void EMO_MOMBI3A_prun(EMO_MOMBI3A *alg, EMO_Param *param, EMO_Population *pop, EMO_MOP *mop);
```

3. Actualizamos `moea.c` para agregar `mombi3.c` en la lista de moeas:

```c
// moea.c

const char *EMO_MOEA_list[] = { "SMS_EMOA",
                                "NSGA2",
                                "NSGA3",
                                "MOMBI2",
                                "MOMBI3",
                                
                                "HV_IBEA",
                                "EPS_IBEA",
                                "R2_IBEA",
                                "MOEAD",
                                "SPEA2",
                                "HYPE",
                                "MOVAP",
                                
                                "MOMBI3A",		// nuevo, agrega el nombre del moea aqui
                                 NULL
                              };
```

4. Agregar a las siguientes listas al final

```c
void EMO_MOEA_alloc(EMO_MOEA *moea, EMO_Param *param, EMO_Population *pop, EMO_MOP *mop, const char *str) {


const EMO_MOEA_falloc a[] = { (EMO_MOEA_falloc) EMO_SMSEMOA_alloc, 
                                (EMO_MOEA_falloc) EMO_NSGA2_alloc,
                                (EMO_MOEA_falloc) EMO_NSGA3_alloc,
                                (EMO_MOEA_falloc) EMO_MOMBI2_alloc,
                                (EMO_MOEA_falloc) EMO_MOMBI3_alloc,
                                (EMO_MOEA_falloc) EMO_IBEA_alloc,
                                (EMO_MOEA_falloc) EMO_IBEA_alloc,
                                (EMO_MOEA_falloc) EMO_IBEA_alloc,
                                (EMO_MOEA_falloc) EMO_MOEAD_alloc,
                                (EMO_MOEA_falloc) EMO_SPEA2_alloc,
                                (EMO_MOEA_falloc) EMO_HYPE_alloc,
                                (EMO_MOEA_falloc) EMO_MOVAP_alloc,
                                
                                (EMO_MOEA_falloc) EMO_MOMBI3A_alloc   // nuevo
                              };

  const EMO_MOEA_ffree f[]  = { (EMO_MOEA_ffree) EMO_SMSEMOA_free, 
                                (EMO_MOEA_ffree) EMO_NSGA2_free,
                                (EMO_MOEA_ffree) EMO_NSGA3_free,
                                (EMO_MOEA_ffree) EMO_MOMBI2_free,
                                (EMO_MOEA_ffree) EMO_MOMBI3_free,
                                (EMO_MOEA_ffree) EMO_IBEA_free,
                                (EMO_MOEA_ffree) EMO_IBEA_free,
                                (EMO_MOEA_ffree) EMO_IBEA_free,
                                (EMO_MOEA_ffree) EMO_MOEAD_free,
                                (EMO_MOEA_ffree) EMO_SPEA2_free,
                                (EMO_MOEA_ffree) EMO_HYPE_free,
                                (EMO_MOEA_ffree) EMO_MOVAP_free,
                                (EMO_MOEA_ffree) EMO_MOMBI3A_free,   // nuevo
                              };

  const EMO_MOEA_frun r[]   = { (EMO_MOEA_frun) EMO_SMSEMOA_run, 
                                (EMO_MOEA_frun) EMO_NSGA2_run,
                                (EMO_MOEA_frun) EMO_NSGA3_run,
                                (EMO_MOEA_frun) EMO_MOMBI2_run,
                                (EMO_MOEA_frun) EMO_MOMBI3_run,
                                (EMO_MOEA_frun) EMO_IBEA_run,
                                (EMO_MOEA_frun) EMO_IBEA_run,
                                (EMO_MOEA_frun) EMO_IBEA_run,
                                (EMO_MOEA_frun) EMO_MOEAD_run,
                                (EMO_MOEA_frun) EMO_SPEA2_run,
                                (EMO_MOEA_frun) EMO_HYPE_run,
                                (EMO_MOEA_frun) EMO_MOVAP_run,
                                (EMO_MOEA_frun) EMO_MOMBI3A_run,    // nuevo
                              };

```


5. Agregar al `case`

```c

void EMO_MOEA_alloc(EMO_MOEA *moea, EMO_Param *param, EMO_Population *pop, EMO_MOP *mop, const char *str) {

switch(i) {
    case 0:  moea->palg = &moea->alg.smsemoa; 
             break;
    case 1:  moea->palg = &moea->alg.nsga2; 
             break;
    case 2:  moea->palg = &moea->alg.nsga3; 
             break;
    case 3:  moea->palg = &moea->alg.mombi2; 
             break;
    case 4:  moea->palg = &moea->alg.mombi3; 
             break;
    case 5:
    case 6:
    case 7:  moea->palg = &moea->alg.ibea; 
             break;
    case 8:  moea->palg = &moea->alg.moead; 
             break;
    case 9:  moea->palg = &moea->alg.spea2; 
             break;
    case 10: moea->palg = &moea->alg.hype; 
             break;
    case 11: moea->palg = &moea->alg.movap; 
             break;
    // nuevo
    // asegurate de que el index 12 corresponda a la posicion de MOMBI3A en las listas
    // anteriores: EMO_MOEA_falloc, EMO_MOEA_ffree, EMO_MOEA_frun
    case 12:  moea->palg = &moea->alg.mombi3a; 
             break;
        
    default: printf("Error, unknown MOEA (2) %s.\n", aux);
             free(aux);
             exit(1);
  }
```

**Nota** `alg.mombi3a` se agregará más adelante en los archivos `moea.h` y `emo.h`.


6. Copiar `struct` y funciones de `MOMBI3` en `include/emo.h` (aunque no entiendo por que estan duplicadas si ya se encuentran en `mombi3a.h`):

```c
// emo.h

typedef struct {
  // ...
} EMO_MOMBI3A;   // nuevo

// modificar nombres de funcion y del primer argumento
void EMO_MOMBI3A_alloc(EMO_MOMBI3A *alg, EMO_Param *param, EMO_Population *pop, EMO_MOP *mop);
void EMO_MOMBI3A_free(EMO_MOMBI3A *alg);
void EMO_MOMBI3A_run(EMO_MOMBI3A *alg, EMO_Param *param, EMO_Population *pop, EMO_MOP *mop);
void EMO_MOMBI3A_prun(EMO_MOMBI3A *alg, EMO_Param *param, EMO_Population *pop, EMO_MOP *mop);
```

7. Agregar al generic moea al final de `emo.h`:

```c
// emo.h

/* Al final, Generic MOEA */
typedef void (*EMO_MOEA_falloc)(void *, EMO_Param *, EMO_Population *, EMO_MOP *);
typedef void (*EMO_MOEA_ffree)(void *);
typedef void (*EMO_MOEA_frun)(void *, EMO_Param *, EMO_Population *, EMO_MOP *);

typedef struct {
  EMO_MOEA_falloc alloc;
  EMO_MOEA_ffree free;
  EMO_MOEA_frun run;

  union {
    EMO_SMSEMOA smsemoa;
    EMO_NSGA2 nsga2;
    EMO_NSGA3 nsga3;
    EMO_MOMBI2 mombi2;
    EMO_MOMBI3 mombi3;
    EMO_IBEA ibea;
    EMO_MOEAD moead;
    EMO_SPEA2 spea2;
    EMO_HYPE hype;
    EMO_MOVAP movap;
      
    // nuevo
    EMO_MOMBI3A mombi3a;
  } alg;

  void *palg;
} EMO_MOEA;
```

8. Agregar `mombi3a` en la sección de `#include` y en la `union` de `moea.h`: 

```c
// moea.h

// nuevo
// agregar esta linea
#include "mombi3a.h"

union {
    EMO_SMSEMOA smsemoa;
    EMO_NSGA2 nsga2;
    EMO_NSGA3 nsga3;
    EMO_MOMBI2 mombi2;
    EMO_MOMBI3 mombi3;
    EMO_IBEA ibea;
    EMO_MOEAD moead;
    EMO_SPEA2 spea2;
    EMO_HYPE hype;
    EMO_MOVAP movap;
    
    // nuevo
    EMO_MOMBI3A mombi3a;
  } alg;
```

9. Por ultimo, agregar a makefile de `src/Makefile`:

```makefile
# src/Makefile
OBJS_BASE	= ... mombi3a.o ...


mombi3a.o:		mombi3a.h mombi3a.c
			$(CC) $(CFLAGS) -c mombi3a.c
```

Compila el proyecto:

```
(dev) auraham@rocket:~/git/emo_project$ make clean; make
```

y prueba el demo:

```
# base
(dev) auraham@rocket:~/git/emo_project/demo$ ./emo_moea MOMBI3 input/Param_03D.cfg dtlz3 1
# nuevo
(dev) auraham@rocket:~/git/emo_project/demo$ ./emo_moea MOMBI3A input/Param_03D.cfg dtlz3 1
```

```
# base
(dev) auraham@rocket:~/git/emo_project/demo$ python3 plot_pops.py -i output/MOMBI3_DTLZ3_03D_R01.pof -p dtlz3

# nuevo
(dev) auraham@rocket:~/git/emo_project/demo$ python3 plot_pops.py -i output/MOMBI3A_DTLZ3_03D_R01.pof -p dtlz3
```

Ambos comandos de graficación deben devolver esto:

![](demo/figure_mombi3_dtlz3.png)

**Nota** Repite el procedimiento para `mombi3a.c` y `mombi3b.c`, hay que actualizar los `#include`.


# Implementacion de fevs

Compara las implementaciones de las fevs como sigue:

```
(dev) auraham@rocket:~/git/emo_project/demo$ ./emo_fevs 
```

```
ws: 1.00004500
te: 1.00000000
asf: 90000.00000000
d1: 1.00004500, d2: 1.68816765
pbi: 9.44088323
d1: 1.00004500, d2: 1.68816765
ipbi: 7.44079323
vads: 1962.14168703
vads2: 1962.14168278
vads_hughes: 8836883836402500.00000000
```



```
%run emo_fevs.py
```

```
ws: 1.00004500
te: 1.00000000
asf: 90000.00000000
pbi: 9.44088323, d1: 1.00004500, d2: 1.68816765
ipbi: 8.44092651, d1: 0.00004500, d2: 1.68819430
vads: 8836883836402500.00000000
```



-----

**Nota** en la rev `3cdf565`, la implementacion de `ipbi` difieren.

**Nota** en la rev `e0688b6` la implementacion de `pbi` e `ipbi` en `c`  y `python` es la misma.

-----



**Nota** en la rev `1b07eef` algunas fevs difieren en el segundo caso de ejemplo:

```
(dev) auraham@rocket:~/git/emo_project/demo$ ./emo_fevs 
ws: 0.54619100
*te: 0.13613201
*asf: 62.68475211
d1: 1.22259997, d2: 1.16415175, norm_w: 0.36495257, norm_num: 0.44619100
python_pbi: 7.04335875
d1: 1.79149035, d2: 1.35667325, norm_w: 0.36495257, norm_num: 0.65380901
python_ipbi: 4.99187590
*python_vads: 8836883836402713.00000000
```

```
In [1]: %run emo_fevs.py                    
ws: 0.54619100
*te: 0.12251881
*asf: 53.72978753
pbi: 7.04335875, d1: 1.22259997, d2: 1.16415175
ipbi: 4.99187590, d1: 1.79149035, d2: 1.35667325
*vads: 1134748212995.55590820
```



**Nota** la rev `5d4f095` devuelve lo mismo en ambos scripts.



## Manejo de fevs (`MOMBI3A`, `MOMBI3B`)

Modificamos esta funcion para incluir los vectores ideal y de nadir:

```c
// mombi3.c

/* R2 ranking algorithm of the population */
void EMO_MOMBI3_r2_ranking(EMO_MOMBI3 *alg, EMO_Utility *utl, double *data, int *filter, int size)
```

Modificamos la lista de fevs:

```c
void EMO_MOMBI3_r2_ranking(EMO_MOMBI3 *alg, EMO_Utility *utl, double *data, int *filter, int size) {


static const EMO_UtilityFunction fdic[] = { 
                                             EMO_Utility_ewc,
                                             EMO_Utility_aasf,
                                             EMO_Utility_wpo2,
                                             EMO_Utility_wn2,
                                             EMO_Utility_asf,
                                             EMO_Utility_che,
                                             EMO_Utility_ws,
                                             NULL
                                          };
```

Modificamos el loop para llamar a la funcion ipbi a parte

```c
void EMO_MOMBI3_r2_ranking(EMO_MOMBI3 *alg, EMO_Utility *utl, double *data, int *filter, int size) {
  
  h = 0;
  while(fdic[h] != NULL) {

    for(i = utl->nobj; i < alg->wsize; i++) {
      mu = 0;

      for(j = 0; j < size; j++) {
        if(filter == NULL || filter[j] == 1) {
          alg->sort[mu][0] = (double) j;
          alg->sort[mu][1] = fdic[h](utl, alg->W + i * utl->nobj, data + j * utl->nobj);
          mu++;
        }
      }
```



## Manejo de fevs (`MOMBI3C`)

A diferencia de las versiones `mombi3a` y `mombi3b`, `mombi3c` conserva las fevs de `mombi3` y agrega `ipbi`.

Modificamos esta funcion para incluir los vectores ideal y de nadir:

```c
// mombi3c.c

/* R2 ranking algorithm of the population */
void EMO_MOMBI3_r2_ranking(EMO_MOMBI3 *alg, EMO_Utility *utl, double *data, int *filter, int size)
```

Modificamos la lista de fevs, en este caso, solo agregamos una fev como placeholder de `ipbi`, ya que `EMO_Utility_python_ipbi` no tiene el mismo tipo de `EMO_UtilityFunction`:

```c
void EMO_MOMBI3_r2_ranking(EMO_MOMBI3 *alg, EMO_Utility *utl, double *data, int *filter, int size) {


static const EMO_UtilityFunction fdic[] = { 
                                             EMO_Utility_ewc,   // 0
                                             EMO_Utility_aasf,  // 1
                                             EMO_Utility_wpo2,  // 2
                                             EMO_Utility_wn2,   // 3
                                             EMO_Utility_asf,   // 4
                                             EMO_Utility_che,   // 5
                                             EMO_Utility_ws,    // 6
                                             // no podemos agregar ipbi aqui porque no tiene 
                                             // el mismo tipo que EMO_UtilityFunction
                                             // en su lugar, colocamos una copia de 
                                             // EMO_Utility_ws y agregamos un caso en el bloque
                                             // if(filter == NULL || filter[j] == 1)
                                             EMO_Utility_ws,    // 7 (placeholder)
                                             NULL
                                          };
```

Modificamos el loop para llamar a la funcion `ipbi` a parte

```c
void EMO_MOMBI3_r2_ranking(EMO_MOMBI3 *alg, EMO_Utility *utl, double *data, int *filter, int size) {
  
  h = 0;
  while(fdic[h] != NULL) {

    for(i = utl->nobj; i < alg->wsize; i++) {
      mu = 0;

      for(j = 0; j < size; j++) {
        if(filter == NULL || filter[j] == 1) {
          alg->sort[mu][0] = (double) j;
          alg->sort[mu][1] = fdic[h](utl, alg->W + i * utl->nobj, data + j * utl->nobj);
          mu++;
        }
      }
```

debe quedar asi:

```c
h = 0;
while(fdic[h] != NULL) {

    for(i = utl->nobj; i < alg->wsize; i++) {
        mu = 0;

        for(j = 0; j < size; j++) {
            if(filter == NULL || filter[j] == 1) {
                
                // evaluacion de las fevs de mombi3
                alg->sort[mu][0] = (double) j;
                alg->sort[mu][1] = fdic[h](utl, alg->W + i * utl->nobj, data + j * utl->nobj);
                
                // caso especial
                if (h == 7) {
                	alg->sort[mu][1] = EMO_Utility_python_ipbi(alg->W + i * utl->nobj, // double *w, 
                                                          data + j * utl->nobj,   // double *x, 
                                                          utl->nobj,              // int nobj, 
                                                          alg->min,               // double *z_ideal
                                                          alg->max                // double *z_nadir
                                                          );  
                }
                
                // siguiente individuo
                mu++;
            }
        }
    }
    
    ...
    
    h++;
}

```





# Evaluacion de MOMBI-III A

**Nota** la rev `ab4fcbc` de `mombi3a.c` emplea las mismas fevs que `mss`.

```
# evaluacion
(dev) auraham@rocket:~/git/emo_project/demo$ ./emo_moea MOMBI3A input/Param_03D.cfg dtlz3 1
(dev) auraham@rocket:~/git/emo_project/demo$ python3 plot_pops.py -i output/MOMBI3A_DTLZ3_03D_R01.pof -p dtlz3
```

![](demo/figure_mombi3a_fevs.png)

```
(dev) auraham@rocket:~/git/emo_project/demo/output$ cat MOMBI3A_DTLZ3_03D_R01.pof
# 106 3
3.533635e+00 7.400491e-04 7.897667e-03 
3.600050e+00 1.121188e-02 1.175438e-03 
3.197467e+00 1.169427e-02 7.402786e-05 
3.370800e+00 9.653297e-04 8.797410e-05 
4.589120e+00 1.000248e-02 7.161109e-03 
3.255183e+00 6.740306e-03 5.942514e-02 
3.651722e+00 2.792313e-02 2.392456e-02 
3.325833e+00 2.238326e-03 2.366858e-03 
3.669163e+00 1.354400e-02 5.633855e-03 
3.569081e+00 1.626731e-03 5.391341e-02 
5.074790e+02 1.331387e+00 4.421028e+02 
2.084324e+03 9.536500e-03 1.162492e-02 
3.619230e+00 1.472220e-02 1.091299e-03 
2.367500e+02 3.735164e+02 2.171331e+02 
3.453164e+00 1.533171e-04 3.104348e-03 
4.146451e+00 4.470771e-03 1.012967e-02 
3.160637e+00 9.875016e-03 2.918846e-04 
3.755887e+00 4.566510e-03 8.025440e-03 
3.230765e+00 1.902657e-02 5.030998e-03 
3.372720e+00 6.283380e-03 7.342227e-03 
4.505899e+00 4.142305e-03 1.542337e-05 
3.184273e+00 6.601032e-03 1.218911e-02 
4.983498e-07 6.967832e-09 4.027363e+00 
3.593236e+00 4.825643e-03 9.250394e-04 
3.625561e+00 3.373246e-03 1.769604e-02 
7.842817e-02 5.519339e-06 1.854670e+01 
2.044091e+01 4.836297e+02 3.537900e+00 
6.350240e-05 6.095894e+00 9.104464e-10 
3.263118e+00 4.405608e-03 3.180943e-03 
3.250731e+00 2.678473e-03 1.041313e-03 
3.400080e+00 2.675873e-03 5.220654e-03 
3.251676e+00 6.687700e-03 2.180631e-02 
3.304056e+00 2.818142e-03 1.203858e-02 
3.532614e+00 1.231924e-02 5.544423e-03 
3.623465e+00 9.123587e-02 1.871948e-02 
3.570599e+00 1.627423e-03 3.530141e-03 
1.480264e-02 4.357465e+00 4.870134e-06 
3.189338e+00 6.603965e-03 1.150879e-02 
2.084668e+03 9.538076e-03 3.209985e-02 
3.542582e+00 2.128311e-02 4.503271e-05 
3.285420e+00 4.534444e-02 3.061875e-02 
3.200436e+00 5.563724e-03 4.383810e-03 
3.601998e+00 7.502666e-03 8.367371e-03 
3.390405e+00 6.346073e-03 2.176062e-04 
4.581014e+00 2.067604e-04 7.148443e-03 
7.383132e-02 2.173378e+01 2.429083e-05 
3.608690e+00 1.343054e-02 2.260203e-03 
3.474353e+00 1.608020e-04 1.072349e-03 
3.799700e+00 6.216556e-04 2.359303e-03 
3.150837e+00 1.183255e-02 4.935125e-02 
3.673796e+00 5.509995e-02 5.590541e-02 
3.234322e+00 3.571413e-02 5.928375e-03 
3.149388e+00 1.991734e-02 5.037684e-03 
5.613821e+00 2.462854e+02 3.576244e+02 
3.344556e+00 3.694648e-02 1.346241e-02 
3.598840e+00 2.749703e-03 1.919009e-02 
3.588304e+00 1.438955e-02 1.756605e-02 
3.887963e+00 4.525117e-03 1.926361e-03 
3.685372e+00 2.789428e-03 3.275652e-02 
3.670244e+00 5.312602e-02 2.542716e-02 
3.140536e+00 2.160864e-03 9.656867e-06 
3.747105e+00 7.353556e-05 3.461167e-03 
3.406670e+00 1.354667e-02 2.263725e-02 
3.236838e+00 6.678578e-03 2.947701e-03 
3.162335e+00 7.719645e-04 2.883153e-03 
3.355108e+00 1.558961e-03 1.222459e-02 
5.232044e+02 1.372643e+00 4.558024e+02 
3.230255e+00 2.651838e-03 2.941701e-03 
3.265935e+00 4.725982e-02 3.043744e-02 
3.322989e+00 7.871443e-02 5.316756e-03 
3.523638e+00 3.696106e-03 6.967282e-04 
3.134915e+00 3.461645e-02 4.899242e-02 
3.137297e+00 1.178171e-02 4.810758e-02 
3.221983e+00 5.892470e-04 2.957359e-02 
3.201645e+00 1.876505e-02 5.014756e-02 
3.582684e+00 7.592958e-04 5.501032e-03 
3.374831e+00 5.010628e-02 4.279081e-04 
3.206159e+00 1.619362e-05 1.148402e-02 
3.128128e+00 6.643345e-03 1.919238e-03 
3.608194e+00 1.389257e-03 1.139649e-05 
3.336027e+00 1.429551e-02 1.215518e-02 
3.218517e+00 3.421733e-03 4.408572e-03 
3.608837e+00 2.279388e-02 2.260324e-03 
3.139916e+00 1.960795e-02 1.140140e-02 
3.220574e+00 3.607272e-02 2.936435e-03 
3.546495e+00 7.427425e-04 2.347135e-02 
1.654135e+02 4.355534e+02 1.847374e+02 
3.650829e+00 8.544998e-02 2.392456e-02 
3.572890e+00 6.552991e-03 1.038504e-02 
3.868025e+00 1.181451e-03 1.409618e-03 
3.207515e+00 1.103516e-02 1.266896e-02 
3.699852e+00 6.597386e-05 8.335859e-05 
2.673089e+02 4.549916e+02 2.196729e+02 
3.303897e+00 1.813289e-03 9.006495e-03 
3.433207e+00 1.220033e-02 7.723764e-03 
3.515550e+00 2.098127e-02 5.046459e-02 
3.749118e+00 2.303438e-02 1.835349e-02 
7.640084e+00 1.968696e-07 2.572345e-10 
3.461648e+00 1.082242e-02 1.137878e-02 
3.495743e+00 2.230538e-02 7.987391e-03 
3.376042e+00 1.259040e-02 1.225865e-02 
3.513298e+00 3.822753e-02 2.200571e-03 
3.636518e+00 4.893193e-03 2.718391e-05 
3.284284e+00 5.256433e-03 5.115606e-02 
3.461164e+00 7.209320e-03 1.155700e-03 
5.432569e-07 6.431342e-11 6.644379e+00
```

Puedes usar el bloque anterior como salida de referencia (rev `cdae6a8: Se agrego salida de referencia de mombi3a.c`).



-----

**Nota** la rev `094f872`  contiene dos versiones de mombi3

 - `mombi3a.c`: usa `ws`, `te`, `asf`, `pbi`, `ipbi`, `vads`
 - `mombi3b.c`: usa `ws`, `te`, `asf`, `pbi`,  `vads`

-----



### Traslado de fevs

Las fevs se movieron de `mombi3a.c` a `utility.c` en la rev `be28d8a`. La salida del bloque anterior es la misma.






### Pruebas

----

**Nota** usa los archivos `Param_*_paper.cfg` para usar un budget similar al de mss.

----

```
./emo_moea MOMBI3A input/Param_03D_paper.cfg dtlz3 1
./emo_moea MOMBI3A input/Param_05D_paper.cfg dtlz3 1
./emo_moea MOMBI3A input/Param_10D_paper.cfg dtlz3 1

```



- [x] Crear scripts para realizar la experimentacion en `alux2` y en `deepsea`

  - [ ] Hacer solo 20 pruebas por cada mop

  - [ ] Hacer un script por cada mop para dividir las pruebas entre los servidores

    ​			

```
# alux2: dtlz3 dtlz1

./emo_moea MOMBI3A input/Param_03D_paper.cfg $mop_name 20
./emo_moea MOMBI3A input/Param_05D_paper.cfg $mop_name 20
./emo_moea MOMBI3A input/Param_10D_paper.cfg $mop_name 20

donde $mop_name es el parametro del script
```





## Evaluacion de MOMBI3 C

**Nota** La rev `71cf068` contiene a `mombi3c` con `ipbi`.

```
# evaluacion
(dev) auraham@rocket:~/git/emo_project/demo$ ./emo_moea MOMBI3C input/Param_03D.cfg dtlz3 1
(dev) auraham@rocket:~/git/emo_project/demo$ python3 plot_pops.py -i output/MOMBI3C_DTLZ3_03D_R01.pof -p dtlz3
```

Esta es la salida:

![](demo/figure_mombi3c_ipbi_dtlz3.png)





```
(dev) auraham@rocket:~/git/emo_project/demo$ cat output/MOMBI3C_DTLZ3_03D_R01.pof
# 106 3
4.914415e+01 2.173499e+03 7.437467e-05 
1.001653e-04 6.791310e+00 4.464987e-03 
7.511905e-06 7.435058e+00 5.892013e-16 
2.878703e-22 1.160746e-15 1.895643e+01 
1.392407e-04 7.448435e+00 5.902614e-16 
4.799616e+00 9.656637e-02 4.292972e+00 
7.000559e+02 1.066641e+02 5.788361e+02 
8.041499e-08 6.782910e+00 3.118064e-01 
3.093432e+02 1.758875e+03 8.910716e+02 
6.019803e+00 1.248139e-03 4.466350e+00 
1.009995e+00 7.263676e+00 3.455809e-12 
8.003373e+00 8.430562e-11 3.771448e-12 
1.148420e-15 1.875512e+01 1.486275e-15 
1.948176e-02 2.190799e+03 3.613665e+01 
1.985410e+01 2.191871e+03 7.192288e-05 
2.758449e-02 6.600860e+00 4.339813e-03 
4.691548e-16 7.661879e+00 2.159914e-07 
3.093432e+02 1.758875e+03 8.910716e+02 
9.132605e-02 2.194336e+03 7.506851e-05 
1.009995e+00 7.263676e+00 3.455809e-12 
3.834329e-03 2.188601e+03 3.594266e+01 
3.894468e-06 7.524775e+00 2.121264e-07 
1.385489e-04 7.411428e+00 5.873287e-16 
4.102515e-03 2.192074e+03 3.419707e+01 
8.395636e-05 7.404608e+00 6.126565e-05 
1.181536e-04 6.320417e+00 3.962763e+00 
1.958026e+02 1.775104e+03 8.910716e+02 
3.834329e-03 2.188601e+03 3.594266e+01 
5.528615e+01 2.438129e-01 3.278273e-11 
3.793118e-01 7.297775e+00 5.791027e-16 
2.324456e-05 7.389284e+00 3.815695e-04 
6.273363e-14 8.212523e-12 5.453091e+01 
4.676004e-16 7.636494e+00 6.051644e-16 
3.834329e-03 2.188601e+03 3.594266e+01 
1.385489e-04 7.411428e+00 5.873287e-16 
6.994620e+02 1.104144e+02 3.644694e+02 
7.645272e+00 5.462575e-03 4.466795e-07 
2.791649e-02 2.188015e+03 6.036447e+01 
8.261641e+00 8.702615e-11 3.893152e-12 
2.026742e-08 9.421598e-06 7.501625e+00 
4.102498e-03 2.192065e+03 7.499081e-05 
9.004529e+02 1.323793e+02 2.038212e+02 
2.896872e-06 7.405516e+00 6.127317e-05 
2.410234e-06 4.609029e+00 4.541145e+00 
1.502647e-05 4.903778e+00 4.228216e+00 
1.517897e-06 7.371908e+00 3.806722e-04 
5.547460e+01 5.843562e-10 2.614142e-11 
6.473635e-05 8.199011e-06 7.456990e+00 
3.455983e-07 6.621364e+00 3.513135e-03 
4.565031e-22 1.840705e-15 3.006100e+01 
2.791649e-02 2.188015e+03 6.036447e+01 
4.099131e-03 2.190266e+03 7.492926e-05 
2.993772e-15 1.056836e+01 8.375037e-16 
8.395636e-05 7.404608e+00 6.126565e-05 
5.747359e+01 2.183508e+03 7.472395e-05 
7.000559e+02 1.066641e+02 5.788361e+02 
4.133995e+00 5.060099e-01 3.450436e+00 
1.181536e-04 6.320417e+00 3.962763e+00 
7.700859e+00 1.596687e-03 4.499271e-07 
9.004529e+02 1.323793e+02 2.038212e+02 
2.804006e+02 1.594312e+03 8.387991e+02 
1.258391e-03 4.606431e+00 4.538586e+00 
1.943528e-02 2.195920e+03 5.891398e-01 
5.726087e+01 2.175426e+03 7.444738e-05 
6.502947e-16 1.062012e+01 8.416057e-16 
5.552897e+01 2.448837e-01 3.292672e-11 
7.704338e+00 8.872115e-04 2.726269e-06 
6.994620e+02 1.104144e+02 3.644694e+02 
7.576569e-01 6.620489e+00 4.381090e-03 
1.028500e-04 6.973334e+00 2.502436e-04 
4.172108e-05 7.477098e+00 7.355727e-08 
8.723428e-08 7.358109e+00 3.382479e-01 
5.185604e-05 7.404608e+00 6.126565e-05 
7.681086e+00 8.845339e-04 4.487718e-07 
1.945446e-02 2.198087e+03 5.897211e-01 
2.791649e-02 2.188015e+03 6.036447e+01 
7.981048e+02 1.126314e+02 4.148531e+02 
5.104951e-04 9.299671e-02 5.428901e+01 
1.988491e+01 2.195274e+03 7.203453e-05 
2.055063e-06 3.998928e-05 7.294114e+00 
9.132546e-02 2.194322e+03 1.036550e-05 
5.478798e-22 2.209153e-15 3.607820e+01 
2.905313e+02 1.651914e+03 8.368836e+02 
6.536934e-16 8.102052e-12 5.379581e+01 
9.132605e-02 2.194336e+03 7.506851e-05 
5.115841e+00 6.261900e-01 4.269933e+00 
3.512489e+02 1.751080e+03 8.911166e+02 
7.316014e+00 7.340380e-02 3.447715e-12 
3.864625e-01 7.358264e+00 5.839193e-16 
5.473679e+00 3.910964e-03 5.203918e+00 
2.017778e-06 3.760498e-03 7.421671e+00 
6.444720e+02 1.258066e+02 6.128160e+02 
5.473681e+00 3.910965e-03 5.203920e+00 
1.014622e+00 7.296952e+00 3.471641e-12 
3.885307e-06 7.507074e+00 1.246080e-06 
8.572259e+02 1.673380e+02 1.233432e+01 
9.399982e+01 1.661728e+03 8.698278e+02 
2.510219e-06 4.800227e+00 4.292649e+00 
8.663480e-06 4.599498e+00 4.531755e+00 
4.090148e-03 2.185466e+03 7.476505e-05 
5.484311e+00 3.918560e-03 5.214026e+00 
2.276833e-08 1.058418e-05 7.460547e+00 
1.841062e-02 7.344226e+00 5.820050e-16 
9.399982e+01 1.661728e+03 8.698278e+02 
4.612277e-16 7.532421e+00 7.912611e-03 
7.560628e+00 5.402097e-03 4.417342e-07 
```









## Pruebas en servidores

**alux2**

```
ssh alux2
cd git
git clone git@gitlab.com:auraham/emo_project.git
cd emo_project
make clean; make
cd demo
mkdir log

rm output/*
rm log/*

nohup ./emo_moea MOMBI3A input/Param_03D_paper.cfg dtlz1 50 > log/log_dtlz1_m_3.log &
nohup ./emo_moea MOMBI3A input/Param_05D_paper.cfg dtlz1 50 > log/log_dtlz1_m_5.log &
nohup ./emo_moea MOMBI3A input/Param_10D_paper.cfg dtlz1 50 > log/log_dtlz1_m_10.log &

nohup ./emo_moea MOMBI3A input/Param_03D_paper.cfg dtlz3 50 > log/log_dtlz3_m_3.log &
nohup ./emo_moea MOMBI3A input/Param_05D_paper.cfg dtlz3 50 > log/log_dtlz3_m_5.log &
nohup ./emo_moea MOMBI3A input/Param_10D_paper.cfg dtlz3 50 > log/log_dtlz3_m_10.log &

nohup ./emo_moea MOMBI3A input/Param_03D_paper.cfg maf3 50 > log/log_maf3_m_3.log &
nohup ./emo_moea MOMBI3A input/Param_05D_paper.cfg maf3 50 > log/log_maf3_m_5.log &
nohup ./emo_moea MOMBI3A input/Param_10D_paper.cfg maf3 50 > log/log_maf3_m_10.log &

nohup ./emo_moea MOMBI3A input/Param_03D_paper.cfg maf4 50 > log/log_maf4_m_3.log &
nohup ./emo_moea MOMBI3A input/Param_05D_paper.cfg maf4 50 > log/log_maf4_m_5.log &
nohup ./emo_moea MOMBI3A input/Param_10D_paper.cfg maf4 50 > log/log_maf4_m_10.log &



# lanzar estos comandos el jueves en alux2 (para comparar con MOMBI3)

nohup ./emo_moea MOMBI3 input/Param_03D_paper.cfg dtlz1 50 > log/log_dtlz1_m_3.log &
nohup ./emo_moea MOMBI3 input/Param_05D_paper.cfg dtlz1 50 > log/log_dtlz1_m_5.log &
nohup ./emo_moea MOMBI3 input/Param_10D_paper.cfg dtlz1 50 > log/log_dtlz1_m_10.log &

nohup ./emo_moea MOMBI3 input/Param_03D_paper.cfg dtlz3 50 > log/log_dtlz3_m_3.log &
nohup ./emo_moea MOMBI3 input/Param_05D_paper.cfg dtlz3 50 > log/log_dtlz3_m_5.log &
nohup ./emo_moea MOMBI3 input/Param_10D_paper.cfg dtlz3 50 > log/log_dtlz3_m_10.log &

nohup ./emo_moea MOMBI3 input/Param_03D_paper.cfg maf3 50 > log/log_maf3_m_3.log &
nohup ./emo_moea MOMBI3 input/Param_05D_paper.cfg maf3 50 > log/log_maf3_m_5.log &
nohup ./emo_moea MOMBI3 input/Param_10D_paper.cfg maf3 50 > log/log_maf3_m_10.log &

nohup ./emo_moea MOMBI3 input/Param_03D_paper.cfg maf4 50 > log/log_maf4_m_3.log &
nohup ./emo_moea MOMBI3 input/Param_05D_paper.cfg maf4 50 > log/log_maf4_m_5.log &
nohup ./emo_moea MOMBI3 input/Param_10D_paper.cfg maf4 50 > log/log_maf4_m_10.log &



```





```
(keras) auraham@deepsea:~/git/emo_project/demo$ jobs
[1]   Running                 nohup ./emo_moea MOMBI3A input/Param_03D_paper.cfg dtlz3 50 > log/log_dtlz3_m_3.log &
[2]   Running                 nohup ./emo_moea MOMBI3A input/Param_05D_paper.cfg dtlz3 50 > log/log_dtlz3_m_5.log &
[3]   Running                 nohup ./emo_moea MOMBI3A input/Param_10D_paper.cfg dtlz3 50 > log/log_dtlz3_m_10.log &
[4]   Running                 nohup ./emo_moea MOMBI3A input/Param_03D_paper.cfg maf3 50 > log/log_maf3_m_3.log &
[5]   Running                 nohup ./emo_moea MOMBI3A input/Param_05D_paper.cfg maf3 50 > log/log_maf3_m_5.log &
[6]   Running                 nohup ./emo_moea MOMBI3A input/Param_10D_paper.cfg maf3 50 > log/log_maf3_m_10.log &
[7]   Running                 nohup ./emo_moea MOMBI3A input/Param_03D_paper.cfg maf4 50 > log/log_maf4_m_3.log &
[8]-  Running                 nohup ./emo_moea MOMBI3A input/Param_05D_paper.cfg maf4 50 > log/log_maf4_m_5.log &
[9]+  Running                 nohup ./emo_moea MOMBI3A input/Param_10D_paper.cfg maf4 50 > log/log_maf4_m_10.log &

```



```
(dev) acamacho@alux2:~$ ps aux|grep emo
root      1116  0.0  0.0 287740  6812 ?        Ssl   2019   1:22 /usr/lib/accountsservice/accounts-daemon
message+  1122  0.0  0.0  50308  4836 ?        Ss    2019   0:05 /usr/bin/dbus-daemon --system --address=systemd: --nofork --nopidfile --systemd-activation --syslog-only
daemon    1184  0.0  0.0  28332  2160 ?        Ss    2019   0:00 /usr/sbin/atd -f
acamacho 26717 99.9  0.0   9800  3708 ?        R    16:48   7:43 ./emo_moea MOMBI3A input/Param_03D_paper.cfg dtlz1 50
acamacho 26721 99.8  0.0  10724  4560 ?        R    16:48   7:31 ./emo_moea MOMBI3A input/Param_05D_paper.cfg dtlz1 50
acamacho 26725 99.9  0.0  10864  4552 ?        R    16:48   7:26 ./emo_moea MOMBI3A input/Param_10D_paper.cfg dtlz1 50
acamacho 26728 99.8  0.0   9800  3516 ?        R    16:48   7:18 ./emo_moea MOMBI3A input/Param_03D_paper.cfg dtlz3 50
acamacho 26731  100  0.0  10724  4620 ?        R    16:48   7:14 ./emo_moea MOMBI3A input/Param_05D_paper.cfg dtlz3 50
acamacho 26734 99.9  0.0  10884  4716 ?        R    16:48   7:09 ./emo_moea MOMBI3A input/Param_10D_paper.cfg dtlz3 50
acamacho 26737 99.9  0.0   9800  3652 ?        R    16:49   7:05 ./emo_moea MOMBI3A input/Param_03D_paper.cfg maf3 50
acamacho 26740 99.9  0.0  10724  4708 ?        R    16:49   7:01 ./emo_moea MOMBI3A input/Param_05D_paper.cfg maf3 50
acamacho 26743 99.7  0.0  10884  4652 ?        R    16:49   6:57 ./emo_moea MOMBI3A input/Param_10D_paper.cfg maf3 50
acamacho 26746 99.8  0.0   9800  3596 ?        R    16:49   6:53 ./emo_moea MOMBI3A input/Param_03D_paper.cfg maf4 50
acamacho 26749 99.9  0.0  10724  4552 ?        R    16:49   6:49 ./emo_moea MOMBI3A input/Param_05D_paper.cfg maf4 50
acamacho 26752 99.7  0.0  10884  4644 ?        R    16:49   6:45 ./emo_moea MOMBI3A input/Param_10D_paper.cfg maf4 50

```



espera a que termine, mientras, crea los scripts para medir el hv y reportar el valor en las tablas de mss

no hagas figuras







**descargar archivos**

```
# deepsea
/home/auraham/git/emo_project/demo/output

# mi laptop
cd Desktop
mkdir resultados_mombi3_deepsea
cd resultados_mombi3_deepsea
rsync -azP auraham@deepsea:/home/auraham/git/emo_project/demo/output .

# mi laptop
cd Desktop
mkdir resultados_mombi3_alux2
cd resultados_mombi3_alux2
rsync -azP acamacho@alux2:/home/acamacho/git/emo_project/demo/output .


```







**experimentacion pendiente**

- [ ] hacer que `mombi3a`, `mombi3b` y `mombi3` guarden los objs cada generacion con una bandera
- [ ] hacer que `mombi3b` sea como `mombi3a` pero no use `ipbi`
- [ ]  lanzar los comandos de arriba (que usan `mombi3a`), pero ahora con
- [ ]  `mombi3` el metodo original
  - [ ]  y `mombi3b`. (como `mombi3a` pero sin `ipbi`)

RECUERDA NO GUARDAR LOS OBJS CADA GENERACION



**resultados a generar**

- [x] Una tabla de hv
- [ ] Una animacion del proceso de busqueda de mombi3a, mombi3, y mss
- [x] Agregar `maf3` en `config.py` (`/home/auraham/git/emo_project/demo/performance`) para terminar de calcular el hv

```python
# config.py 
# MOMBI3A_%s_03D_R%02d.pof

# define the problems and algorithms
mop_names = ("dtlz1", "dtlz3", "maf4")      # add maf3 later
moea_names = (
                "MOMBI3A",
            )
```





## Animacion de mombi3

Ejecuta estos comandos en la rev `0b07df8 Se agrego soporte para guardar los objs cada generacion en mombi3.c` para obtener los objs de `mombi3`

```
cd git/emo_project/demo
./emo_moea MOMBI3 input/Param_03D.cfg dtlz1 1
./emo_moea MOMBI3A input/Param_03D.cfg dtlz1 1
./emo_moea MOMBI3B input/Param_03D.cfg dtlz1 1

./emo_moea MOMBI3 input/Param_03D.cfg dtlz3 1
./emo_moea MOMBI3A input/Param_03D.cfg dtlz3 1
./emo_moea MOMBI3B input/Param_03D.cfg dtlz3 1
```

Asegurate de usar este bloque:

```c
/* Save population every generation */
if (1) {
    // note: we use run = 1
    int run_idx = 0;
    EMO_Population_write_generation(pop, NULL, mop, param->prefix, run_idx, 0, generation_t);
}
```

Ejecuta la animación de este modo:

```
%run anim_pop.py -p dtlz3 -a MOMBI3
%run anim_pop.py -p dtlz3 -a MOMBI3A
%run anim_pop.py -p dtlz3 -a MOMBI3B
```





## Que sigue?

- [x] hacer que `mombi3a`, `mombi3b` y `mombi3` guarden los objs cada generacion con una bandera
- [x] hacer que `mombi3b` sea como `mombi3a` pero no use `ipbi`
- [ ] lanzar los comandos de arriba (que usan `mombi3a`), pero ahora con
  - [ ] `mombi3` el metodo original
  - [ ] y `mombi3b`. (como `mombi3a` pero sin `ipbi`)

ie, me refiero a estos comandos, reemplazando el `moea_name` por `MOMBI3` o `MOMBI3B`:

```
nohup ./emo_moea MOMBI3 input/Param_03D_paper.cfg dtlz1 50 > log/log_dtlz1_m_3.log &
nohup ./emo_moea MOMBI3 input/Param_05D_paper.cfg dtlz1 50 > log/log_dtlz1_m_5.log &
nohup ./emo_moea MOMBI3 input/Param_10D_paper.cfg dtlz1 50 > log/log_dtlz1_m_10.log &

nohup ./emo_moea MOMBI3 input/Param_03D_paper.cfg dtlz3 50 > log/log_dtlz3_m_3.log &
nohup ./emo_moea MOMBI3 input/Param_05D_paper.cfg dtlz3 50 > log/log_dtlz3_m_5.log &
nohup ./emo_moea MOMBI3 input/Param_10D_paper.cfg dtlz3 50 > log/log_dtlz3_m_10.log &

nohup ./emo_moea MOMBI3 input/Param_03D_paper.cfg maf3 50 > log/log_maf3_m_3.log &
nohup ./emo_moea MOMBI3 input/Param_05D_paper.cfg maf3 50 > log/log_maf3_m_5.log &
nohup ./emo_moea MOMBI3 input/Param_10D_paper.cfg maf3 50 > log/log_maf3_m_10.log &

nohup ./emo_moea MOMBI3 input/Param_03D_paper.cfg maf4 50 > log/log_maf4_m_3.log &
nohup ./emo_moea MOMBI3 input/Param_05D_paper.cfg maf4 50 > log/log_maf4_m_5.log &
nohup ./emo_moea MOMBI3 input/Param_10D_paper.cfg maf4 50 > log/log_maf4_m_10.log &
```





## Experimentacion para el paper `paper_moead_mss_dci`

A continuacion describo el procedimiento para realizar la experimentacion para el paper. Compararemos el desempeño de `mombi3` (método original) y `mombi3c` (método original + `ipbi`). Para esto, ejecutaremos los siguientes comandos en `alux2`.

```
ssh alux2
cd ~/git/emo_project
nohup git pull &

make clean; make
cd demo

# no sirvio
./run_server.sh MOMBI3 dtlz1   # revisa htop para checar si mando los 4 comandos en el background
```

----

**Nota** Por alguna razon, no puedo ejecutar mas de una instancia de `emo_moea` con `run_server.sh`, creo que tiene que ver con que no puedo renombrar un archivo de log:

```
(dev) acamacho@alux2:~/git/emo_project/demo/log$ cat log_MOMBI3_dtlz1_10.log 
log tmp MOMBI3_dtlz1_0.log
Error when renaming the file MOMBI3_dtlz1_0.log by ./output/MOMBI3_DTLZ1_10D.log
```

```c
void EMO_Param_alloc_from_file(EMO_Param *param, EMO_MOP *mop, char *alg, char *file, char *problem) {
  char *str1, *str2, *str3, *algorithm;
  int v = 0, flag, proc;
  double fitness = 0;

  param->mop = mop;
  param->parser = (EMO_Parser *) malloc(sizeof(EMO_Parser));
  param->dbg = (EMO_Debug *) malloc(sizeof(EMO_Debug));
  param->rand = (EMO_Rand *) malloc(sizeof(EMO_Rand));
  param->stop = (EMO_Stop *) malloc(sizeof(EMO_Stop));

  param->prefix = (char *) malloc(sizeof(char) * MAX_CHAR);
  param->flog = (char *) malloc(sizeof(char) * MAX_CHAR);
  param->fsum = (char *) malloc(sizeof(char) * MAX_CHAR);
  param->algorithm = (char *) malloc(sizeof(char) * MAX_CHAR);
  param->subalgorithm = (char *) malloc(sizeof(char) * MAX_CHAR);

  str1 = (char *) malloc(sizeof(char) * MAX_CHAR);
  str2 = (char *) malloc(sizeof(char) * MAX_CHAR);
  str3 = (char *) malloc(sizeof(char) * MAX_CHAR);

  strcpy(param->algorithm, alg);

  algorithm = EMO_toupper(alg);

  #ifdef EMO_MPI
    MPI_Comm_rank(MPI_COMM_WORLD, &proc);
    sprintf(param->algorithm, "p%s", algorithm);
  #else
    proc = 0;
    strcpy(param->algorithm, algorithm);
  #endif

  sprintf(param->flog, "%s_%s_%d.log", param->algorithm, problem, proc); // random number

  printf("log tmp %s\n", param->flog);
  EMO_Debug_alloc(param->dbg, EMO_DEBUG_ON, proc, param->flog);

  strcpy(param->subalgorithm, algorithm); // required by IBEA

  EMO_Parser_alloc_from_file(param->parser, param->dbg, file);

  if(strcmp(problem, "DEFAULT") != 0 && strcmp(problem, "default") != 0)
    EMO_Benchmark_alloc(mop, param, problem);

  if(!EMO_Param_get_char(param, str1, "output")) {
    printf("Error, output is not defined in the configuration file.\n");
    exit(1);
  }

  #ifdef EMO_MPI
    EMO_Debug_printf(param->dbg, "EMO_MPI is defined");
    sprintf(param->flog, "%s/%s_%d_%s_%.2dD.log", str1, param->algorithm, proc, mop->name, mop->nobj);
    sprintf(param->prefix, "%s/%s_%d_%s_%.2dD_R", str1, param->algorithm, proc, mop->name, mop->nobj);
    sprintf(param->fsum, "%s/%s_%d_%s_%.2dD.sum", str1, param->algorithm, proc, mop->name, mop->nobj);
  #else
    EMO_Debug_printf(param->dbg, "EMO_MPI is not defined");
    sprintf(param->flog, "%s/%s_%s_%.2dD.log", str1, param->algorithm, mop->name, mop->nobj);
    sprintf(param->prefix, "%s/%s_%s_%.2dD_R", str1, param->algorithm, mop->name, mop->nobj);
    sprintf(param->fsum, "%s/%s_%s_%.2dD.sum", str1, param->algorithm, mop->name, mop->nobj);
  #endif

    // el error ocurre aqui
  EMO_Debug_rename(param->dbg, param->flog);
  ...
}
```

Por lo tanto, vamos a modificar `run_server.sh` para incluir un nuevo parametro `$m_objs` y agregar una hoja de calculo para registrar los experimentos.

Falta tambien agregar el caso para m=8 objs.

----

```
ssh alux2
cd ~/git/emo_project
nohup git pull &

make clean; make
cd demo

# start: Fri Apr  3 08:51:58 CST 2020
# end 9:06
./run_server.sh MOMBI3 dtlz1 03
./run_server.sh MOMBI3 dtlz2 03
./run_server.sh MOMBI3 dtlz3 03
./run_server.sh MOMBI3 inv_dtlz1 03
./run_server.sh MOMBI3 maf1 03
./run_server.sh MOMBI3 maf2 03
./run_server.sh MOMBI3 maf3 03
./run_server.sh MOMBI3 maf4 03
./run_server.sh MOMBI3 maf5 03
```



## Descarga de resultados

```
sudo openvpn --config acamacho__ssl_vpn_config.ovpn 

# laptop
cd /media/data/emo_project/demo/output
rsync -azP acamacho@alux2:/home/acamacho/git/emo_project/demo/output .
```



```
# alux2
mosh alux2
cd ~/git/emo_project/
git status                # verificar que no haya archivos modificados
git pull                  # actualizar repo para agregar weights despues
make clean; make
Sat Apr 11 08:09:17 CDT 2020
```



## Proxima evaluacion

- [x] Agregar `Param_08D_paper.cfg`
- [x] Agregar `wfile = ./input/weight/weight_08D_3.sld` y verificar que `pop_size` sea igual al numero de weights.
- [x] Verificar que estos parametros sean iguales en todos los archivos de config:

```
# Crossover probability
pc = 1.0

# Mutation probability (by default -1 is equivalent to Pm = 1/N)
pm = -1 

# Crossover distribution index (SBX)
nc = 30

# Mutation distribution index (Real polynomial mutation)
nm = 20
```

- [x] Establecer el valor de `H=3`:

```
# H is the partition in Simplex Lattice Design method (SLD),
# parameter H for two_level_penalty_boundary_intersection 
# and quadratic_penalty_boundary_intersection
utility_H = 3 
```

- [x] Establecer variables de wfg: nota: lo deje igual
- [x] Establecer el valor de `feval`

```
# 1000 generaciones * 220 pop_size
feval = 220000
```

- [x] Cambiar la frecuencia a 100 generaciones en este ciclo de `mombi3.c` y `mombi3c.c`

```c
// debe empezar en dos
int generation_t = 2;


/* Save population every generation */
if (generation_t % 100 == 0) {
    // note: we use run = 1
    int run_idx = 0;
    EMO_Population_write_generation(pop, NULL, mop, param->prefix, run_idx, 0, generation_t);
}
```

- [ ] Realizar experimentacion en mi laptop (ejecutar comandos de `log_emo.ods`)

```
cd /home/auraham/git/emo_project/
make clean; make

cd /home/auraham/git/emo_project/demo
mkdir output_paper


./run_server.sh MOMBI3 dtlz1 03       		dom abr 26 22:18:12 CDT 2020
./run_server.sh MOMBI3 dtlz2 03				dom abr 26 22:18:12 CDT 2020
./run_server.sh MOMBI3 dtlz3 03				dom abr 26 22:18:12 CDT 2020
./run_server.sh MOMBI3 inv_dtlz1 03			dom abr 26 22:18:12 CDT 2020
./run_server.sh MOMBI3 maf1 03				dom abr 26 22:41:08 CDT 2020
./run_server.sh MOMBI3 maf2 03				dom abr 26 22:41:08 CDT 2020
./run_server.sh MOMBI3 maf3 03				dom abr 26 22:41:08 CDT 2020
./run_server.sh MOMBI3 maf4 03				dom abr 26 22:41:08 CDT 2020
./run_server.sh MOMBI3 maf5 03				dom abr 26 23:00:36 CDT 2020		

./run_server.sh MOMBI3 dtlz1 05				dom abr 26 23:00:36 CDT 2020
./run_server.sh MOMBI3 dtlz2 05				dom abr 26 23:00:36 CDT 2020
./run_server.sh MOMBI3 dtlz3 05				dom abr 26 23:00:36 CDT 2020
./run_server.sh MOMBI3 inv_dtlz1 05			lun abr 27 00:50:49 CDT 2020
./run_server.sh MOMBI3 maf1 05				lun abr 27 00:50:49 CDT 2020
./run_server.sh MOMBI3 maf2 05				lun abr 27 00:50:49 CDT 2020
./run_server.sh MOMBI3 maf3 05				lun abr 27 00:50:49 CDT 2020
./run_server.sh MOMBI3 maf4 05				lun abr 27 21:57:21 CDT 2020
./run_server.sh MOMBI3 maf5 05				lun abr 27 21:57:21 CDT 2020

./run_server.sh MOMBI3 dtlz1 08				lun abr 27 21:57:21 CDT 2020
./run_server.sh MOMBI3 dtlz2 08				lun abr 27 21:57:21 CDT 2020
./run_server.sh MOMBI3 dtlz3 08				lun abr 27 23:32:22 CDT 2020
./run_server.sh MOMBI3 inv_dtlz1 08			lun abr 27 23:32:22 CDT 2020
./run_server.sh MOMBI3 maf1 08				lun abr 27 23:32:22 CDT 2020
./run_server.sh MOMBI3 maf2 08				lun abr 27 23:32:22 CDT 2020
./run_server.sh MOMBI3 maf3 08				mar abr 28 01:30:09 CDT 2020
./run_server.sh MOMBI3 maf4 08				mar abr 28 01:30:09 CDT 2020
./run_server.sh MOMBI3 maf5 08				mar abr 28 01:30:09 CDT 2020

./run_server.sh MOMBI3 dtlz1 10				mié abr 29 23:23:33 CDT 2020
./run_server.sh MOMBI3 dtlz2 10				mié abr 29 23:23:33 CDT 2020
./run_server.sh MOMBI3 dtlz3 10				mié abr 29 23:23:33 CDT 2020
./run_server.sh MOMBI3 inv_dtlz1 10			mié abr 29 23:23:33 CDT 2020
./run_server.sh MOMBI3 maf1 10				jue abr 30 21:47:03 CDT 2020
./run_server.sh MOMBI3 maf2 10				jue abr 30 21:47:03 CDT 2020
./run_server.sh MOMBI3 maf3 10				jue abr 30 21:47:03 CDT 2020
./run_server.sh MOMBI3 maf4 10				jue abr 30 21:47:03 CDT 2020
./run_server.sh MOMBI3 maf5 10				mié may  6 22:26:45 CDT 2020

-------


./run_server.sh MOMBI3C dtlz1 03			dom may 10 23:41:41 CDT 2020						
./run_server.sh MOMBI3C dtlz2 03			dom may 10 23:41:41 CDT 2020
./run_server.sh MOMBI3C dtlz3 03			dom may 10 23:41:41 CDT 2020
./run_server.sh MOMBI3C inv_dtlz1 03		lun may 11 01:08:46 CDT 2020	
./run_server.sh MOMBI3C maf1 03				lun may 11 01:08:46 CDT 2020
./run_server.sh MOMBI3C maf2 03				lun may 11 01:08:46 CDT 2020
./run_server.sh MOMBI3C maf3 03				lun may 11 01:08:46 CDT 2020
./run_server.sh MOMBI3C maf4 03				lun may 11 01:38:55 CDT 2020	
./run_server.sh MOMBI3C maf5 03				lun may 11 01:38:55 CDT 2020

./run_server.sh MOMBI3C dtlz1 05			jue may  7 23:37:54 CDT 2020			
./run_server.sh MOMBI3C dtlz2 05			jue may  7 23:37:54 CDT 2020
./run_server.sh MOMBI3C dtlz3 05			jue may  7 23:37:54 CDT 2020
./run_server.sh MOMBI3C inv_dtlz1 05		jue may  7 23:37:54 CDT 2020
./run_server.sh MOMBI3C maf1 05				vie may  8 03:32:39 CDT 2020				
./run_server.sh MOMBI3C maf2 05				vie may  8 03:32:39 CDT 2020
./run_server.sh MOMBI3C maf3 05				vie may  8 03:32:39 CDT 2020
./run_server.sh MOMBI3C maf4 05				vie may  8 03:32:39 CDT 2020
./run_server.sh MOMBI3C maf5 05				vie may  8 22:42:52 CDT 2020					

./run_server.sh MOMBI3C dtlz1 08			vie may  8 22:42:52 CDT 2020
./run_server.sh MOMBI3C dtlz2 08			vie may  8 22:42:52 CDT 2020
./run_server.sh MOMBI3C dtlz3 08			vie may  8 22:42:52 CDT 2020
./run_server.sh MOMBI3C inv_dtlz1 08		dom may 10 21:13:17 CDT 2020	
./run_server.sh MOMBI3C maf1 08				dom may 10 21:13:17 CDT 2020
./run_server.sh MOMBI3C maf2 08				dom may 10 21:13:17 CDT 2020
./run_server.sh MOMBI3C maf3 08				dom may 10 21:13:17 CDT 2020
./run_server.sh MOMBI3C maf4 08				dom may 10 22:32:46 CDT 2020		
./run_server.sh MOMBI3C maf5 08				dom may 10 22:32:46 CDT 2020

./run_server.sh MOMBI3C dtlz1 10			dom may  3 21:31:00 CDT 2020
./run_server.sh MOMBI3C dtlz2 10			dom may  3 21:31:00 CDT 2020
./run_server.sh MOMBI3C dtlz3 10			dom may  3 21:31:00 CDT 2020
./run_server.sh MOMBI3C inv_dtlz1 10		dom may  3 21:31:00 CDT 2020
./run_server.sh MOMBI3C maf1 10				lun may  4 22:29:17 CDT 2020			
./run_server.sh MOMBI3C maf2 10				lun may  4 22:29:17 CDT 2020
./run_server.sh MOMBI3C maf3 10				lun may  4 22:29:17 CDT 2020
./run_server.sh MOMBI3C maf4 10				lun may  4 22:29:17 CDT 2020
./run_server.sh MOMBI3C maf5 10				mié may  6 22:26:45 CDT 2020			



```

- [x] Crear repo `emo_project_results` en `/media/data/git` **Nota** ya no es necesario, guardé los resultados en el mismo repositorio `emo_project`.
- [x] Descargar los resultados en `/media/data/git/emo_project_results`
- [x] Configurar `config.py` en `~/git/emo_project/demo/performance` para apuntar al path donde esten los resultados:

```python
# number of independent runs
runs = 50                       

# path for saving logs
logs = {
    #"rocket"  : "/home/auraham/Desktop/resultados_mombi3_alux2/output/",
    "rocket"  : "/media/data/git/emo_project/demo/output",
}

# path of results
paths = {
    #"rocket"  : "/home/auraham/Desktop/resultados_mombi3_alux2/output/",
    "rocket"  : "/media/data/git/emo_project/demo/output",
}

# path for saving results in git
paths_git = {
    #"rocket"  : "/home/auraham/Desktop/resultados_mombi3_alux2/output_git/",
    "rocket"  : "/media/data/git/emo_project/demo/output_git,
}

# computational budget
budget = {      # {m_objs:iters}
    "dtlz1"     : {3: 300, 5: 500, 8: 800, 10: 1000},
    "dtlz2"     : {3: 300, 5: 500, 8: 800, 10: 1000},
    "dtlz3"     : {3: 300, 5: 500, 8: 800, 10: 1000},
    "inv-dtlz1" : {3: 300, 5: 500, 8: 800, 10: 1000},
    "maf1"      : {3: 300, 5: 500, 8: 800, 10: 1000},
    "maf2"      : {3: 300, 5: 500, 8: 800, 10: 1000},
    "maf3"      : {3: 300, 5: 500, 8: 800, 10: 1000},
    "maf4"      : {3: 300, 5: 500, 8: 800, 10: 1000},
    "maf5"      : {3: 300, 5: 500, 8: 800, 10: 1000},
}

# define the problems and algorithms
mop_names = ("dtlz1", "dtlz2", "dtlz3", "inv-dtlz1", "maf1", "maf2", "maf3", "maf4", "maf5")
moea_names = (
                "MOMBI3",
                "MOMBI3C",
            )
```

- [x] Configurar `moea_names` en `paper_table_mss_baselines.py` en `~/git/emo_project/demo/tables`:

```python
# ADD M:8 OBJS
# {m_objs:fevals}
    main_budget = {      
        "dtlz1"     : {3: 300, 5: 500, 8: 800, 10: 1000},
        "dtlz2"     : {3: 300, 5: 500, 8: 800, 10: 1000},
        "dtlz3"     : {3: 300, 5: 500, 8: 800, 10: 1000},
        "inv-dtlz1" : {3: 300, 5: 500, 8: 800, 10: 1000},
        "maf1"      : {3: 300, 5: 500, 8: 800, 10: 1000},
        "maf2"      : {3: 300, 5: 500, 8: 800, 10: 1000},
        "maf3"      : {3: 300, 5: 500, 8: 800, 10: 1000},
        "maf4"      : {3: 300, 5: 500, 8: 800, 10: 1000},
        "maf5"      : {3: 300, 5: 500, 8: 800, 10: 1000},
    }


# ADD THIS
mombi3_path = "/home/auraham/git/emo_project/demo/output_paper_git"

# define sorted moea_names
moea_names = (

    # moead + normalization
    #(baseline_path, "moead_norm_v1-ws-norm-true",    "WS"), 
    (baseline_path, "moead_norm_v1-te-norm-true",    "TE"),
    (baseline_path, "moead_norm_v1-asf-norm-true",   "MTE"),      # ASF
    (baseline_path, "moead_norm_v1-pbi-norm-true",   "PBI"),
    #(baseline_path, "moead_norm_v1-ipbi-norm-true",  "IPBI"),    # IGD in MaF3 is too large
    (baseline_path, "moead_norm_v1-vads-norm-true",  "VADS"),

    # ADD THIS
    # mombi3
    (mombi3_path, "MOMBI3",  "MOMBI-III"),
    (mombi3_path, "MOMBI3C",  "MOMBI-III*"),

    # mss + normalization
    (proposal_path, "mss-dci_norm_hyp_mean-divs-8-25-mean-10-ps-normal-ss-gra-norm-true",  "MSS-DCI"),
)
```

- [x] Crear la estructura de directorios esperada por `rocket` (usar `/home/auraham/Desktop/resultados_mombi3_alux2/output` como referencia):

```
/media/data/git/emo_project/demo/output
	dtlz1
		MOMBI3C
			stats
	...
	maf5
		MOMBI3C
			stats
```

debe quedar asi

```
~/git/emo_project/demo/output_paper/
	dtlz1/
		MOMBI3A/
			stats
	...
	maf5/
		MOMBI3A/
			stats
```

Usa estos comandos

```
cd ~/git/emo_project/demo/output_paper
mkdir -p dtlz1/MOMBI3/stats
mkdir -p dtlz2/MOMBI3/stats
mkdir -p dtlz3/MOMBI3/stats
mkdir -p inv-dtlz1/MOMBI3/stats
mkdir -p maf1/MOMBI3/stats
mkdir -p maf2/MOMBI3/stats
mkdir -p maf3/MOMBI3/stats
mkdir -p maf4/MOMBI3/stats
mkdir -p maf5/MOMBI3/stats

mkdir -p dtlz1/MOMBI3C/stats
mkdir -p dtlz2/MOMBI3C/stats
mkdir -p dtlz3/MOMBI3C/stats
mkdir -p inv-dtlz1/MOMBI3C/stats
mkdir -p maf1/MOMBI3C/stats
mkdir -p maf2/MOMBI3C/stats
mkdir -p maf3/MOMBI3C/stats
mkdir -p maf4/MOMBI3C/stats
mkdir -p maf5/MOMBI3C/stats
```


- [x] Descomenta un mop de `performance/config.py`antes de calcular el hv durante la noche:

```python
mop_names = (
            "dtlz1", 
            #"dtlz2", 
            #"dtlz3", 
            #"inv-dtlz1",   # falta este mop!
            #"maf1", 
            #"maf2", 
            #"maf3", 
            #"maf4", 
            #"maf5",
            )
```


- [x] Ejecutar los scripts:

```
cd ~/git/emo_project/demo/performance
python3 run_hv.py -k rocket
```

- [x] Guarda los resultados en gitlab

```
cd ~/git/emo_project/demo/output_paper
git add * -f
git commit . -m "Se agregaron resultados hv de MOP_NAME"
```


- [x] Crea las tablas **nota** la rev`6b944f5` tiene este punto y los anteriores, solo falta agregar el analisis estadistico

```
cd ~/git/emo_project/demo/tables
python3 paper_table_mss_baselines.py -l a -i hv
```

- [ ] Agregar script para analisis estadistico