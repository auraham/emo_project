# emo_test.py
from __future__ import print_function
import numpy as np
import os, sys, argparse
np.seterr(all='raise')

# rocket
sys.path.insert(0, "/home/auraham/git/moead_mss_dci")
from rocket.problems import evaluate_mop

def pretty(mat):
    
    for i, row in enumerate(mat):
        
        s = " ".join(["%.5f" % v for v in row])
        print("%d %s" % (i, s))

if __name__ == "__main__":
    
    
    # ./emo_test inv_dtlz1 input/Param_10D_test.cfg input_inv_dtlz1_m_10.txt
    # python3 emo_test.py -p inv_dtlz1 -m 3 -i input_inv_dtlz1_m_10.txt
    ap = argparse.ArgumentParser()
    ap.add_argument("-p", "--mop_name", required=True, help="mop name: dtlz1", choices=("maf1", "maf2", "maf3", "maf4", "maf5", "inv_dtlz1"))
    ap.add_argument("-m", "--m_objs", required=True, help="m_objs: 3, 5, 8, 10", type=int)
    ap.add_argument("-i", "--input_file", required=True, help="mop name: dtlz1")
    args = vars(ap.parse_args())
    
    # load input
    chroms = np.genfromtxt(args["input_file"])
    mop_name = args["mop_name"]
    m_objs = args["m_objs"]
    pop_size, n_genes = chroms.shape

    # change if needed
    if mop_name == "inv_dtlz1":
        mop_name = "inv-dtlz1"
    
    # evaluate input
    params = {"name": mop_name}
    objs = evaluate_mop(chroms, m_objs, params)
    
    # save output
    mop_name = mop_name.replace("-", "_")
    filename = "output_objs_%s_m_%d_python.txt" % (mop_name, m_objs)
    header = " %d %d" % (pop_size, m_objs)
    np.savetxt(filename, objs, fmt="%.30f", comments="#", header=header)
    print(filename)
