# plot_all_pops.py
from __future__ import print_function
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.pyplot import rcParams
import matplotlib.pyplot as plt
import numpy as np
import os, sys, argparse
np.seterr(all='raise')


# remove this
import sys
sys.path.insert(0, "/home/auraham/git/moead_mss_dci")

from rocket.plot import parallel_coordinates as pc
from rocket.fronts import get_real_front
from rocket.pareto_fronts import get_front

colors = [
        "#74CE74",
        "#FFA500",
        "#F7523A",
        "#5A5A5A",
        "#0759AB",
        "y",
        "m",
        "g",
        "c",
        "#F9FF81",
        
        
        "#7BC4D4", 
        "#F8BD5A", 
        "#F86B5A", 
        "#4CBC9A", 
        "#45754A",
        "#85C68B", 
        "#6A007C",
        "#7BC4D4", 
        "#F8BD5A", 
        "#F86B5A", 
        "#4CBC9A", 
        "#45754A",
        "#85C68B", 
        "#6A007C",
        
        "#7BC4D4", 
        "#F8BD5A", 
        "#F86B5A", 
        "#4CBC9A", 
        "#45754A",
        "#85C68B", 
        "#6A007C",
        
        "#7BC4D4", 
        "#F8BD5A", 
        "#F86B5A", 
        "#4CBC9A", 
        "#45754A",
        "#85C68B", 
        "#6A007C",
        
        ]
        
basecolors = {
    "real_front"    : "#7C8577",
    "z_ideal"       : "#ECF037",
    "z_nadir"       : "#F03785",
    "to_keep"       : "#8CDC6B",
    "to_replace"    : "#620F0E",
    "almost_black"  : "#262626",
    "weights"       : "#E4AFC8",
    "weights_hl"    : "#7D2950",
    "cherry_dark"   : "#A91458",
    "cherry_light"  : "#E4AFC8",
    "jitter"        : "#74CE74",
    "jitter_dark"   : "#539453",
}


def load_rcparams(figsize=None, for_paper=False):
    """
    Load a custom rcParams dictionary
    """
    
    rcParams['axes.titlesize']  = 14            # title
    rcParams['axes.labelsize']  = 17            # $f_i$ labels
    rcParams['xtick.color']     = "#474747"     # ticks gray color
    rcParams['ytick.color']     = "#474747"     # ticks gray color
    rcParams['xtick.labelsize'] = 10            # ticks size
    rcParams['ytick.labelsize'] = 10            # ticks size
    rcParams['legend.fontsize'] = 12            # legend
    rcParams['legend.fontsize'] = 12            # legend

    if for_paper:    
        rcParams['font.family'] = 'serif'       # font face
                
    if isinstance(figsize, tuple):
        rcParams['figure.figsize'] = figsize    # figsize, common values: (12, 5), (8, 6)
    
    #print("(load_rcparams) figsize: (%d, %d), you can use (12, 5), (8, 6)" % (rcParams['figure.figsize'][0], rcParams['figure.figsize'][1]))


def plot_pops(pops, labels, title=None, figname=None, real_front=None, p=None, plot_surface=False, 
        lower_lims=None, upper_lims=None, output="", show_plot=True, z_ideal=None,
        z_nadir=None, loc="lower left", ncol=2, show_legend=True, custom_colors=None, custom_ms=None, elev=30, azim=45):
    """
    Plot a set of points in 2D and 3D.
    
    pops            list of l point matrices of size (n_points, m_objs)
    labels          list of l string labels, one for each point matrix
    
    title           string, plot title
    figname         string, figure name
    real_front      matrix, Pareto front
    p               int, spacing parameter needed for ploting real_front using a wireframe
    plot_surface    bool, determines if the wireframe will be filled
    lower_lims      (m_objs, ) array, lower bounds of the plot
    upper_lims      (m_objs, ) array, upper bounds of the plot
    output          string, output path
    show_plot       bool, determines if the plot will be shown (useful when the plot will be saved but not shown)
    z_ideal         (m_objs, ) array, z ideal vector
    z_nadir         (m_objs, ) array, z ideal nadir
    loc             string, legend location
    ncol            int, number of legend columns
    show_legend     bool, enable legend
    custom_colors   tuple, list with colors for pops
    """
    
    # check
    m_objs = pops[0].shape[1]
    if m_objs > 3:
        print("plot_pops.py, m_objs: %s > 3, it is not possible to plot" % m_objs)    # @todo: add log support here
        # return fig, ax
        return None, None

    # create plot
    fig = plt.figure(figname) if figname else plt.figure()
    ax  = fig.add_subplot(111) if m_objs == 2 else fig.add_subplot(111, projection="3d")

    # set labels
    ax.set_xlabel("$f_1$")
    ax.set_ylabel("$f_2$")
    if m_objs == 3:
        ax.set_zlabel("$f_3$")

    # set title
    if title:
        ax.set_title(title)

    # set lims
    if not (lower_lims is None or upper_lims is None):
    
        ax.set_xlim(lower_lims[0], upper_lims[0])
        ax.set_ylim(lower_lims[1], upper_lims[1])
        
        if m_objs == 3:
            ax.set_zlim(lower_lims[2], upper_lims[2])
    
    # change panes
    if m_objs == 3:
        ax.w_xaxis.set_pane_color((0.95, 0.95, 0.95, 1.0))
        ax.w_yaxis.set_pane_color((0.95, 0.95, 0.95, 1.0))
        ax.w_zaxis.set_pane_color((0.95, 0.95, 0.95, 1.0))
    
            
    # change the axis title to almostblack
    ax.title.set_color(basecolors["almost_black"])
    
    # callback
    lined = {}      # lines dictionary
    lines = []      # lines list
    
    # real front
    if real_front is not None:
        
        if m_objs == 2:
            
            l, = ax.plot(real_front[:, 0], real_front[:, 1], color=basecolors["real_front"])
            lines.append(l)
            lined["Real Front"] = l
            
        elif m_objs == 3:
            
            l = None
            
            if p:
            
                # plot meshgrid
                X = real_front[:, 0].reshape(p, p)
                Y = real_front[:, 1].reshape(p, p)
                Z = real_front[:, 2].reshape(p, p)
    
                if plot_surface:
                    l = ax.plot_surface(X, Y, Z, rstride=2, cstride=2, color=basecolors["real_front"], alpha=0.2)
                else:
                    l = ax.plot_wireframe(X, Y, Z, rstride=2, cstride=2, color=basecolors["real_front"])
                    
            else:
                
                # plot scatter
                l = ax.scatter(real_front[:, 0], real_front[:, 1], real_front[:, 2], label="Real Front", 
                        facecolor=basecolors["real_front"], c=basecolors["real_front"], 
                        edgecolor=basecolors["almost_black"], linewidth=0.15)
                    
            lines.append(l)
            lined["Real Front"] = l
    
    # custom colors
    if custom_colors is None:
        custom_colors = tuple(colors)       # @todo: maybe tuple() is not needed
    
    # custom marker sizes
    if custom_ms is None:
        custom_ms = tuple([6 for i in range(len(pops))])
    
    
    
    # pops
    for i, pop in enumerate(pops):
        
        if m_objs == 2:
            #l = ax.scatter(pop[:, 0], pop[:, 1], label=labels[i], facecolor=custom_colors[i], 
            #            edgecolor=basecolors["almost_black"], linewidth=0.15, zorder=5)
            
            l, = ax.plot(pop[:, 0], pop[:, 1], label=labels[i], c=custom_colors[i%5], 
                        mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15, ms=custom_ms[i])
            
            lines.append(l)
            lined[labels[i]] = l
            
        elif m_objs == 3:
            l, = ax.plot(pop[:, 0], pop[:, 1], pop[:, 2], label=labels[i], c=custom_colors[i%5], 
                        mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15, ms=custom_ms[i])
            lines.append(l)
            lined[labels[i]] = l
    
    
    # z_ideal
    if z_ideal is not None:
        
        l = None
        z = z_ideal.reshape((1, m_objs))
        
        if m_objs == 2:
            l, = ax.plot(z[:, 0], z[:, 1], label="Z ideal", mec=basecolors["almost_black"], 
                    c=basecolors["z_ideal"], ls="none", marker="*", markeredgewidth=0.15)
            
        elif m_objs == 3:
            l, = ax.plot(z[:, 0], z[:, 1], z[:, 2], label="Z ideal", mec=basecolors["almost_black"], 
                    c=basecolors["z_ideal"], ls="none", marker="*", markeredgewidth=0.15)

        lines.append(l)
        lined["Z ideal"] = l
        
        
    # z_nadir
    if z_nadir is not None:
        
        l = None
        z = z_nadir.reshape((1, m_objs))
        
        if m_objs == 2:
            l, = ax.plot(z[:, 0], z[:, 1], label="Z nadir", mec=basecolors["almost_black"], 
                    c=basecolors["z_nadir"], ls="none", marker="*", markeredgewidth=0.15)
            
        elif m_objs == 3:
            l, = ax.plot(z[:, 0], z[:, 1], z[:, 2], label="Z nadir", mec=basecolors["almost_black"], 
                    c=basecolors["z_nadir"], ls="none", marker="*", markeredgewidth=0.15)

        lines.append(l)
        lined["Z nadir"] = l
    
    
    # legend
    if show_legend:
        
        leg = ax.legend(loc=loc, scatterpoints=1, ncol=ncol, numpoints=1)
        
        # for callback
        for legtxt, __ in zip(leg.get_texts(), lines):
            legtxt.set_picker(5)
        
        
    # inner callback
    def on_pick(event):
        
        legtxt, line = None, None
        
        obj = event.artist
        is_callable = callable(getattr(obj, "get_text", None))
        
        if is_callable:
            
            legtxt = event.artist
            line = lined[legtxt.get_text()]
            
        else:
            
            legtxt = None
            line = event.artist
            
        is_visible = line.get_visible()
        
        # fade a little bit
        if is_visible:
            
            line.set_visible(False)
            
            if legtxt is not None:
                legtxt.set_alpha(0.2)
                
        else:
            
            line.set_visible(True)
            
            if legtxt is not None:
                legtxt.set_alpha(None)
                
        fig.canvas.draw()
    
    if m_objs == 3:
        ax.view_init(elev, azim)
        
    
    # register callback only if legend is visible
    if show_legend:
        #fig.canvas.callbacks.connect("pick_event", on_pick)
        fig.canvas.mpl_connect("pick_event", on_pick)

    # save before plt.show
    if output:                                                  # @todo: check if output is a valid path
        #fig.set_size_inches(width, height)
        fig.savefig(output, dpi=300, bbox_inches="tight")
    
    
    if show_plot:
        plt.show()


    return fig, ax
    

def load_objs(input_filepath):

    objs = np.genfromtxt(input_filepath, comments="#") 
    return objs.copy()

if __name__ == "__main__":
    
    
    # --- parse arguments ---
    ap = argparse.ArgumentParser()
    #ap.add_argument("-i", "--input", required=True, help="Pareto front approximation file")
    ap.add_argument("-p", "--mop_name", required=True, help="MOP name")
    args = vars(ap.parse_args())

    # get input
    input_filepaths = (
                    "output/MOMBI3A_DTLZ3_03D_R01.pof",
                    "output/MOMBI3A_DTLZ3_03D_R02.pof",
                    "output/MOMBI3A_DTLZ3_03D_R03.pof",
                    "output/MOMBI3A_DTLZ3_03D_R04.pof",
                    "output/MOMBI3A_DTLZ3_03D_R05.pof",
                    "output/MOMBI3A_DTLZ3_03D_R06.pof",
                    "output/MOMBI3A_DTLZ3_03D_R07.pof",
                    "output/MOMBI3A_DTLZ3_03D_R08.pof",
                    "output/MOMBI3A_DTLZ3_03D_R09.pof",
    )
    
    # resultados alux2
    input_filepaths = ["/home/auraham/Desktop/resultados_mombi3_alux2/output/MOMBI3A_%s_03D_R%02d.pof" % (args["mop_name"].upper(), run+1) for run in range(50)]
    
    pops = []
    lbls = []
    m_objs = None
    
    for i, input_filepath in enumerate(input_filepaths):
        
        # load objs
        objs = load_objs(input_filepath)
        
        pop_size, m_objs = objs.shape
    
        pops.append(objs.copy())
        lbls.append("run %d" % i)
    
    # real front
    front = get_front(args["mop_name"], m_objs)
    p = None
    
    if args["mop_name"] in ("dtlz1", "dtlz2", "dtlz3", "inv-dtlz1"):
        front, p = get_real_front(args["mop_name"], m_objs)
    
    lower_lims = np.zeros((m_objs, ))
    upper_lims = front.max(axis=0)*5.1
    
    # plot objs
    if m_objs == 3:
        
        load_rcparams((10, 8))
        title = input_filepath
        plot_pops(pops, lbls, title=title, real_front=front, p=p, lower_lims=lower_lims, upper_lims=upper_lims)
    
    else:
        
        
        ymin = np.min(front.min(axis=0))
        ymax = np.max(front.max(axis=0))
        
        pops = (front, objs)
        lbls = ("front", "pop")
        
        load_rcparams((10, 8))
        title = input_filepath
        pc(pops, lbls, title=title, ylims=(ymin, ymax))
