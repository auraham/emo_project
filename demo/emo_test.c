/*
emo_test.c

run:
./emo_test zdt1 input/Param_02D.cfg input_zdt1_m_2.txt

./emo_test default input/Param_02D.cfg input_default_m_2.txt
 

for maf:
./emo_test default input/Param_10D_test.cfg input_maf_m_10.txt

for inv_dtlz1:
./emo_test default input/Param_10D_test.cfg input_inv_dtlz1_m_10.txt

note: make sure that:
    1. the number of lines in input_maf_m_10.txt and
    2. the parameter psize in Param_10D_test.cfg IS THE SAME.
*/
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "emo.h"

// it is already defined in emo.h
//#define PI 3.14159265358979311599796346854418516159057617187500

/* Definition of a new Multi-objective Optimization Problem (MOP) 

    minimize   {f_0(x), f_1(x), ..., f_{mop->nobj-1}(x)}

    subject to g_0(x) >= 0
               g_1(x) >= 0
               ...
               g_{mop->ncon - 1}(x) >= 0

               x \in [x_min, x_max]

 f: vector of objective functions
 g: vector of inequality constraints
 x: vector of decision variables

 Note: the following function prototype should be defined for a 
       MOP with inequality constraints:

 void myMOP_eval(EMO_MOP *mop, double *f, double *g, double *x)

 A non feasible solution is that one which for any i g_i(x) < 0.

*/
void myMOP_eval(EMO_MOP *mop, double *f, double *x) {
    f[0] = x[0] * x[0];
    f[1] = pow(x[0] - 1.0, 2.0);
    
    f[0] = 0;
    f[1] = 1;
}

void myMOP_alloc(EMO_MOP *mop) {
    int i;

    /* MOP's name */
    mop->name = (char *) malloc(sizeof(char) * 200);
    strcpy(mop->name, "WING_DESIGN");

    /* Number of decision variables */
    mop->nvar = 2;

    /* Number of objectives */
    mop->nobj = 2;

    /* Box constraints */
    mop->xmin = (double *) calloc(sizeof(double), mop->nvar);
    mop->xmax = (double *) calloc(sizeof(double), mop->nvar);

    for(i = 0; i < mop->nvar; i++) {
        mop->xmin[i] = 0.0;
        mop->xmax[i] = 1.0;
    }

    /* Required */
    mop->npos = 0;
    mop->feval = 0;
    mop->coding = EMO_REAL;

    /* MOP without inequality constraints */
    mop->ncon = 0;
    mop->f = myMOP_eval;
}

/* ---- maf1 ---- */

void maf1_eval(EMO_MOP *mop, double *f, double *x) {
    // This code is based on MAF01.java
    // See https://github.com/ranchengcn/IEEE-CEC-MaOO-Competition

    // variables
    double g=0, subf1=1, subf3;
    int i, j;
    int m_objs = mop->nobj;
    int n_vars = mop->nvar;
    
    // evaluate g(xm)
    for ( j = m_objs-1 ; j < n_vars; j++ )
        g += (pow(x[j] - 0.5, 2));
    subf3 = 1 + g;
        
    
    // evaluate objectives
    f[m_objs-1] = x[0]*subf3;
    for (i = m_objs-2 ; i > 0 ; i-- ) {
        subf1 *= x[m_objs-i-2];
        f[i] = subf3*(1-subf1*(1-x[m_objs-i-1]));
    }
    f[0] = (1 - subf1*x[m_objs-2]) * subf3;

    
    // debug
    for (i=0; i<m_objs; i++) {
        
        if (i == m_objs - 1)
            printf("%.5f", f[i]);
        else
            printf("%.5f ", f[i]);
    }
    printf("\n");
}

void maf1_alloc(EMO_MOP *mop) {
    // allocate maf1 assuming m=10 objs and n=19 vars
    
    int i;

    /* MOP's name */
    mop->name = (char *) malloc(sizeof(char) * 200);
    strcpy(mop->name, "maf1");

    /* Number of objectives */
    mop->nobj = 10;
    
    /* Number of decision variables */
    mop->nvar = mop->nobj + 10 - 1;         // m + k - 1

    /* Box constraints */
    mop->xmin = (double *) calloc(sizeof(double), mop->nvar);
    mop->xmax = (double *) calloc(sizeof(double), mop->nvar);

    for(i = 0; i < mop->nvar; i++) {
        mop->xmin[i] = 0.0;
        mop->xmax[i] = 1.0;
    }

    /* Required */
    mop->npos = 0;
    mop->feval = 0;
    mop->coding = EMO_REAL;

    /* MOP without inequality constraints */
    mop->ncon = 0;
    mop->f = maf1_eval;
}

/* ---- maf2 ---- */

void maf2_eval(EMO_MOP *mop, double *f, double *x) {
    // This code is based on MAF02.java
    // See https://github.com/ranchengcn/IEEE-CEC-MaOO-Competition

    // variables
    int i, j;
    int m_objs = mop->nobj;
    int n_vars = mop->nvar;
    double *g = (double *) malloc(sizeof(double)*m_objs);
    double *thet = (double *) malloc(sizeof(double)*(m_objs-1));
    int lb, ub;
    int const2 = (int) floor((n_vars-m_objs+1)/(double)m_objs);
    double subf1=1, subf2, subf3;
    
    // init
    // this is importat for updating g
    for (i = 0 ; i < m_objs ; i++ )
        g[i] = 0;
    for (i = 0 ; i < m_objs-1 ; i++ )
        thet[i] = 0;
    
    // evaluate g, thet
    for (i = 0 ; i < m_objs-1 ; i++ ) {
        g[i] = 0;
        lb = m_objs + i * const2;
        ub = m_objs + (i+1) * const2 - 1;
        
        for ( j = lb-1 ; j < ub ; j++ )
            g[i] += pow(x[j]/2-0.25, 2);

        thet[i] = PI/2*(x[i]/2+0.25);
    }
    
    lb = m_objs + (m_objs-1)*const2;
    ub = n_vars;
    
    for ( j =lb-1 ; j<ub; j++) {
        double res = pow(x[j]/2-0.25, 2);
        g[m_objs-1] += res;
    }
    
    // evaluate fm, fm-1, ..., 2, f1
    f[m_objs-1] = sin(thet[0]) * (1 + g[m_objs-1]);
    
    // fi=cos(thet1)cos(thet2) ...
    for (i=m_objs-2; i>0; i--) {
        subf1 *= cos(thet[m_objs-i-2]);
        f[i] = subf1*sin(thet[m_objs-i-1])*(1+g[i]);
    }
    f[0] = subf1*cos(thet[m_objs-2]) * (1+g[0]);
    
    // debug
    for (i=0; i<m_objs; i++) {
        
        if (i == m_objs - 1)
            printf("%.5f", f[i]);
        else
            printf("%.5f ", f[i]);
    }
    printf("\n");
    
    free(g);
    free(thet);
}

void maf2_alloc(EMO_MOP *mop) {
    // allocate maf1 assuming m=10 objs and n=19 vars
    
    int i;

    /* MOP's name */
    mop->name = (char *) malloc(sizeof(char) * 200);
    strcpy(mop->name, "maf2");

    /* Number of objectives */
    mop->nobj = 10;
    
    /* Number of decision variables */
    mop->nvar = mop->nobj + 10 - 1;         // m + k - 1

    /* Box constraints */
    mop->xmin = (double *) calloc(sizeof(double), mop->nvar);
    mop->xmax = (double *) calloc(sizeof(double), mop->nvar);

    for(i = 0; i < mop->nvar; i++) {
        mop->xmin[i] = 0.0;
        mop->xmax[i] = 1.0;
    }

    /* Required */
    mop->npos = 0;
    mop->feval = 0;
    mop->coding = EMO_REAL;

    /* MOP without inequality constraints */
    mop->ncon = 0;
    mop->f = maf2_eval;
}

/* ---- maf3 ---- */

void maf3_eval(EMO_MOP *mop, double *f, double *x) {
    // This code is based on MAF03.java
    // See https://github.com/ranchengcn/IEEE-CEC-MaOO-Competition

    // variables
    int i, j;
    int m_objs = mop->nobj;
    int n_vars = mop->nvar;
    double g = 0;
    double subf1=1, subf3;
    
    // evaluate g
    for (i=m_objs-1; i<n_vars; i++)
        g += pow(x[i]-0.5, 2) - cos(20*PI*(x[i]-0.5));
    g = 100*(n_vars - m_objs + 1 + g);
    
    subf3 = 1 + g;
    
    // evaluate fm, fm-1, ..., 2, f1
    f[m_objs-1] = pow(sin(PI*x[0]/2)*subf3, 2);
    
    // f = (subf1*subf2*subf3)^4
    for (i=m_objs-2; i>0; i--) {
        subf1 *= cos(PI*x[m_objs-i-2]/2);
        f[i] = pow(subf1*sin(PI*x[m_objs-i-1]/2)*subf3, 4);
    }
    f[0] = pow(subf1*cos(PI*x[m_objs-2]/2)*subf3, 4);
    
    // debug
    for (i=0; i<m_objs; i++) {
        
        if (i == m_objs - 1)
            printf("%.5f", f[i]);
        else
            printf("%.5f ", f[i]);
    }
    printf("\n");
    
}

void maf3_alloc(EMO_MOP *mop) {
    // allocate maf1 assuming m=10 objs and n=19 vars
    
    int i;

    /* MOP's name */
    mop->name = (char *) malloc(sizeof(char) * 200);
    strcpy(mop->name, "maf3");

    /* Number of objectives */
    mop->nobj = 10;
    
    /* Number of decision variables */
    mop->nvar = mop->nobj + 10 - 1;         // m + k - 1

    /* Box constraints */
    mop->xmin = (double *) calloc(sizeof(double), mop->nvar);
    mop->xmax = (double *) calloc(sizeof(double), mop->nvar);

    for(i = 0; i < mop->nvar; i++) {
        mop->xmin[i] = 0.0;
        mop->xmax[i] = 1.0;
    }

    /* Required */
    mop->npos = 0;
    mop->feval = 0;
    mop->coding = EMO_REAL;

    /* MOP without inequality constraints */
    mop->ncon = 0;
    mop->f = maf3_eval;
}

/* ---- maf4 ---- */

void maf4_eval(EMO_MOP *mop, double *f, double *x) {
    // This code is based on MAF03.java
    // See https://github.com/ranchengcn/IEEE-CEC-MaOO-Competition

    // variables
    int i, j;
    int m_objs = mop->nobj;
    int n_vars = mop->nvar;
    double g = 0;
    double subf1=1, subf3;
    
    // init
    double *const4 = (double*) malloc(sizeof(double)*m_objs);
    for (i=0; i<m_objs; i++)
        const4[i] = pow(2, i+1);
    
    // evaluate g
    for (i=m_objs-1; i<n_vars; i++)
        g += pow(x[i]-0.5, 2) - cos(20*PI*(x[i]-0.5));
    g = 100*(n_vars - m_objs + 1 + g);
    
    subf3 = 1 + g;
    
    // evaluate fm, fm-1, ..., 2, f1
    f[m_objs-1] = const4[m_objs-1]*(1 - sin(PI*x[0]/2))*subf3;
    
    // fi = 2^i*(1-subf1*subf2)*(subf3)
    for (i=m_objs-2; i>0; i--) {
        subf1 *= cos(PI*x[m_objs-i-2]/2);
        f[i] = const4[i]*(1-subf1*sin(PI*x[m_objs-i-1]/2))*subf3;
    }
    f[0] = const4[0]*(1-subf1*cos(PI*x[m_objs-2]/2))*subf3;
    
    // debug
    for (i=0; i<m_objs; i++) {
        
        if (i == m_objs - 1)
            printf("%.5f", f[i]);
        else
            printf("%.5f ", f[i]);
    }
    printf("\n");
    
    free(const4);
}

void maf4_alloc(EMO_MOP *mop) {
    // allocate maf1 assuming m=10 objs and n=19 vars
    
    int i;

    /* MOP's name */
    mop->name = (char *) malloc(sizeof(char) * 200);
    strcpy(mop->name, "maf4");

    /* Number of objectives */
    mop->nobj = 10;
    
    /* Number of decision variables */
    mop->nvar = mop->nobj + 10 - 1;         // m + k - 1

    /* Box constraints */
    mop->xmin = (double *) calloc(sizeof(double), mop->nvar);
    mop->xmax = (double *) calloc(sizeof(double), mop->nvar);

    for(i = 0; i < mop->nvar; i++) {
        mop->xmin[i] = 0.0;
        mop->xmax[i] = 1.0;
    }

    /* Required */
    mop->npos = 0;
    mop->feval = 0;
    mop->coding = EMO_REAL;

    /* MOP without inequality constraints */
    mop->ncon = 0;
    mop->f = maf4_eval;
}

/* ---- maf5 ---- */

void maf5_eval(EMO_MOP *mop, double *f, double *x) {
    // This code is based on MAF03.java
    // See https://github.com/ranchengcn/IEEE-CEC-MaOO-Competition

    // variables
    int i, j;
    int m_objs = mop->nobj;
    int n_vars = mop->nvar;
    double g = 0;
    double subf1=1, subf3;
    
    // init
    double *const5 = (double*) malloc(sizeof(double)*m_objs);
    for (i=0; i<m_objs; i++)
        const5[i] = pow(2, i+1);
    
    // evaluate g
    for (i=m_objs-1; i<n_vars; i++)
        g += pow(x[i]-0.5, 2);
    
    subf3 = 1 + g;
    
    // evaluate fm, fm-1, ..., 2, f1
    f[m_objs-1] = 2*pow(sin(PI*pow(x[0],100)/2)*subf3, 1);
    
    // fi = 2^i*(1-subf1*subf2)*(subf3)
    for (i=m_objs-2; i>0; i--) {
        subf1 *= cos(PI*pow(x[m_objs-i-2],100)/2);
        f[i] = const5[m_objs-i-1]*pow(subf1*sin(PI*pow(x[m_objs-i-1],100)/2)*subf3, 1);
    }
    f[0] = const5[m_objs-1]*pow(subf1*(cos(PI*pow(x[m_objs-2],100)/2))*subf3, 1);
    
    // debug
    for (i=0; i<m_objs; i++) {
        
        if (i == m_objs - 1)
            printf("%.5f", f[i]);
        else
            printf("%.5f ", f[i]);
    }
    printf("\n");
    
    free(const5);
}

void maf5_alloc(EMO_MOP *mop) {
    // allocate maf1 assuming m=10 objs and n=19 vars
    
    int i;

    /* MOP's name */
    mop->name = (char *) malloc(sizeof(char) * 200);
    strcpy(mop->name, "maf5");

    /* Number of objectives */
    mop->nobj = 10;
    
    /* Number of decision variables */
    mop->nvar = mop->nobj + 10 - 1;         // m + k - 1

    /* Box constraints */
    mop->xmin = (double *) calloc(sizeof(double), mop->nvar);
    mop->xmax = (double *) calloc(sizeof(double), mop->nvar);

    for(i = 0; i < mop->nvar; i++) {
        mop->xmin[i] = 0.0;
        mop->xmax[i] = 1.0;
    }

    /* Required */
    mop->npos = 0;
    mop->feval = 0;
    mop->coding = EMO_REAL;

    /* MOP without inequality constraints */
    mop->ncon = 0;
    mop->f = maf5_eval;
}

/* ---- inv-dtlz1 ---- */

void inv_dtlz1_eval(EMO_MOP *mop, double *f, double *x) {
    
    // variables
    int i, j, t, c;
    int m_objs = mop->nobj;
    int n_vars = mop->nvar;
    double g, v;

    g = 0.0;
    c = mop->nvar - mop->nobj + 1;

    for(i = mop->nvar - c; i < mop->nvar; i++)
        g += pow(x[i] - 0.5, 2.0) - cos(20.0 * PI * (x[i] - 0.5));

    g = 100.0 * (c + g);
    v = 0.5 * (1.0 + g);

    for(i = 0; i < mop->nobj; i++) {
            f[i] = v;
            t = mop->nobj - i - 1; 

        for(j = 0; j < t; j++) 
            f[i] *= x[j];

        if(t < mop->nobj - 1) 
            f[i] *= (1.0 - x[t]);
    }
    
    // inv-dtlz1
    for (i=0; i<m_objs; i++)
        f[i] = (0.5*(1+g)) - f[i];
    
          
    // debug
    for (i=0; i<m_objs; i++) {
        
        if (i == m_objs - 1)
            printf("%.5f", f[i]);
        else
            printf("%.5f ", f[i]);
    }
    printf("\n");
    
}

void inv_dtlz1_alloc(EMO_MOP *mop) {
    // allocate maf1 assuming m=10 objs and n=19 vars
    
    int i;

    /* MOP's name */
    mop->name = (char *) malloc(sizeof(char) * 200);
    strcpy(mop->name, "inv-dtlz1");

    /* Number of objectives */
    mop->nobj = 10;
    
    /* Number of decision variables */
    mop->nvar = mop->nobj + 5 - 1;         // m + k - 1, where k=5

    /* Box constraints */
    mop->xmin = (double *) calloc(sizeof(double), mop->nvar);
    mop->xmax = (double *) calloc(sizeof(double), mop->nvar);

    for(i = 0; i < mop->nvar; i++) {
        mop->xmin[i] = 0.0;
        mop->xmax[i] = 1.0;
    }

    /* Required */
    mop->npos = 0;
    mop->feval = 0;
    mop->coding = EMO_REAL;

    /* MOP without inequality constraints */
    mop->ncon = 0;
    mop->f = inv_dtlz1_eval;
}


int main(int argc, char **argv) {
    
    EMO_Population pop;
    EMO_Param param;
    EMO_MOEA moea;
    EMO_MOP mop;
    int i, j, k;
    int rows, cols, start, size;
    char mop_name[100];
    char input_config[100];
    char input_matrix[100];
    char output_matrix[100];
    char algorithm[100];

    if(argc != 4) {
        //printf("\nSyntax: %s MOEA parameter_file {MOP, default} runs\n\n", argv[0]);
        printf("\nSyntax: %s mop_name parameter_file input_matrix\n\n", argv[0]);
        EMO_Dictionary_print(stdout, EMO_MOEA_list, "MOEA");
        EMO_Dictionary_print(stdout, EMO_Benchmark_list, "\nMOP");
        EMO_Dictionary_print(stdout, EMO_Benchmark_listc, "\nConstrained MOP");
        printf("\nWhen the default option is selected, the MOEA solves the problem defined in %s.c:myMOP_eval\n\n", argv[0]);
        return 1;
    }
    
    // get params
    sprintf(mop_name, "%s", argv[1]);
    sprintf(input_config, "%s", argv[2]);
    sprintf(input_matrix, "%s", argv[3]);
    sprintf(algorithm, "%s", "NSGA2");
    
    // debug
    printf("mop:           %s\n", mop_name);
    printf("config:        %s\n", input_config);
    printf("input matrix:  %s\n", input_matrix);

    if(strcmp(mop_name, "default") == 0) {
        //myMOP_alloc(&mop);
        //maf1_alloc(&mop);
        //maf2_alloc(&mop);
        //maf3_alloc(&mop);
        //maf4_alloc(&mop);
        //maf5_alloc(&mop);
        inv_dtlz1_alloc(&mop);
    }
        
  
    // read parameters from file
    // EMO_Param_alloc_from_file(EMO_Param *param, EMO_MOP *mop, char *alg, char *file, char *problem)
    // This function assigns pop_size in param
    EMO_Param_alloc_from_file(&param, &mop, algorithm, input_config, mop_name);

    // allocate population
    // EMO_MOEA_alloc(EMO_MOEA *moea, EMO_Param *param, EMO_Population *pop, EMO_MOP *mop, const char *str)
    EMO_MOEA_alloc(&moea, &param, &pop, &mop, algorithm);

    // init additional flags
    EMO_Stop_start(param.stop);

    // create initial population randomly
    // evaluate initial population
    EMO_Population_init(&pop, param.rand, &mop);

    // read input file
    rows = 0;           // use zero, its value will be defined in EMO_File_read
    cols = 0;           // use zero, its value will be defined in EMO_File_read
    start = 0;
    double *data = NULL;
    data = EMO_File_read(data, &rows, &cols, input_matrix, start);
    printf("reading input %s\n", input_matrix);

    // copy population from file
    // snippet from common.c:EMO_Population_init()
    for(i = 0; i < pop.mu; i++) {
        k = i * mop.nvar;

        for(j = 0; j < mop.nvar; j++)
            pop.var[k + j] = data[k + j];
    }
    
    // evaluate population
    start = 0;
    size = pop.mu;
    EMO_Population_evaluate(&pop, &mop, start, size);
    
    // write objs file
    rows = pop.mu;
    cols = mop.nobj;
    sprintf(output_matrix, "output_objs_%s_m_%d.txt", mop_name, mop.nobj);
    EMO_File_write(pop.obj, NULL, rows, cols, output_matrix, "%.30f ", 0);
    printf("output: %s\n", output_matrix);
    
    // write chroms file (optional)
    /*rows = pop.mu;
    cols = mop.nvar;
    sprintf(output_matrix, "output_chroms_%s_m_%d.txt", mop_name, mop.nobj);
    EMO_File_write(pop.var, NULL, rows, cols, output, "%f ", 0);
    printf("output: %s\n", output_matrix);
    */
    
    // deallocate memory
    EMO_MOEA_free(&moea);
    EMO_Param_free(&param);     // free MOP
    EMO_Population_free(&pop);
    
    return 0;
}

