# About this package

This package contains a few utilities to generate tables.



## Baselines table

This table shows the performance of MSS, MOMBI-III A (it uses the same fevs as MSS) and several configurations of MOEA/D, specifically:

```python
# define sorted moea_names
moea_names = (

    # moead + normalization
    #(baseline_path, "moead_norm_v1-ws-norm-true",    "WS"), 
    (baseline_path, "moead_norm_v1-te-norm-true",    "TE"),
    (baseline_path, "moead_norm_v1-asf-norm-true",   "MTE"),      # ASF
    (baseline_path, "moead_norm_v1-pbi-norm-true",   "PBI"),
    #(baseline_path, "moead_norm_v1-ipbi-norm-true",  "IPBI"),    # IGD in MaF3 is too large
    (baseline_path, "moead_norm_v1-vads-norm-true",  "VADS"),

    # mombi3a
    (mombi3a_path, "MOMBI3A",  "MOMBI-III A"),

    # mss + normalization
    (proposal_path, "mss-dci_norm_hyp_mean-divs-8-25-mean-10-ps-normal-ss-gra-norm-true",  "MSS-DCI"),
)
```

Run these commands (case `a` only):

```
python3 paper_table_mss_baselines.py -l a -i hv
```

This table shows the `mean (sd)` of the performance indicator for each row. The best value is highlighted in **bold** and the `best_k=3` values are highlighted with a gray background.








