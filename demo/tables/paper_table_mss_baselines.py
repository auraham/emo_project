# paper_table_mss_baselines.py
# python3 paper_table_mss_baselines.py -l a -i hv
from __future__ import print_function
import numpy as np
import os, sys, argparse

np.seterr(all='raise')

# add lab's path to sys.path
lab_path = os.path.abspath("/home/auraham/git/moead_mss_dci")
sys.path.insert(0, lab_path)

from stats_table import table_frontend_paper
from rocket.labs import get_mop_configs

if __name__ == "__main__":
    
    ap = argparse.ArgumentParser()
    ap.add_argument("-l", "--case", required=True, help="case", choices=("a", "b"))
    ap.add_argument("-i", "--indicator", required=True, help="performance indicator", choices=("igd", "hv", "sp"))
    args = vars(ap.parse_args())
    
    # -----

    # {m_objs:fevals}
    main_budget = {      
        "dtlz1"     : {3: 300, 5: 500, 8: 800, 10: 1000},
        "dtlz2"     : {3: 300, 5: 500, 8: 800, 10: 1000},
        "dtlz3"     : {3: 300, 5: 500, 8: 800, 10: 1000},
        "inv-dtlz1" : {3: 300, 5: 500, 8: 800, 10: 1000},
        "maf1"      : {3: 300, 5: 500, 8: 800, 10: 1000},
        "maf2"      : {3: 300, 5: 500, 8: 800, 10: 1000},
        "maf3"      : {3: 300, 5: 500, 8: 800, 10: 1000},
        "maf4"      : {3: 300, 5: 500, 8: 800, 10: 1000},
        "maf5"      : {3: 300, 5: 500, 8: 800, 10: 1000},
    }
    
    runs = 50       # number of independent runs
    step = 100      # save pop every step generations for posterior hv calculation
    mop_configs = get_mop_configs(runs, step, main_budget)

    # results from gitlab
    baseline_path = "/media/data/git/moead_mss_dci_results/experiments/test_moead_norm_v1/results_case_%s_git" % args["case"]
    mombi3_path   = "/home/auraham/git/emo_project/demo/output_paper"
    proposal_path = "/media/data/git/moead_mss_dci_results/experiments/paper_test_mss_contrib/results_case_%s_git" % args["case"]
    
    # control method (our proposal)
    control = "mss-dci_norm_hyp_mean-divs-8-25-mean-10-ps-normal-ss-gra-norm-true"
    
    # define sorted moea_names
    moea_names = (
                
                # moead + normalization (we comment out ws, mte, ipbi for saving space in paper)
                #(baseline_path, "moead_norm_v1-ws-norm-true",    "WS"), 
                (baseline_path, "moead_norm_v1-te-norm-true",    "TE"),
                #(baseline_path, "moead_norm_v1-asf-norm-true",   "MTE"),      # ASF
                (baseline_path, "moead_norm_v1-pbi-norm-true",   "PBI"),
                #(baseline_path, "moead_norm_v1-ipbi-norm-true",  "IPBI"),
                (baseline_path, "moead_norm_v1-vads-norm-true",  "VADS"),
                
                # mombi3a
                (mombi3_path, "MOMBI3",  "MOMBI-III"),
                (mombi3_path, "MOMBI3C", "MOMBI-III C"),
                
                # mss + normalization
                (proposal_path, control,  "MSS-DCI"),
                )
    
    # sorted mop_names
    mop_names = (
            ("dtlz1", "DTLZ1"),
            ("dtlz2", "DTLZ2"),
            ("dtlz3", "DTLZ3"),
            ("inv-dtlz1", "inv-DTLZ1"),
            ("maf1", "MaF1"),
            ("maf2", "MaF2"),
            ("maf3", "MaF3"),
            ("maf4", "MaF4"),
            ("maf5", "MaF5"),
            )                     
    
    # create header
    print("# table for case %s" % args["case"])
    table_frontend_paper(control, mop_configs, mop_names, moea_names, indicator=args["indicator"])
