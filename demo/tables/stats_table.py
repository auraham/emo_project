# tables_core.py
from __future__ import print_function
import numpy as np
import pandas as pd
import os, sys, argparse

np.seterr(all='raise')

# add lab's path to sys.path
lab_path = os.path.abspath("../../")
sys.path.insert(0, lab_path)

from rocket.performance import get_igd_series
from rocket.performance import get_sp_series
from rocket.performance import get_hv_last_generation
from rocket.statistics import statistical_analysis
       
def print_latex_title(moea_names):
    
    moeas = " & ".join([moea_label for _experiment_path, moea_name, moea_label in moea_names])
    
    line = "Problem & $m$ & %s \\\\" % moeas
    
    print("\\toprule")
    print(line)
    print("\\midrule")
    print("")

    
def highlight_row(values, best_value):
    """
    Compara los valores del arreglo values
    Si todos son iguales a best_value, la bandera hl es igual a False
    Sino, hl es igual a True y to_keep es un arreglo booleano indicando que entrada de values es la que se debe marcar
    
    """
    
    
    hl = True
    n_vals = len(values)
    count = 0
    
    for val in values:
        
        if np.isclose(val, best_value):
            count += 1
            
    if count == n_vals:
        hl = False
        
    return hl

    
def print_latex_block(mop_name, m_objs, block, min_is_better, print_mop_name=True, use_midrule=False, fmt="%.4f"):
    
    rows, cols = block.shape
    
    for i in range(rows):
        
        row_mop_name = mop_name if i == 0 else " "*len(mop_name)        # print mop name in the first row of each block
        row_m_objs   = "%2d" % m_objs if i == 1 else "  "               # print m_objs in the third row of each block
        
        if not print_mop_name:
            row_mop_name = " "*len(mop_name)

        
        # prev
        #values = " & ".join([ fmt % v for v in block[i] ])

        # ---

        values   = ""
        vals     = block[i]
        comp_val = np.min    if min_is_better else np.max
        comp_arg = np.argmin if min_is_better else np.argmax
        best_val = comp_val(block[i])                        # best value    of row block[i]
        best_arg = comp_arg(block[i])                        # best argument of row block[i]
        
        # highlight?
        hl = highlight_row(block[i], best_val)
        
        
        # added for standard deviation
        if i == rows - 1:
            
            str_values = []
            
            for ind, value in enumerate(block[i]):          # recuerda, block[i] es una linea de la tabla
                
                item = "$\\pm$%.4f" % value         # \pm = +- symbol
                
                str_values.append(item)
                
            values = " & ".join(str_values)
        
        elif hl:
        
            # si hl==True, entonces solo un valor se marcara como minimo por fila
            # @todo: add support for more than one min value
        
            str_values = []
        
            """
            block
                      indicator moead   indicator amoead
            array([[  1.52591600e+00,   1.53229100e+00],  best
                   [  1.52515450e+00,   1.52979500e+00],  median
                   [  1.52522090e+00,   1.53001490e+00],  mean
                   [  1.52493600e+00,   1.52899200e+00],  worst
                   [  2.56492930e-04,   7.85181480e-04]]) sd

            """
            
            for ind, value in enumerate(block[i]):          # recuerda, block[i] es una linea de la tabla
                
                item = None
                
                if ind == best_arg:
                    
                    item = "$\\mathbf{%.4f}$" % value       # use $\mathbf{..}$ instead of \\textbf{..} for thesis template
                    
                else:
                    
                    item = "$%.4f$" % value
                    
                    
                str_values.append(item)
                
            values = " & ".join(str_values)
            
        else:
            
            # si hl==False, entonces toda la fila tiene valores iguales, ninguno se marcara
            
            values = " & ".join([ fmt % v for v in block[i] ])

        # ---

        line = " %s & %s & %s \\\\" % (row_mop_name, row_m_objs, values)
        
        print(line)
    
    
    if use_midrule:
        print("\\midrule")
    else:
        span = " "*(len(mop_name)+2)
        n_cols = 2 + block.shape[1]         # numero de cols: problem + m + len(moeas)
        print("\n%s\\cmidrule{%d-%d}" % (span, 2, n_cols))
    
    print("")


def table(mop_configs, mop_names, moea_names, experiment_path, indicator):

    rand = np.random.RandomState(100)

    # print title
    print_latex_title(moea_names)
    
    #for mop_name, config in mop_configs.items():
    for mop_name, mop_label in mop_names:
        
        instances  = mop_configs[mop_name]["budget"]        # 'budget': {3: 600, 4: 1000, 5: 1200},
        min_m_objs = np.min(list(instances.keys())) 
        max_m_objs = np.max(list(instances.keys())) 
        
        for m_objs in sorted(instances.keys()):
            
            iters = mop_configs[mop_name]["budget"][m_objs]
            step  = mop_configs[mop_name]["step"]
            runs  = mop_configs[mop_name]["runs"]
            
            rows = 5                        # best, median, mean, worst, sd values, update: la tabla no cabe en el paper con 5
            rows = 3                        # mean, median, sd values
            cols = len(moea_names)          # number of moeas
            block = np.zeros((rows, cols))
            
            for col_id, (moea_name, moea_label) in enumerate(moea_names):
                
                
                # --- place your data reader here ----
                
                col = None
                min_is_better = None
                
                if indicator == "hv":
                
                    col = get_hv_last_generation(moea_name, mop_name, m_objs, iters, experiment_path)
                    min_is_better = False
                    
                    # corner case for maf2 and maf4
                    if mop_name in ("maf2", "maf4"):
                        col = get_hv_last_generation(moea_name, mop_name, m_objs, iters, experiment_path, return_hv_raw=True)
                
                    """
                    # get hv series                                                                             # for m=3, step=100 and dtlz1, there are runs=50, t_steps=3, for t=100,200,300
                    serie = get_hv_series(moea_name, mop_name, m_objs, iters, step, runs, experiment_path)      # shape: (50, 6) = (runs, t_steps)
                    
                    # shape (50, ) = (runs, )
                    # each value represent the final hv of an independent run
                    hv_last_generation = serie[:, -1]
                    
                    col = hv_last_generation.copy()
                    min_is_better = False
                    """
                    
                elif indicator == "igd":
                    
                    # get igd series                                                                            # for m=3, step=100 and dtlz1, there are runs=50, t_steps=3, for t=100,200,300
                    serie = get_igd_series(moea_name, mop_name, m_objs, iters, step, runs, experiment_path)     # shape: (50, 6) = (runs, t_steps)
                    
                    # shape (50, ) = (runs, )
                    # each value represent the final hv of an independent run
                    igd_last_generation = serie[:, -1]
                    
                    col = igd_last_generation.copy()
                    min_is_better = True
                
                elif indicator == "sp":
                    
                    # get sp series                                                                             # for m=3, step=100 and dtlz1, there are runs=50, t_steps=3, for t=100,200,300
                    serie = get_sp_series(moea_name, mop_name, m_objs, iters, step, runs, experiment_path)      # shape: (50, 6) = (runs, t_steps)
                    
                    # shape (50, ) = (runs, )
                    # each value represent the final hv of an independent run
                    sp_last_generation = serie[:, -1]
                    
                    col = sp_last_generation.copy()
                    min_is_better = False
                
                # --- place your data reader here ----
                
                # save them
                block[0, col_id] = np.mean(col)
                block[1, col_id] = np.median(col)
                block[2, col_id] = np.std(col)
                
            print_mop_name = True if m_objs == min_m_objs else False
            print_midrule  = True if m_objs == max_m_objs else False
            
            print_latex_block(mop_label, m_objs, block, min_is_better, print_mop_name, print_midrule)


    # @todo: fix this
    print("\\bottomrule\n")
    
# -----

     
def print_title_latex(moea_names):
    
    moeas = " & ".join([moea_label for _, moea_name, moea_label in moea_names])
    
    line = "Problem & $m$ & %s \\\\" % moeas
    
    print("\\toprule")
    print(line)
    print("\\midrule")
    print("")


def print_title_markdown(moea_names):
    
    moeas = " | ".join([moea_label for _, moea_name, moea_label in moea_names])
    span  = " | ".join(["-"*(len(moea_label)) for _, moea_name, moea_label in moea_names])
    
    line = "| Problem | $m$ | %s |" % moeas
    
    print(line)
    print( "| ------- | --- | %s |" % span)


def print_title_csv(moea_names):
    
    moeas = ";".join(['%s' % moea_label for _, moea_name, moea_label in moea_names])
    
    # with row_id
    line = 'Row_id;Problem;m;%s' % moeas
    
    # without row_id
    line = 'Problem;m;%s' % moeas
    
    print(line)

  
def print_block_latex(mop_name, m_objs, block, min_is_better, print_mop_name=True, use_midrule=False, fmt="%.4f"):
    
    rows, cols = block.shape
    
    for i in range(rows):
        
        row_mop_name = mop_name if i == 0 else " "*len(mop_name)        # print mop name in the first row of each block
        row_m_objs   = "%2d" % m_objs if i == 1 else "  "               # print m_objs in the third row of each block
        
        if not print_mop_name:
            row_mop_name = " "*len(mop_name)

        
        # prev
        #values = " & ".join([ fmt % v for v in block[i] ])

        # ---

        values   = ""
        vals     = block[i]
        comp_val = np.min    if min_is_better else np.max
        comp_arg = np.argmin if min_is_better else np.argmax
        best_val = comp_val(block[i])                        # best value    of row block[i]
        best_arg = comp_arg(block[i])                        # best argument of row block[i]
        
        # highlight?
        hl = highlight_row(block[i], best_val)
        
        
        # added for standard deviation
        if i == rows - 1:
            
            str_values = []
            
            for ind, value in enumerate(block[i]):          # recuerda, block[i] es una linea de la tabla
                
                item = "$\\pm$%.4f" % value         # \pm = +- symbol
                
                str_values.append(item)
                
            values = " & ".join(str_values)
        
        elif hl:
        
            # si hl==True, entonces solo un valor se marcara como minimo por fila
            # @todo: add support for more than one min value
        
            str_values = []
        
            """
            block
                      indicator moead   indicator amoead
            array([[  1.52591600e+00,   1.53229100e+00],  best
                   [  1.52515450e+00,   1.52979500e+00],  median
                   [  1.52522090e+00,   1.53001490e+00],  mean
                   [  1.52493600e+00,   1.52899200e+00],  worst
                   [  2.56492930e-04,   7.85181480e-04]]) sd

            """
            
            for ind, value in enumerate(block[i]):          # recuerda, block[i] es una linea de la tabla
                
                item = None
                
                if ind == best_arg:
                    
                    item = "$\\mathbf{%.4f}$" % value       # use $\mathbf{..}$ instead of \\textbf{..} for thesis template
                    
                else:
                    
                    item = "$%.4f$" % value
                    
                    
                str_values.append(item)
                
            values = " & ".join(str_values)
            
        else:
            
            # si hl==False, entonces toda la fila tiene valores iguales, ninguno se marcara
            
            values = " & ".join([ fmt % v for v in block[i] ])

        # ---

        line = " %s & %s & %s \\\\" % (row_mop_name, row_m_objs, values)
        
        print(line)
    
    
    if use_midrule:
        print("\\midrule")
    else:
        span = " "*(len(mop_name)+2)
        n_cols = 2 + block.shape[1]         # numero de cols: problem + m + len(moeas)
        print("\n%s\\cmidrule{%d-%d}" % (span, 2, n_cols))
    
    print("")

  
def print_block_markdown(mop_name, m_objs, block, min_is_better, fmt="%.4f"):
    
    rows, cols = block.shape
    
    for i in range(rows):
        
        row_mop_name = mop_name        
        row_m_objs   = "%2d" % m_objs

        # ---

        values   = ""
        vals     = block[i]
        comp_val = np.min    if min_is_better else np.max
        comp_arg = np.argmin if min_is_better else np.argmax
        best_val = comp_val(block[i])                        # best value    of row block[i]
        best_arg = comp_arg(block[i])                        # best argument of row block[i]
        
        # highlight?
        hl = highlight_row(block[i], best_val)
        
        # added for standard deviation
        if i == rows - 1:
            
            str_values = []
            
            for ind, value in enumerate(block[i]):          # recuerda, block[i] es una linea de la tabla
                
                item = "$\\pm$%.4f " % value         # \pm = +- symbol
                
                str_values.append(item)
                
            values = " | ".join(str_values)
        
        elif hl:
        
            # si hl==True, entonces solo un valor se marcara como minimo por fila
            # @todo: add support for more than one min value
        
            str_values = []
        
            """
            block
                      indicator moead   indicator amoead
            array([[  1.52591600e+00,   1.53229100e+00],  best
                   [  1.52515450e+00,   1.52979500e+00],  median
                   [  1.52522090e+00,   1.53001490e+00],  mean
                   [  1.52493600e+00,   1.52899200e+00],  worst
                   [  2.56492930e-04,   7.85181480e-04]]) sd

            """
            
            for ind, value in enumerate(block[i]):          # recuerda, block[i] es una linea de la tabla
                
                item = None
                
                if ind == best_arg:
                    
                    item = " **%.4f** " % value       # use $\mathbf{..}$ instead of \\textbf{..} for thesis template
                    
                else:
                    
                    item = "   %.4f   " % value
                    
                    
                str_values.append(item)
                
            values = " | ".join(str_values)
            
        else:
            
            # si hl==False, entonces toda la fila tiene valores iguales, ninguno se marcara
            
            values = " | ".join([ fmt % v for v in block[i] ])

        # ---

        line = "| %s | %s | %s |" % (row_mop_name, row_m_objs, values)
        
        print(line)

  
def print_block_csv(mop_name, m_objs, block, min_is_better, fmt="%.4f", counter=0):
    
    rows, cols = block.shape
    count = counter
    
    for i in range(rows):
        
        row_mop_name = "%s" % mop_name        
        row_m_objs   = "%d" % m_objs

        # ---

        values   = ""
        vals     = block[i]
        comp_val = np.min    if min_is_better else np.max
        comp_arg = np.argmin if min_is_better else np.argmax
        best_val = comp_val(block[i])                        # best value    of row block[i]
        best_arg = comp_arg(block[i])                        # best argument of row block[i]
        
        # highlight?
        hl = highlight_row(block[i], best_val)
        
        # added for standard deviation
        if i == rows - 1:
            
            str_values = []
            
            for ind, value in enumerate(block[i]):          # recuerda, block[i] es una linea de la tabla
                
                item = "%.10f" % value 
                
                str_values.append(item)
                
            values = ";".join(str_values)
        
        elif hl:
        
            # si hl==True, entonces solo un valor se marcara como minimo por fila
            # @todo: add support for more than one min value
        
            str_values = []
        
            """
            block
                      indicator moead   indicator amoead
            array([[  1.52591600e+00,   1.53229100e+00],  best
                   [  1.52515450e+00,   1.52979500e+00],  median
                   [  1.52522090e+00,   1.53001490e+00],  mean
                   [  1.52493600e+00,   1.52899200e+00],  worst
                   [  2.56492930e-04,   7.85181480e-04]]) sd

            """
            
            for ind, value in enumerate(block[i]):          # recuerda, block[i] es una linea de la tabla
                
                item = None
                
                if ind == best_arg:
                    
                    item = "%.10f" % value       # use $\mathbf{..}$ instead of \\textbf{..} for thesis template
                    
                else:
                    
                    item = "%.10f" % value
                    
                    
                str_values.append(item)
                
            values = ";".join(str_values)
            
        else:
            
            # si hl==False, entonces toda la fila tiene valores iguales, ninguno se marcara
            
            values = " ; ".join([ fmt % v for v in block[i] ])

        # ---

        # with row_id
        #line = "%d;%s;%s;%s" % (count, row_mop_name, row_m_objs, values)
        
        # without row_id
        line = "%s;%s;%s" % (row_mop_name, row_m_objs, values)
        
        count+=1
        
        print(line)

    
def table_frontend(mop_configs, mop_names, moea_names, indicator, fmt="latex", stat="mean"):
    """
    Print a table in three formats: markdown, latex, csv
    """

    rand = np.random.RandomState(100)

    # print title
    if fmt == "latex":
        print_title_latex(moea_names)
    
    if fmt == "markdown":
        print_title_markdown(moea_names)
    
    if fmt == "csv":
        print_title_csv(moea_names) 
    
    counter = 0
    
    #for mop_name, config in mop_configs.items():
    for mop_name, mop_label in mop_names:
        
        instances  = mop_configs[mop_name]["budget"]        # 'budget': {3: 600, 4: 1000, 5: 1200},
        min_m_objs = np.min(list(instances.keys())) 
        max_m_objs = np.max(list(instances.keys())) 
        
        for m_objs in sorted(instances.keys()):
            
            iters = mop_configs[mop_name]["budget"][m_objs]
            step  = mop_configs[mop_name]["step"]
            runs  = mop_configs[mop_name]["runs"]
            
            rows = 5                        # best, median, mean, worst, sd values, update: la tabla no cabe en el paper con 5
            rows = 3                        # mean, median, sd values
            cols = len(moea_names)          # number of moeas
            block = np.zeros((rows, cols))
            
            for col_id, (experiment_path, moea_name, moea_label) in enumerate(moea_names):
                
                
                # --- place your data reader here ----
                
                col = None
                min_is_better = None
                
                if indicator == "hv":
                
                    col = get_hv_last_generation(moea_name, mop_name, m_objs, iters, experiment_path)
                    min_is_better = False
                    
                    # corner case for maf2 and maf4
                    if mop_name in ("maf2", "maf4"):
                        col = get_hv_last_generation(moea_name, mop_name, m_objs, iters, experiment_path, return_hv_raw=True)
                
                    """
                    # get hv series                                                                             # for m=3, step=100 and dtlz1, there are runs=50, t_steps=3, for t=100,200,300
                    serie = get_hv_series(moea_name, mop_name, m_objs, iters, step, runs, experiment_path)      # shape: (50, 6) = (runs, t_steps)
                    
                    # shape (50, ) = (runs, )
                    # each value represent the final hv of an independent run
                    hv_last_generation = serie[:, -1]
                    
                    col = hv_last_generation.copy()
                    min_is_better = False
                    """
                    
                elif indicator == "igd":
                    
                    # get igd series                                                                            # for m=3, step=100 and dtlz1, there are runs=50, t_steps=3, for t=100,200,300
                    serie = get_igd_series(moea_name, mop_name, m_objs, iters, step, runs, experiment_path)     # shape: (50, 6) = (runs, t_steps)
                    
                    # shape (50, ) = (runs, )
                    # each value represent the final hv of an independent run
                    igd_last_generation = serie[:, -1]
                    
                    col = igd_last_generation.copy()
                    min_is_better = True
                
                elif indicator == "sp":
                    
                    # get sp series                                                                             # for m=3, step=100 and dtlz1, there are runs=50, t_steps=3, for t=100,200,300
                    serie = get_sp_series(moea_name, mop_name, m_objs, iters, step, runs, experiment_path)      # shape: (50, 6) = (runs, t_steps)
                    
                    # shape (50, ) = (runs, )
                    # each value represent the final hv of an independent run
                    sp_last_generation = serie[:, -1]
                    
                    col = igd_last_generation.copy()
                    min_is_better = False
                
                # --- place your data reader here ----
                
                # save them
                block[0, col_id] = np.mean(col)
                block[1, col_id] = np.median(col)
                block[2, col_id] = np.std(col)
                
            print_mop_name = True if m_objs == min_m_objs else False
            print_midrule  = True if m_objs == max_m_objs else False
            
            if fmt == "latex":
                print_block_latex(mop_label, m_objs, block, min_is_better, print_mop_name, print_midrule)

            if fmt == "markdown":
                print_block_markdown(mop_label, m_objs, block, min_is_better)

            if fmt == "csv":
                
                to_keep = [0]
                
                if stat == "mean":
                    to_keep = [0]
                    
                if stat == "median":
                    to_keep = [1]
                    
                if stat == "sd":
                    to_keep = [2]
                
                block = block[to_keep, :]        # only one stat (mean, median, or sd) to ease analysis in a spreadsheet
                print_block_csv(mop_label, m_objs, block, min_is_better, counter=counter)
                counter += block.shape[0]   # increment counter (number of rows printed)


    if fmt == "latex":
        print("\\bottomrule\n")

# --- for paper ---
    


def print_block_latex_paper(mop_name, m_objs, block, block_tag, list_moea_names, min_is_better, print_mop_name=True, use_midrule=False, fmt="%.4f", best_k=3, tab_summary=None):
    
    rows, cols = block.shape
    
    # -- for loop
    
    row_mop_name = mop_name
    row_m_objs   = m_objs
    
    if not print_mop_name:
        row_mop_name = " "*len(mop_name)
    
    # ---

    values   = ""
    vals     = block[0, :]                  # all the means
    comp_val = np.min     if min_is_better else np.max
    comp_arg = np.argmin  if min_is_better else np.argmax
    best_val = comp_val(vals)                        # best value    of row block[i]
    best_arg = comp_arg(vals)                        # best argument of row block[i]
    best_set = vals.argsort()[:best_k] if min_is_better else vals.argsort()[-best_k:]       # top-k values in row block[i]
    
    tag_dict = {
        "+": " $+$",
        "-": " $-$",
        "~": " $\\approx$",
        "": "",
    }
    
    # highlight?
    hl = highlight_row(vals, best_val)
    
    # added for standard deviation
    if hl:
    
        # si hl==True, entonces solo un valor se marcara como minimo por fila
        # @todo: add support for more than one min value
    
        str_values = []
    
        """
        block
                  indicator moead   indicator amoead
        array([[  1.52591600e+00,   1.53229100e+00],  mean
               [  1.52515450e+00,   1.52979500e+00]])  sd

        """
        
        # ---- new method starts ----
        
        for col_id in range(cols):
            
            # stat symbol (+, -, ~)
            moea_name = list_moea_names[col_id]
            tag = block_tag[moea_name]
            tag = tag_dict[tag]             # converts "+" -> "$+$"
            
            ind      = col_id
            val_mean = block[0, col_id]
            val_sd   = block[1, col_id]
        
            item = None
            
            preffix = "\\cellcolor{gray!50}" if col_id in best_set else ""
            
            if ind == best_arg:
                
                item = "%s$\\mathbf{%.3f}$ (%.3f)%s" % (preffix, val_mean, val_sd, tag)       # use $\mathbf{..}$ instead of \\textbf{..} for thesis template
                
            else:
                
                item = "%s$%.3f$ (%.3f)%s" % (preffix, val_mean, val_sd, tag)
                
                
            str_values.append(item)
            
        values = " & ".join(str_values)
        
        
        # ---- new method ends ----
        
        """
        for ind, value in enumerate(block[i]):          # recuerda, block[i] es una linea de la tabla
            
            item = None
            
            if ind == best_arg:
                
                item = "$\\mathbf{%.4f}$" % value       # use $\mathbf{..}$ instead of \\textbf{..} for thesis template
                
            else:
                
                item = "$%.4f$" % value
                
                
            str_values.append(item)
            
        values = " & ".join(str_values)
        """
        
    else:
        
        # si hl==False, entonces toda la fila tiene valores iguales, ninguno se marcara
        
        values = " & ".join([ fmt % v for v in block[0, :] ])

    # ---

    line = " %s & %s & %s \\\\" % (row_mop_name, row_m_objs, values)
    
    print(line)
    
    # -- for loop ends --
    
    if use_midrule:
        print("\\midrule")
    #else:
    #    span = " "*(len(mop_name)+2)
    #    n_cols = 2 + block.shape[1]         # numero de cols: problem + m + len(moeas)
    #    print("\n%s\\cmidrule{%d-%d}" % (span, 2, n_cols))
    
    print("")

      

def table_frontend_paper(control, mop_configs, mop_names, moea_names, indicator, alpha=0.05):
    """
    Print a table latex format
    Similar to table_frontend(), but this function put all the info [mean (sd)] in a single row instead of two
    """

    rand = np.random.RandomState(100)

    min_is_better = None
    
    # print title
    print_title_latex(moea_names)
    
    #for mop_name, config in mop_configs.items():
    for mop_name, mop_label in mop_names:
        
        instances  = mop_configs[mop_name]["budget"]        # 'budget': {3: 600, 4: 1000, 5: 1200},
        min_m_objs = np.min(list(instances.keys())) 
        max_m_objs = np.max(list(instances.keys())) 
        
        for m_objs in sorted(instances.keys()):
            
            iters = mop_configs[mop_name]["budget"][m_objs]
            step  = mop_configs[mop_name]["step"]
            runs  = mop_configs[mop_name]["runs"]
            
            rows = 2                        # mean and sd values
            cols = len(moea_names)          # number of moeas
            block = np.zeros((rows, cols))
            block_tag = {}                  # holds the +, -, ~ symbols
            list_moea_names = []
            data_dict = {}
            
            for col_id, (experiment_path, moea_name, moea_label) in enumerate(moea_names):
                
                list_moea_names.append(moea_name)
                
                # --- place your data reader here ----
                
                col = None
                
                if indicator == "hv":
                
                    col = get_hv_last_generation(moea_name, mop_name, m_objs, iters, experiment_path)
                    min_is_better = False
                    
                    # corner case for maf2 and maf4
                    if mop_name in ("maf2", "maf4"):
                        col = get_hv_last_generation(moea_name, mop_name, m_objs, iters, experiment_path, return_hv_raw=True)
                
                    """
                    # get hv series                                                                             # for m=3, step=100 and dtlz1, there are runs=50, t_steps=3, for t=100,200,300
                    serie = get_hv_series(moea_name, mop_name, m_objs, iters, step, runs, experiment_path)      # shape: (50, 6) = (runs, t_steps)
                    
                    # shape (50, ) = (runs, )
                    # each value represent the final hv of an independent run
                    hv_last_generation = serie[:, -1]
                    
                    col = hv_last_generation.copy()
                    min_is_better = False
                    """
                    
                elif indicator == "igd":
                    
                    # get igd series                                                                            # for m=3, step=100 and dtlz1, there are runs=50, t_steps=3, for t=100,200,300
                    serie = get_igd_series(moea_name, mop_name, m_objs, iters, step, runs, experiment_path)     # shape: (50, 6) = (runs, t_steps)
                    
                    # shape (50, ) = (runs, )
                    # each value represent the final hv of an independent run
                    igd_last_generation = serie[:, -1]
                    
                    col = igd_last_generation.copy()
                    min_is_better = True
                
                elif indicator == "sp":
                    
                    # get sp series                                                                             # for m=3, step=100 and dtlz1, there are runs=50, t_steps=3, for t=100,200,300
                    serie = get_sp_series(moea_name, mop_name, m_objs, iters, step, runs, experiment_path)      # shape: (50, 6) = (runs, t_steps)
                    
                    # shape (50, ) = (runs, )
                    # each value represent the final hv of an independent run
                    sp_last_generation = serie[:, -1]
                    
                    col = sp_last_generation.copy()
                    min_is_better = False
                
                # --- place your data reader here ----
                
                # save them
                block[0, col_id] = np.mean(col)
                block[1, col_id] = np.std(col)
                
                # update data_dict
                data_dict[moea_name] = col.copy()
                
            # --- create dataframe for stat analysis ---
            
            # read csv
            df_pi = pd.DataFrame(data_dict)
            tab_summary, tab_pvalues, tab_tags = statistical_analysis(
                                                                    df_pi, 
                                                                    control, 
                                                                    alpha, 
                                                                    min_is_better, 
                                                                    display_info=False, 
                                                                    display_boxplot=False)
            
            block_tag = tab_tags
            
            # --- create dataframe for stat analysis ---
            
            
            print_mop_name = True if m_objs == min_m_objs else False
            print_midrule  = True if m_objs == max_m_objs else False
            
            print_block_latex_paper(mop_label, m_objs, block, block_tag, list_moea_names, min_is_better, print_mop_name, print_midrule)
            
            
    print("\\bottomrule\n")
    
    # debug
    print("control:       %s" % control)
    print("alpha:         %0.4f" % alpha)
    print("min_is_better: %s" % min_is_better)


