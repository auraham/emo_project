# create_random_pop.py
import numpy as np
from numpy.random import RandomState

rand = RandomState(100)

pop_size = 100
n_genes = 19
header = " %d %d" % (pop_size, n_genes)
filename = "input_maf_m_10.txt"

data = rand.random_sample((pop_size, n_genes))
np.savetxt(filename, data, fmt="%.10f", comments="#", header=header)
print(filename)
