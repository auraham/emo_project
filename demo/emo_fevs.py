# emo_fevs.py
import numpy as np

# remove this
import sys
sys.path.insert(0, "/home/auraham/git/moead_mss_dci")

from rocket.forms.scalar.vads import vads, vads_matlab
from rocket.forms.scalar.pbi import pbi
from rocket.forms.scalar.ipbi import ipbi
from rocket.forms.scalar.ws import ws
from rocket.forms.scalar.te import te
from rocket.forms.scalar.asf import asf

if __name__ == "__main__":
    
    m_objs = 10
    vads_p = 100
    z_ideal = np.zeros((m_objs, ))
    z_nadir = np.ones((m_objs, ))
    theta = 5
    
    w = np.array([0.0000100000, 0.0000100000, 0.0000100000, 0.0000100000, 0.0000100000, 0.0000100000, 0.0000100000, 0.0000100000, 0.0000100000, 1.0000000000])
    x = np.array([0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0])
    
    # other values
    w = np.array([0.07200801, 0.18278161, 0.14073106, 0.11509637, 0.0299957, 0.02999106, 0.01116699, 0.16652855, 0.11556865, 0.13613201])
    z_ideal = np.ones((m_objs, )) * 0.1
    z_nadir = np.ones((m_objs, )) * 1.2
       
    
    r1 = vads(x, w, vads_p)
    r2 = vads_matlab(x, w, vads_p)
    
    
    print("ws: %.8f" % ws(x, w))                # devuelve la misma salida que emo_project
    print("te: %.8f" % te(x, w, z_ideal))       # devuelve la misma salida que emo_project, aunque la version en python usa z_ideal
    print("asf: %.8f" % asf(x, w, z_ideal))     # devuelve la misma salida que emo_project, aunque la version en python usa z_ideal
    
    r, d1, d2 = pbi(x, w, z_ideal, z_nadir, theta)
    print("pbi: %.8f, d1: %.8f, d2: %.8f" % (r, d1, d2))
    
    r, d1, d2 = ipbi(x, w, z_ideal, z_nadir, theta)
    r = -r
    print("ipbi: %.8f, d1: %.8f, d2: %.8f" % (r, d1, d2))
    
    print("vads: %.8f" % vads(x, w, vads_p))     # devuelve la misma salida que emo_project, aunque la version en python usa z_ideal
    
    
    
