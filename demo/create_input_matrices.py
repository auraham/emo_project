# create_input_matrices.py
from __future__ import print_function
import numpy as np
from numpy.random import RandomState
import os, sys, argparse
np.seterr(all='raise')

# rocket
sys.path.insert(0, "/home/auraham/git/moead_mss_dci")
from rocket.helpers import get_n_genes

if __name__ == "__main__":
    
    for mop in ("maf", "inv_dtlz1"):
    
        for m_objs in (3, 5, 8, 10):
            
            mop_name = "maf1"
            
            if mop == "inv_dtlz1":
                mop_name = "inv-dtlz1"
            
            # get number of variables
            n_genes, _, _ = get_n_genes(mop_name, m_objs)

            rand = RandomState(100)
            pop_size = 10
            header = " %d %d" % (pop_size, n_genes)
            filename = "input_%s_m_%d.txt" % (mop, m_objs)

            data = rand.random_sample((pop_size, n_genes))
            np.savetxt(filename, data, fmt="%.10f", comments="#", header=header)
            print(filename)
