#!/usr/bin/env bash
# validate_mops.sh

for m_objs in 3 5 ; do
    
    for mop_name in maf1 maf2 maf3 maf4 maf5 ; do
    
        # evaluate implementations
        ./emo_test "$mop_name" input/Param_0"$m_objs"D_test.cfg input_maf_m_"$m_objs".txt

        python3 emo_test.py -p "$mop_name" -m "$m_objs" -i input_maf_m_"$m_objs".txt
    
        # comparison
        colordiff output_objs_"$mop_name"_m_"$m_objs".txt output_objs_"$mop_name"_m_"$m_objs"_python.txt
    
    done
done
