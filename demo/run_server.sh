#!/usr/bin/env bash
# 
# usage
# ./run_server.sh moea_name mop_name m_objs
# 
# example
# ./run_server.sh MOMBI3 dtlz1 03
# ./run_server.sh MOMBI3C dtlz1 03

if [ "$#" -ne 3 ] ; then

    echo "usage: ./run_server.sh moea_name mop_name"

else

    mkdir log

    moea_name="$1"
    mop_name="$2"
    m_objs="$3"
        
    # examples
    #nohup python3 run.py -a "$moea_name" -p "$mop_name" -m "$m_objs" -s 25 -c 10 -q mean -r "$parent_selection" -t "$survival_selection" -f "$contrib_method" -l "$case" -d false > log/log_"$moea_name"_"$mop_name"_"$m_objs"_"$parent_selection"_"$survival_selection"_"$contrib_method"_"$case".log &
    #nohup ./command "$moea_name" "$mop_name" > log/log_"$moea_name"_"$mop_name"_"$m_objs".log &

    nohup ./emo_moea "$moea_name" input/Param_"$m_objs"D_paper.cfg "$mop_name" 50 > log/log_"$moea_name"_"$mop_name"_"$m_objs".log &
    
fi
