# anim_pop.py
# %run anim_pop.py -p dtlz1 -a MOMBI3
from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from mpl_toolkits.mplot3d import Axes3D
import os, sys, argparse

# add lab's path to sys.path
lab_path = "/home/auraham/git/moead_mss_dci"
sys.path.insert(0, lab_path)

from rocket.fronts import get_real_front
from rocket.plot import load_rcparams
from rocket.plot.colors import basecolors

colors = [
        "#74CE74",
        "#E4AFC8",
        "#7D2950",
        ]


def get_pop(t):
    """
    Get population at generation t
    """
    
    # MOMBI3_DTLZ1_03D_R_t_339.pof
    filename = "%s_%s_03D_R_t_%d.pof" % (moea_name, mop_name.upper(), t) 
    filepath = os.path.join(exp_path, filename)
    objs = np.genfromtxt(filepath)
    
    return objs


# --- parse arguments ---
ap = argparse.ArgumentParser()
ap.add_argument("-p", "--mop_name", required=True, help="mop name: dtlz1, dtlz3", choices=("dtlz1", "dtlz3"))
ap.add_argument("-a", "--moea_name", required=True, help="moea name", choices=("MOMBI3", "MOMBI3A", "MOMBI3B"))
args = ap.parse_args()

mop_name  = args.mop_name
mop_label = "%s %s" % (mop_name.upper(), args.moea_name)



# -- dont edit from here --

# moea and pareto front
moea_name   = args.moea_name
m_objs      = 3
exp_path    = "output"
max_iters   = 470
iters       = np.arange(1, max_iters+1, 1, dtype=int)
real_front, p = get_real_front(mop_name, m_objs)


# set up figure and animation
load_rcparams()
fig = plt.figure(figsize=(15, 10))  # 22, 18 o 16, 14
ax = fig.add_subplot(111, projection="3d")

# additional setup
ax.set_xlim(0, 1.1)
ax.set_ylim(0, 1.1)
ax.set_zlim(0, 1.1)
ax.xaxis.set_rotate_label(False)  # disable automatic rotation
ax.yaxis.set_rotate_label(False)  # disable automatic rotation
ax.zaxis.set_rotate_label(False)  # disable automatic rotation
ax.set_xlabel("$f_1$", labelpad=15.0, fontsize=20)
ax.set_ylabel("$f_2$", labelpad=15.0, fontsize=20)
ax.set_zlabel("$f_3$", labelpad=12.0, fontsize=20)
ax.view_init(15, 45)

if mop_name == "dtlz3":
    ax.set_xlim(0, 5.1)
    ax.set_ylim(0, 5.1)
    ax.set_zlim(0, 5.1)

# plot real front
if not real_front is None:
    X = real_front[:, 0].reshape(p, p)
    Y = real_front[:, 1].reshape(p, p)
    Z = real_front[:, 2].reshape(p, p)
    ax.plot_wireframe(X, Y, Z, rstride=2, cstride=2, color=basecolors["real_front"])

# initial points
particles, = ax.plot([], [], [], c=colors[0], 
            mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15)

# initial weights
wparticles, = ax.plot([], [], [], c=colors[1], 
            mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15)

# initial highlighted weights
hwparticles, = ax.plot([], [], [], c=colors[2], 
            mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15)

# initial title
pos = (0.0, 0.5, 1.2)
label = ax.text(pos[0], pos[1], pos[2], "0", fontsize=15, color="#252525")

# initial title (mdp)
pos = (0.0, 0.5, 1.1)
label_mdp = ax.text(pos[0], pos[1], pos[2], "0", fontsize=15, color="#252525")

# initial title (mdp)
pos = (0.0, 0.5, 1.5)
if mop_name == "dtlz1":
    pos = (0.0, 0.5, 1.3)
label_mop = ax.text(pos[0], pos[1], pos[2], "0", fontsize=22, color="#878787")
    

fig.patch.set_facecolor("#ffffff")

# adjust margins
fig.subplots_adjust(left=0.2,
                    bottom=0.05,
                    right=0.8,
                    top=0.95,
                    wspace=0.1,
                    hspace=0.1,
                    )

def init():
    """
    Initialize animation
    """

    # this is needed to initialize a set of points for 3D
    particles.set_data([], [])
    particles.set_3d_properties([])
    
    # this is needed to initialize a set of points for 3D
    wparticles.set_data([], [])
    wparticles.set_3d_properties([])
    
    # this is needed to initialize a set of points for 3D
    hwparticles.set_data([], [])
    hwparticles.set_3d_properties([])
    
    label.set_text("")
    label_mdp.set_text("")
    label_mop.set_text("")

    return particles, wparticles, hwparticles, label, label_mdp, label_mop

def animate(i):
    """
    Perform animation step
    """
    
    global iters, ax, moea_name, mop_name, mdp
    
    # load new set of points
    points = get_pop(iters[i])
    
    # update points in plot
    particles.set_data(points[:, 0], points[:, 1])
    particles.set_3d_properties(points[:, 2])
    
    # empty by default
    wparticles.set_data([], [])
    wparticles.set_3d_properties([])
    
    # empty by default
    hwparticles.set_data([], [])
    hwparticles.set_3d_properties([])
    
    
    
    # update title
    label_mop.set_text(mop_label)
    
    # update title
    title = "Generation %d" % iters[i]
    label.set_text(title)
    
    # update title mdp
    t = iters[i]
    #if t % 10 == 0 and t >= 50:
    #    title = "MDP %.5f " % mdp_dict[t]
    #    label_mdp.set_text(title)
    
    return particles, wparticles, hwparticles, label, label_mdp, label_mop


# run animation
anim = animation.FuncAnimation(fig,             # figure object that is used to get draw
                            func=animate,       # the function to call at each frame, it has this signature def func(frame, *fargs) -> iterable_of_artists
                            init_func=init,     # a function to draw a clear frame
                            frames=len(iters),  # argument passed to func, ie range(frames)
                            interval=100,       # delay between frames in milliseconds
                            blit=True,          # flag to optimize drawing (if True, func must return iterable_of_artist)
                            )

# save animation
#filename = "demo_%s_%s.mp4" % (mop_name, args.moea_name)
#anim.save(filename, fps=10, writer='ffmpeg', extra_args=['-vcodec', 'libx264'])
#print(filename)

plt.show()
