#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "emo.h"

// Hughes03b, u->p = 100
double EMO_Utility_vads_local(double *w, double *x, int nobj, int vads_p) {
  double nx, nw, v;
  int i;

  nx = nw = v = 0;

  for(i = 0; i < nobj; i++) {
    nx += x[i] * x[i];
    nw += w[i] * w[i];
    v += x[i] * w[i];
  }

  nx = sqrt(nx);
  nw = sqrt(nw);
  v = pow(v / (nx * nw) , vads_p);
  return nx / (1e-3 + v);
}

// Hughes03b
double EMO_Utility_vads2_local(double *w, double *x, int nobj, int vads2_p, double wzero) {
  double nx, nw, v, y;
  int i;

  nx = nw = v = 0;

  for(i = 0; i < nobj; i++) {
    y = w[i]? w[i] : wzero;
    nx += x[i] * x[i];
    nw += 1.0 / (y * y);
    v += x[i] / y;
  }

  nx = sqrt(nx);
  nw = sqrt(nw);
  v = pow(v / (nx * nw) , vads2_p);
  return nx / (1e-3 + v); 

}

// python
double EMO_Utility_python_vads(double *w, double *x, int nobj, int q, double wzero) {

    double eps = 2.2204e-16;
    double norm_x = 0;
    double norm_w = 0;
    double dot_x_w = 0;
    int i;

    // norm(objs), norm(weights)
    for(i = 0; i < nobj; i++) {
        norm_x += x[i] * x[i];
        norm_w += w[i] * w[i];
    }
    norm_x = sqrt(norm_x);
    norm_w = sqrt(norm_w);
    
    // dot(f(x), w)
    for(i = 0; i < nobj; i++) {
        dot_x_w += x[i] * w[i];
    }
    
    // denominator
    double den = dot_x_w / (norm_x*norm_w + eps);
    
    double res = norm_x / (pow(den, q) + eps);
    
    return res;
}

double EMO_Utility_python_vads_prev(double *w, double *x, int nobj, int vads2_p, double wzero) {
    
    /*
    def vads_matlab(objs, weights, V):
    
        eps = 2.2204e-16
        t  = (objs*weights).sum()       # dot(v, f)
        lv = np.sqrt((objs**2).sum())   # norm(f)
        Q  = t / (lv + eps)
        tv = lv / (Q**V + eps)          # norm(f) / 
        
        return tv
    
    */
    
    double eps = 2.2204e-16;
    double lv = 0;
    double t = 0;
    int i;

    
    for(i = 0; i < nobj; i++) {
        lv += x[i] * x[i];         
        t += x[i]* w[i];      // t: dot(x, w)
    }

    lv = sqrt(lv);              // lv: norm of x
    
    double Q = t / (lv + eps);
    double tv = lv / (pow(Q, vads2_p) + eps);
    
    return tv;

    // -----
    /*

  

  for(i = 0; i < nobj; i++) {
    y = w[i]? w[i] : wzero;
    nx += x[i] * x[i];
    nw += 1.0 / (y * y);
    v += x[i] / y;
  }

  nx = sqrt(nx);
  nw = sqrt(nw);
  v = pow(v / (nx * nw) , vads2_p);
  return nx / (1e-3 + v); 
    */
}

// python
double EMO_Utility_python_pbi(double *w, double *x, int nobj, double *z_ideal, double *z_nadir) {
    double norm_w = 0, norm_num = 0, norm_d = 0, d1 = 0, d2 = 0;
    int i;

    int pbi_theta = 5;

    // norm(w)
    for(i = 0; i < nobj; i++) {
        norm_w  += w[i] * w[i]; 
    }
    norm_w = sqrt(norm_w);

    // dot [(f(x) - z_ideal), w] 
    for (i = 0; i < nobj; i++) {
        norm_num += ((x[i] - z_ideal[i]) * w[i]);
    }
    //norm_num = sqrt(norm_num);    // no es necesario porque es un escalar (?), solo usa fabs
    
    d1 = fabs(norm_num) / norm_w;

    
    double t = 0;
    for (i = 0; i < nobj; i++) {
        t = (x[i] - (z_ideal[i] + (d1 * w[i]/norm_w)));
        norm_d += t * t;
    }
    d2 = sqrt(norm_d);
    
    
    //printf("d1: %.8f, d2: %.8f, norm_w: %.8f, norm_num: %.8f\n", d1, d2, norm_w, norm_num);
    
    return d1 + pbi_theta * d2;
}

// python
double EMO_Utility_python_ipbi(double *w, double *x, int nobj, double *z_ideal, double *z_nadir) {
    double norm_w = 0, norm_num = 0, norm_d = 0, d1 = 0, d2 = 0;
    int i;

    int pbi_theta = 5;

    // norm(w)
    for(i = 0; i < nobj; i++) {
        norm_w  += w[i] * w[i]; 
    }
    norm_w = sqrt(norm_w);

    // dot [(z_nadir[i] - f(x)), w] 
    for (i = 0; i < nobj; i++) {
        norm_num += ((z_nadir[i] - x[i]) * w[i]);
    }
    //norm_num = sqrt(norm_num);    // no es necesario porque es un escalar (?), solo usa fabs
    
    d1 = fabs(norm_num) / norm_w;

    
    double t = 0;
    for (i = 0; i < nobj; i++) {
        //t = (x[i] - (z_ideal[i] + (d1 * w[i]/norm_w)));
        t = ((z_nadir[i] - x[i]) - ((d1 * w[i]/norm_w)));
        norm_d += t * t;
    }
    d2 = sqrt(norm_d);
    
    
    //printf("d1: %.8f, d2: %.8f, norm_w: %.8f, norm_num: %.8f\n", d1, d2, norm_w, norm_num);

    return -(d1 - (pbi_theta * d2));
}

// python
double EMO_Utility_python_che(double *w, double *x, int nobj, double *z_ideal) {
  double y, v, vmax = 0;
  int i;

  for(i = nobj - 1; i > -1; i--) {
    y = w[i] ? w[i] : 1e-6; 
    v = fabs(x[i] - z_ideal[i]) * y;

    if(v > vmax)
      vmax = v;
  }
  return vmax;
}


double EMO_Utility_ws_local(double *w, double *x, int nobj) {
  double t = 0;
  int i;

  for(i = nobj - 1; i > -1; i--)
    t += w[i] * x[i];
  return t;
}

// python
double EMO_Utility_python_asf(double *w, double *x, int nobj, double wzero, double *z_ideal) {
  double y, v, vmax = 0;
  int i;

  for(i = nobj - 1; i > -1; i--) {
    y = w[i]? w[i] : wzero;
    v = (x[i] - z_ideal[i]) / y;

    if(v > vmax)
      vmax = v;
  }

  return vmax;
}

double EMO_Utility_che_local(double *w, double *x, int nobj) {
  double y, v, vmax = 0;
  int i;

  for(i = nobj - 1; i > -1; i--) {
    y = w[i] ? w[i] : 1e-6; 
    v = fabs(x[i]) * y;

    if(v > vmax)
      vmax = v;
  }
  return vmax;
}


double EMO_Utility_asf_local(double *w, double *x, int nobj, double wzero) {
  double y, v, vmax = 0;
  int i;

  for(i = nobj - 1; i > -1; i--) {
    y = w[i]? w[i] : wzero;
    v = x[i] / y;

    if(v > vmax)
      vmax = v;
  }

  return vmax;
}


double EMO_Utility_pbi_local(double *w, double *x, int nobj, int inverted) {
  double norm = 0, d1 = 0, d2 = 0;
  int i;
  
  int pbi_theta = 5;
  double *pbi_v = (double*) calloc(nobj, sizeof(double));
 
  // Normalize the weight vector (line segment)
  for(i = nobj - 1; i > -1; i--)
    norm  += w[i] * w[i]; 
  norm = sqrt(norm);

  for(i = nobj - 1; i > -1; i--) 
    d1 += x[i] * w[i] / norm;
 
  d1 = fabs(d1);

  for(i = nobj - 1; i > -1; i--) 
    pbi_v[i] = x[i] - d1 * w[i] / norm;
 
  d2 = 0;
 
  // Normalize b
  for(i = nobj - 1; i > -1; i--)
    d2  += pbi_v[i] * pbi_v[i]; 

  d2 = sqrt(d2);
  
  free(pbi_v);

  printf("d1: %.8f, d2: %.8f\n", d1, d2);

  if(inverted)
    return pbi_theta * d2 - d1;

  return d1 + pbi_theta * d2;
}

int main(int argc, char **argv) {
    
    
    double wzero = 1e-2;
    int nobj = 10;
    int vads_p = 100;
    //double w[] = {0.0000100000, 0.0000100000, 0.0000100000, 0.0000100000, 0.0000100000, 0.0000100000, 0.0000100000, 0.0000100000, 0.0000100000, 1.0000000000};
    double x[] = {0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0};
    //double z_ideal[] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
    //double z_nadir[] = {1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0};

    double w[] = {0.07200801, 0.18278161, 0.14073106, 0.11509637, 0.0299957, 0.02999106, 0.01116699, 0.16652855, 0.11556865, 0.13613201};
    double z_ideal[] = {0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1};
    double z_nadir[] = {1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2};
    
    //r_vads = EMO_Utility_vads_local(w, x, nobj, vads_p);
    //printf("r: %.8f\n", r);

    printf("ws: %.8f\n",  EMO_Utility_ws_local(w, x, nobj));
    printf("python_te: %.8f\n",  EMO_Utility_python_che(w, x, nobj, z_ideal));
    //printf("asf: %.8f\n",  EMO_Utility_asf_local(w, x, nobj, wzero));
    printf("python_asf: %.8f\n",  EMO_Utility_python_asf(w, x, nobj, wzero, z_ideal));
    
    //printf("pbi: %.8f\n",  EMO_Utility_pbi_local(w, x, nobj, 0));
    //printf("ipbi: %.8f\n",  EMO_Utility_pbi_local(w, x, nobj, 1));
    //printf("vads: %.8f\n",  EMO_Utility_vads_local(w, x, nobj, vads_p));
    //printf("vads2: %.8f\n",  EMO_Utility_vads2_local(w, x, nobj, vads_p, wzero));
    
    // python
    printf("python_pbi: %.8f\n",  EMO_Utility_python_pbi(w, x, nobj, z_ideal, z_nadir));
    printf("python_ipbi: %.8f\n",  EMO_Utility_python_ipbi(w, x, nobj, z_ideal, z_nadir));
    printf("python_vads: %.8f\n",  EMO_Utility_python_vads(w, x, nobj, vads_p, wzero));
    
    return 0;
}

