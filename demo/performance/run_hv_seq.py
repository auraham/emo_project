# run_hv_seq.py
# 
# %run run_hv_seq.py -a mss-dci_norm_hyp_mean-divs-8-25-mean-10-ps-normal-ss-gra-norm-true -p maf1 -m 3 -r 50 -t 300 -e /media/data/git/moead_mss_dci_results/experiments/paper_test_mss_contrib/results_case_a -b /media/data/git/moead_mss_dci_results/experiments/paper_test_mss_contrib/results_case_a_git
#
# debug: 5 runs only
# %run run_hv_seq.py -a mss-dci_norm_hyp_mean-divs-8-25-mean-10-ps-normal-ss-gra-norm-true -p maf1 -m 3 -r 5 -t 300 -e /media/data/git/moead_mss_dci_results/experiments/paper_test_mss_contrib/results_case_a -b /media/data/git/moead_mss_dci_results/experiments/paper_test_mss_contrib/results_case_a_git

"""
expected output
hv_raw: 0.2713278487 hv_norm: 0.2038526287
hv_raw: 0.2713354735 hv_norm: 0.2038583572
hv_raw: 0.2630277848 hv_norm: 0.1976166677
hv_raw: 0.2708046655 hv_norm: 0.2034595534
hv_raw: 0.2713669379 hv_norm: 0.2038819970
"""

from __future__ import print_function
import numpy as np
import os, sys, argparse
from distutils.dir_util import mkpath
from shutil import copy2
import subprocess

# add rocket
#script_path = os.path.dirname(os.path.abspath(__file__))                        # /home/auraham/git/moead_mss_dci/experiments/paper_test_mss_sensitivity/performance
#log_path = os.path.join(os.path.dirname(script_path), "log")                    # /home/auraham/git/moead_mss_dci/experiments/paper_test_mss_sensitivity/log
#lab_path = os.path.dirname(os.path.dirname(os.path.dirname(script_path)))       # /home/auraham/git/moead_mss_dci
#sys.path.insert(0, lab_path)

from rocket.helpers import save_filtered_objs
from rocket.helpers import filter_by_reference_point
from rocket.helpers import filter_by_nds
from rocket.helpers import normalize
from rocket.helpers import get_log
from rocket.pareto_fronts import get_front

# This is the same configuration as in data/create_vectors.py and
# rocket.pareto_fronts.simplex_lattice.py
param_H = {
    3: 12,
    5: 6,
    8: 3,
    10: 3,
}

def compute_hv_front(mop_name, m_objs, experiment_path, experiment_path_git, log_path="."):
    """
    Compute the hypervolume indicator of a Pareto front
    """
    
    # for exception
    output = ""
    
    # get log
    name = "front_%s_%s" % (mop_name, m_objs)
    log = get_log(name, log_path)
    
    # get pareto front
    front = get_front(mop_name, m_objs)
    
    # define z_ideal, z_nadir
    z_ideal = front.min(axis=0)
    z_nadir = front.max(axis=0)
    
    # define hv reference point
    ref_point = [1.1] * m_objs
    ref_point_str = ["1.1"] * m_objs
    
    # update: define hv reference point as suggested in 
    # [Ishibuchi17] Reference Point Specification in Hypervolume Calculation for Fair Comparison and Efficient Search
    # [Ishibuchi18] How to Specify a Reference Point in Hypervolume Calculation for Fair Performance Comparison
    H = param_H[m_objs]
    r = 1 + (1 / H)
    ref_point = [r] * m_objs
    ref_point_str = ["%.10f" % r] * m_objs
        
    # log
    log.info("computing hv of front")
    log.info("mop_name:            %s" % mop_name)
    log.info("m_objs:              %d" % m_objs)
    log.info("z_ideal:             %s" % z_ideal)
    log.info("z_nadir:             %s" % z_nadir)
    log.info("H:                   %s" % H)
    log.info("ref_point:           %s" % ref_point)
    log.info("experiment_path:     %s" % experiment_path)
    log.info("experiment_path_git: %s" % experiment_path_git)

    # input file: objs
    filename_objs = "front_%s_m_%d.txt" % (mop_name, m_objs)
    filepath_objs = os.path.join(experiment_path, mop_name, filename_objs)
    
    # output file
    filename_hv = "hv_front_%s_m_%d.txt" % (mop_name, m_objs)
    filepath_hv = os.path.join(experiment_path, mop_name, filename_hv)
    
    # preprocessing input file
    # normalization only (ref point + nds filters are not needed)
    front_norm = normalize(front, z_ideal, z_nadir)                         # normalize in range [0, 1]
    
    # save objs
    save_filtered_objs(front_norm, filepath_objs)
    
    # prepare command
    cmd = ["wfg", "-q", filepath_objs, *ref_point_str, "-o", filepath_hv]
    
    # debug
    log.info("command: %s" % " ".join(cmd))

    try:
        
        # call wfg
        proc = subprocess.run(cmd, stdout=subprocess.PIPE)

        # get output from wfg
        output = proc.stdout.decode("utf-8")

        hv_raw, hv_norm = output.replace("\n", "").split(" ")
        log.info("hv_raw: %s hv_norm: %s" % (hv_raw, hv_norm))
       
    except Exception as e:
        
        log.error("Output of wfg: %s" % output)
        log.error("Exception: %s" % str(e))
        log.error("Exiting...")
        sys.exit(1)
        
    # --- backup ---
    
    if experiment_path_git:
    
        # define backup directory
        backup_path = os.path.join(experiment_path_git, mop_name)
    
        # create backup directory, if needed
        if not os.path.isdir(backup_path):
            mkpath(backup_path)
    
        # define paths (backup)
        filepath_hv_copy = os.path.join(backup_path, filename_hv)
        
        # read hv file
        hv_data = np.genfromtxt(filepath_hv)        # (2, ) array, hv_raw and hv_norm_1.1
        
        # save hv final file (backup)
        comment = "There is a single line with two values (hv of a Pareto front): hv_raw and hv_norm_1.1"
        np.savetxt(filepath_hv_copy, np.array([hv_data]), header=comment)


def compute_hv_seq(moea_name, mop_name, m_objs, runs, iters, experiment_path, experiment_path_git, log_path="."):
    """
    Compute the hypervolume indicator from input files
    """
    
    # for exception
    output = ""
    
    # get log
    name = "%s_%s_%s" % (moea_name, mop_name, m_objs)
    log = get_log(name, log_path)
    
    # get pareto front
    front = get_front(mop_name, m_objs)
    
    # define z_ideal, z_nadir
    z_ideal = front.min(axis=0)
    z_nadir = front.max(axis=0)
    
    # define hv reference point
    ref_point = [1.1] * m_objs
    ref_point_str = ["1.1"] * m_objs
    
    # update: define hv reference point as suggested in 
    # [Ishibuchi17] Reference Point Specification in Hypervolume Calculation for Fair Comparison and Efficient Search
    # [Ishibuchi18] How to Specify a Reference Point in Hypervolume Calculation for Fair Performance Comparison
    H = param_H[m_objs]
    r = 1 + (1 / H)
    ref_point = [r] * m_objs
    ref_point_str = ["%.10f" % r] * m_objs
        
    # log
    log.info("moea_name:           %s" % moea_name)
    log.info("mop_name:            %s" % mop_name)
    log.info("m_objs:              %d" % m_objs)
    log.info("runs:                %d" % runs)
    log.info("iters:               %d" % iters)
    log.info("z_ideal:             %s" % z_ideal)
    log.info("z_nadir:             %s" % z_nadir)
    log.info("H:                   %s" % H)
    log.info("ref_point:           %s" % ref_point)
    log.info("experiment_path:     %s" % experiment_path)
    log.info("experiment_path_git: %s" % experiment_path_git)
    log.info("remember to normalize hv_raw as hv_norm = hv_raw / hv_raw_front")

    for run_id in range(runs):
        
        # input file: objs
        filename_objs = "objs_%s_m_%d_run_%d_t_%d.txt" % (moea_name, m_objs, run_id, iters)
        filepath_objs = os.path.join(experiment_path, mop_name, moea_name, "pops", filename_objs)
        
        # CHANGE for mombi3
        # format: MOMBI3A_DTLZ3_05D_R24.pof
        filename_objs = "%s_%s_%02dD_R%02d.pof" % (moea_name, mop_name.upper(), m_objs, run_id+1)
        filepath_objs = os.path.join(experiment_path, filename_objs)
        
        # CHANGE for mombi3 and inv-dtlz1
        # format: MOMBI3A_INV_DTLZ1_05D_R24.pof rather than
        #         MOMBI3A_INV-DTLZ1_05D_R24.pof
        if mop_name == "inv-dtlz1":
            filename_objs = "%s_%s_%02dD_R%02d.pof" % (moea_name, mop_name.replace("-", "_").upper(), m_objs, run_id+1)
            filepath_objs = os.path.join(experiment_path, filename_objs)
        
        # input file: normalized + filtering using ref point and nds
        filepath_objs_filtered = filepath_objs + ".norm.filtered"

        # output file
        filename_hv = "hv_%s_m_%d_run_%d_t_%d.txt" % (moea_name, m_objs, run_id, iters)
        filepath_hv = os.path.join(experiment_path, mop_name, moea_name, "stats", filename_hv)
        
        # CHANGE for mombi3
        # create hv output dir if it does not exist
        if not os.path.isdir(os.path.dirname(filepath_hv)):
            mkpath(os.path.dirname(filepath_hv))
        
        # check if file exists
        if not os.path.isfile(filepath_objs):
            print("Error: filepath does not exists (filepath: %s)" % filepath_objs)
            continue
        
        # --- preprocessing input file ---
        # normalization + ref point + nds (first front)
        objs = np.genfromtxt(filepath_objs)                                     # load objs
        objs_norm = normalize(objs, z_ideal, z_nadir)                           # normalize in range [0, 1]
        
        # error: use [1,1, ..., 1]
        #objs_norm_fil_a = filter_by_reference_point(objs_norm, ref_point)      # remove points worsen than [1.1, 1.1, ..., 1.1]
        
        unit = np.ones((m_objs, ))
        objs_norm_fil_a = filter_by_reference_point(objs_norm, unit)            # remove points worsen than [1, 1, ..., 1]
        
        objs_norm_fil_b = filter_by_nds(objs_norm_fil_a, procs=4)               # remove dominated solutions
        # --- preprocessing input file ---
        
        # get pop_size after and before filtering
        pop_size  = objs.shape[0]
        pop_size_final = objs_norm_fil_b.shape[0]
        
        # save objs
        save_filtered_objs(objs_norm_fil_b, filepath_objs_filtered)
        
        # debug: use this line if you plan to use run_hv_seq.m as well
        #save_filtered_objs(objs_norm_fil_b, filepath_objs_filtered+".matlab", add_separators=False)
        
        # prepare command
        cmd = ["wfg", "-q", filepath_objs_filtered, *ref_point_str, "-o", filepath_hv]
        
        # debug
        log.info("command: %s" % " ".join(cmd))
        
        try:
        
            # call wfg
            proc = subprocess.run(cmd, stdout=subprocess.PIPE)

            # get output from wfg
            output = proc.stdout.decode("utf-8")

            hv_raw, hv_norm = output.replace("\n", "").split(" ")
            log.info("run_id: %d, hv_raw: %s hv_norm: %s, pop_size: %d/%d" % (run_id, hv_raw, hv_norm, pop_size_final, pop_size))
        
        except Exception as e:
            
            log.error("Output of wfg: %s" % output)
            log.error("Exception: %s" % str(e))
            log.error("Exiting...")
            sys.exit(1)
        
    
def merge_hv_files(moea_name, mop_name, m_objs, runs, iters, experiment_path, experiment_path_git, log_path="."):
    """
    Merge the hv files into a single one
    """
    
    # get log
    name = "%s_%s_%s" % (moea_name, mop_name, m_objs)
    log = get_log(name, log_path)
    
    # load hv of Pareto front
    filename_hv = "hv_front_%s_m_%d.txt" % (mop_name, m_objs)
    filepath_hv = os.path.join(experiment_path, mop_name, filename_hv)
    hv_data = np.genfromtxt(filepath_hv)
    
    # use the raw hv value only
    hv_front_raw = hv_data[0]
    
    # prepare comment for txt file
    comment = "Each line contains three values: hv_raw, hv_norm_1.1, and hv_norm (hv_norm is recommended for showing results). Each line is the hv value of an independent run."
    
    # hv final file
    filename_hv_final = "hv_%s_m_%d_t_%d_complete.txt" % (moea_name, m_objs, iters)
    filepath_hv_final = os.path.join(experiment_path, mop_name, moea_name, "stats", filename_hv_final)
    
    data = []
    
    # merge results
    for run_id in range(runs):
        
        # hv file
        filename_hv = "hv_%s_m_%d_run_%d_t_%d.txt" % (moea_name, m_objs, run_id, iters)
        filepath_hv = os.path.join(experiment_path, mop_name, moea_name, "stats", filename_hv)
   
        # read hv file
        hv_data = np.genfromtxt(filepath_hv)        # (2, ) array, hv_raw and hv_norm_1.1
        
        # save and normalize
        hv_values = np.zeros((3, ))
        
        hv_values[0] = hv_data[0]                   # hv_raw
        hv_values[1] = hv_data[1]                   # hv_norm_1.1
        hv_values[2] = hv_data[0] / hv_front_raw    # hv_norm using HV(P) / HV(Pareto_front)
        
        # append to list
        data.append(hv_values)
        
        # debug
        log.info("run_id: %d, hv normalized: %s" % (run_id, hv_values[2]))
        
        
    # save hv final file
    np.savetxt(filepath_hv_final, np.array(data), header=comment)
    
    # log
    log.info("merging files")
    log.info("mop_name:            %s" % mop_name)
    log.info("m_objs:              %d" % m_objs)
    log.info("hv_raw:              %.10f" % hv_front_raw)
    log.info("hv_filepath:         %s" % filepath_hv)
    log.info("final hv file:       %s" % filepath_hv_final)
    
    
    
    # --- backup ---
    
    if experiment_path_git:
    
        # define backup directory
        backup_path = os.path.join(experiment_path_git, mop_name, moea_name, "stats")
    
        # create backup directory, if needed
        if not os.path.isdir(backup_path):
            mkpath(backup_path)
    
        # save hv final file (backup)
        filepath_hv_final_copy = os.path.join(backup_path, filename_hv_final)
        np.savetxt(filepath_hv_final_copy, np.array(data), header=comment)


        log.info("final hv file (copy):%s" % filepath_hv_final_copy)
    



if __name__ == "__main__":
    
    ap = argparse.ArgumentParser()
    ap.add_argument("-a", "--moea_name",            required=True, help="moea name")
    ap.add_argument("-p", "--mop_name",             required=True, help="mop name")
    ap.add_argument("-m", "--m_objs",               required=True, help="m_objs: 3, 5, 8, 10", type=int, choices=(3,5,8,10))
    ap.add_argument("-r", "--runs",                 required=True, help="number of runs", type=int)
    ap.add_argument("-t", "--iters",                required=True, help="number of generations", type=int)
    ap.add_argument("-e", "--experiment_path",      required=True, help="experiment path")
    ap.add_argument("-b", "--experiment_path_git",  required=False, help="backup of results for git")
    args = vars(ap.parse_args())
    
    compute_hv_front(                           # example
                args["mop_name"],               # maf1
                args["m_objs"],                 # 3
                args["experiment_path"],        # /media/data/git/moead_mss_dci_results/experiments/paper_test_mss_contrib/results_case_a
                args["experiment_path_git"],    # /media/data/git/moead_mss_dci_results/experiments/paper_test_mss_contrib/results_case_a_git
            )
            
    compute_hv_seq(                             # example
                args["moea_name"],              # mss-dci_norm_hyp_mean-divs-8-25-mean-10-ps-normal-ss-gra-norm-true
                args["mop_name"],               # maf1
                args["m_objs"],                 # 3
                args["runs"],                   # 50
                args["iters"],                  # 300
                args["experiment_path"],        # /media/data/git/moead_mss_dci_results/experiments/paper_test_mss_contrib/results_case_a
                args["experiment_path_git"]     # /media/data/git/moead_mss_dci_results/experiments/paper_test_mss_contrib/results_case_a_git
            )

    merge_hv_files(                             # example
                args["moea_name"],              # mss-dci_norm_hyp_mean-divs-8-25-mean-10-ps-normal-ss-gra-norm-true
                args["mop_name"],               # maf1
                args["m_objs"],                 # 3
                args["runs"],                   # 50
                args["iters"],                  # 300
                args["experiment_path"],        # /media/data/git/moead_mss_dci_results/experiments/paper_test_mss_contrib/results_case_a
                args["experiment_path_git"]     # /media/data/git/moead_mss_dci_results/experiments/paper_test_mss_contrib/results_case_a_git
            )


