# run_hv.py
# %run run_hv.py -k rocket
from __future__ import print_function
import numpy as np
import os, sys, argparse
np.seterr(all='raise')

# add rocket
sys.path.insert(0, "/home/auraham/git/moead_mss_dci")

#from rocket.performance import compute_hv_front
#from rocket.performance import compute_hv_seq, merge_hv_files
from run_hv_seq import compute_hv_front
from run_hv_seq import compute_hv_seq, merge_hv_files
from config import *

if __name__ == "__main__":
    
    ap = argparse.ArgumentParser()
    ap.add_argument("-k", "--server_name",  required=True, help="server name", choices=paths.keys())
    args = vars(ap.parse_args())
    
    # path of the results
    experiment_path     = paths[args["server_name"]]
    experiment_path_git = paths_git[args["server_name"]]
    log_path            = logs[args["server_name"]]
    
    
    # compute hv (fronts)
    for mop_name in mop_names:
        
        for m_objs in sorted(budget[mop_name].keys()):
            
            compute_hv_front(               # example
                mop_name,                   # maf1
                m_objs,                     # 3
                experiment_path,            # /media/data/git/moead_mss_dci_results/experiments/paper_test_mss_contrib/results_case_a
                experiment_path_git,        # /media/data/git/moead_mss_dci_results/experiments/paper_test_mss_contrib/results_case_a_git
                log_path
            )       
    
    
    # compute hv (pops)
    for mop_name in mop_names:
        
        for m_objs in sorted(budget[mop_name].keys()):
            
            iters = budget[mop_name][m_objs]
            
            for moea_name in moea_names:
    
                compute_hv_seq(             # example
                    moea_name,              # mss-dci_norm_hyp_mean-divs-8-25-mean-10-ps-normal-ss-gra-norm-true
                    mop_name,               # maf1
                    m_objs,                 # 3
                    runs,                   # 50
                    iters,                  # 300
                    experiment_path,        # /media/data/git/moead_mss_dci_results/experiments/paper_test_mss_contrib/results_case_a
                    experiment_path_git,    # /media/data/git/moead_mss_dci_results/experiments/paper_test_mss_contrib/results_case_a_git
                    log_path
                )

                
                merge_hv_files(             # example
                    moea_name,              # mss-dci_norm_hyp_mean-divs-8-25-mean-10-ps-normal-ss-gra-norm-true
                    mop_name,               # maf1
                    m_objs,                 # 3
                    runs,                   # 50
                    iters,                  # 300
                    experiment_path,        # /media/data/git/moead_mss_dci_results/experiments/paper_test_mss_contrib/results_case_a
                    experiment_path_git,    # /media/data/git/moead_mss_dci_results/experiments/paper_test_mss_contrib/results_case_a_git
                    log_path
                )
                
    
