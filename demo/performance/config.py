# config.py
# Change the settings of the experiment

# ----- CHANGE THESE VALUES -----

case = "a"
experiment_name = "paper_test_mss_sensitivity"

print("REMEMBER to change the name of the experiment (%s)" % experiment_name)

# ----- common options -----

# number of independent runs
runs = 50                       

# path for saving logs
logs = {
    #"rocket"  : "/home/auraham/Desktop/resultados_mombi3_alux2/output/",
    "rocket"  : "/home/auraham/git/emo_project/demo/output_paper/",
}

# path of results
paths = {
    #"rocket"  : "/home/auraham/Desktop/resultados_mombi3_alux2/output/",
    "rocket"  : "/home/auraham/git/emo_project/demo/output_paper/",
}

# path for saving results in git
paths_git = {
    #"rocket"  : "/home/auraham/Desktop/resultados_mombi3_alux2/output_git/",
    "rocket"  : "/home/auraham/git/emo_project/demo/output_paper_git/",
}

# computational budget
budget = {      # {m_objs:iters}
    "dtlz1"     : {3: 300, 5: 500, 8: 800, 10: 1000},
    "dtlz2"     : {3: 300, 5: 500, 8: 800, 10: 1000},
    "dtlz3"     : {3: 300, 5: 500, 8: 800, 10: 1000},
    "inv-dtlz1"     : {3: 300, 5: 500, 8: 800, 10: 1000},
    "maf1"      : {3: 300, 5: 500, 8: 800, 10: 1000},
    "maf2"      : {3: 300, 5: 500, 8: 800, 10: 1000},
    "maf3"      : {3: 300, 5: 500, 8: 800, 10: 1000},
    "maf4"      : {3: 300, 5: 500, 8: 800, 10: 1000},
    "maf5"      : {3: 300, 5: 500, 8: 800, 10: 1000},
}

# MOMBI3A_%s_03D_R%02d.pof

# define the problems and algorithms
mop_names = (
            "dtlz1", 
            "dtlz2", 
            "dtlz3", 
            "inv-dtlz1", 
            "maf1", 
            "maf2", 
            "maf3", 
            "maf4", 
            "maf5",
            )
moea_names = (
                "MOMBI3",
                "MOMBI3C",
            )
