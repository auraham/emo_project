# Performance indicators

This directory contains scripts for evaluating the performance of MOMBI-III (`MOMBI3`, `MOMBI3A`, `MOMBI3B`) regarding the Hypervolume indicator.



| Script                              | Description                                                  |
| ----------------------------------- | ------------------------------------------------------------ |
| `config.py`                         | Parameters of the experiment. **Edit this file before running any indicator**. |
| `run_hv.py`  and `run_hv_server.sh` | Scripts for running the hypervolume indicator.               |

----

**Note** We use the hypervolume implementation of the Walking Fish Group (WFG) available in [this repository](https://gitlab.com/auraham/wfg_hypervolume). Download it before running any other script.

-----



### Hypervolume

The hypervolume is normalized as follows:

```
NHV(P) = HV(P) / HV(Pareto_front)
```

Before computing the indicator, be sure to setup the parameters of the experiment in `config.py`. This script defines:

- The name of the experiment
- The name of the algorithms and problems
- Paths for saving results and logs

Compute the hypervolume indicator as follows:

```
In [1]: %run run_hv.py -k rocket     
```


