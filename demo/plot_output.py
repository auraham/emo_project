# plot_output.py
from __future__ import print_function
import numpy as np

from plot_pops import load_rcparams, load_objs, plot_pops

# remove this
import sys
sys.path.insert(0, "/home/auraham/git/moead_mss_dci")

from rocket.plot import parallel_coordinates as pc
from rocket.fronts import get_real_front
from rocket.pareto_fronts import get_front

def plot_output(input_filepath, mop_name):
    
    # load objs
    objs = load_objs(input_filepath)
    
    pop_size, m_objs = objs.shape
    
    pops = (objs, )
    lbls = ("pop", )
    
    # real front
    front = get_front(mop_name, m_objs)
    p = None
    
    if mop_name in ("dtlz1", "dtlz2", "dtlz3", "inv-dtlz1"):
        front, p = get_real_front(mop_name, m_objs)
    
    # plot objs
    if m_objs == 3:
        
        load_rcparams((10, 8))
        title = input_filepath
        plot_pops(pops, lbls, title=title, real_front=front, p=p)
    
    else:
        
        load_rcparams((10, 8))
        title = input_filepath
        pc(pops, lbls, title=title)

if __name__ == "__main__":
    
    
    inputs = (
        ("output/MOMBI3_MAF1_03D_R01.pof", "maf1"),
        ("output/MOMBI3_MAF2_03D_R01.pof", "maf2"),
        ("output/MOMBI3_MAF3_03D_R01.pof", "maf3"),
        ("output/MOMBI3_MAF4_03D_R01.pof", "maf4"),
        ("output/MOMBI3_MAF5_03D_R01.pof", "maf5"),
        )
        
    for input_filepath, mop_name in inputs:
        plot_output(input_filepath, mop_name)
    
