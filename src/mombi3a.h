
#ifndef _MOMBI3A_
#define _MOMBI3A_

#include "refpoint.h"
#include "utility.h"
#include "random.h"
#include "common.h"
#include "debug.h"
#include "param.h"
#include "stop.h"
#include "list.h"
#include "plot.h"

typedef struct {
  EMO_List lst1, lst2; // temporary lists
  EMO_Refpoint ref;    // Reference points
  double *norm;        // normalized objective functions
  double *rank;        // rank of individuals
  double *rank2;       // rank of individuals
  double *l2;          // l2 norm
  double **sort;       // temporary array for sorting population
  int ssize;           // temporal population size (for memory free)
  double *tmp;         // temporary array, borrar RHG
  int wsize;           // number of weight vectors
  double *W;           // weight vectors
  char **H;            // hyper-heuristic
  char **T;            // target directions
  double *min;         // ideal point
  double *max;         // nadir point
  double *ideal;       // ideal point
  double *new_min;
  double *new_max;
  double *hist;
  int *update;
  EMO_Utility utl;
  int *filter0;        // selected individuals
  int *filter;         // selected individuals
  int *filter2;        // selected individuals
  int max_hist;        /* parameters of the algorithm */
  double epsilon;
  double alpha;
  char *wfile;
  int dm;
  int (*fcomp)(const void *, const void *);
  void (*fnorm)(double *, double *, int *, int, double *, double *, int);
} EMO_MOMBI3A;

void EMO_MOMBI3A_alloc(EMO_MOMBI3A *alg, EMO_Param *param, EMO_Population *pop, EMO_MOP *mop);
void EMO_MOMBI3A_free(EMO_MOMBI3A *alg);
void EMO_MOMBI3A_run(EMO_MOMBI3A *alg, EMO_Param *param, EMO_Population *pop, EMO_MOP *mop);
void EMO_MOMBI3A_prun(EMO_MOMBI3A *alg, EMO_Param *param, EMO_Population *pop, EMO_MOP *mop);


/* --- fevs start ---

double EMO_Utility_python_vads(double *w, double *x, int nobj, int q, double wzero);
double EMO_Utility_python_pbi(double *w, double *x, int nobj, double *z_ideal, double *z_nadir);
double EMO_Utility_python_ipbi(double *w, double *x, int nobj, double *z_ideal, double *z_nadir);
double EMO_Utility_python_che(double *w, double *x, int nobj, double *z_ideal);
double EMO_Utility_python_asf(double *w, double *x, int nobj, double wzero, double *z_ideal);

 --- fevs end ---*/

#endif

