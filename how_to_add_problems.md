# How to add problems

This document explains how to add problems into `emo_project`, as well other useful topics, such as:

- How to read and write matrices
- How to evaluate an input matrix using a test problem
- How to add a problem



## How to read an input matrix

You can use the following function for reading a matrix:

```c
// io.c
double *EMO_File_read(double *data, int *row, int *col, const char *str, int start)
```

You can read an input file as follows:

```c
// read file
int rows = 0;           // use zero, its value will be defined in EMO_File_read
int cols = 0;           // use zero, its value will be defined in EMO_File_read
int start = 0;
double *data = NULL;
data = EMO_File_read(data, &rows, &cols, "input.pos", start);
```

----

**Note** You can inspect `data` after reading a file within `gdb` as follows:

```
p *data@n
```

Replace `n` with the number of columns:

```
p *data@3
```

That will print the first row of `data`. To display the `i`-th row, use:

```
p *(data + i*n)@n
```

Suppose this is your input matrix stored in `input.pos`:

```
# 5 3
1.1 2.2 3.3 
2.1 2.2 3.3 
3.1 2.2 3.3 
4.1 2.2 3.3 
5.1 2.2 3.3 
```

Use this command to display the third row:

```
p *(data + 2*3)@3
```

This is the output:

```
$2 = {3.1000000000000001, 2.2000000000000002, 3.2999999999999998}
```

----



## How to write a matrix

You can use the following function for writing a matrix:

```c
// io.c
void EMO_File_write(double *data, int *filter, int row, int col, const char *str, const char *fmt, unsigned long itera)
```

You can  write a matrix as follows:

```c
// read file
int rows = 0;           // use zero, its value will be defined in EMO_File_read
int cols = 0;           // use zero, its value will be defined in EMO_File_read
int start = 0;
double *data = NULL;
data = EMO_File_read(data, &rows, &cols, "input.pos", start);

// write file
EMO_File_write(data, NULL, rows, cols, "output.pos", "%f ", 0);
```



## How to evaluate a problem from an input matrix

In this section, we will describe how to (1) load a `double` matrix from an input file, (2) evaluate that matrix on a test problem, and (3) save the results on a file.

I recommend to check `emo_moea.c` to get a better insight on how to use the framework. At first, I tried to understand each function in this file. My first approach I considered to rely on `EMO_File_read`  and`EMO_File_write` for I/O, and `EMO_Population_evaluate` for evaluation. Although it was a simple idea, it did not work as expected because I ignored a few other initialization tasks behind scenes. For instance, `EMO_Population_evaluate` requires two pointers, that is, `EMO_Population *pop` and `EMO_MOP *mop`.  These pointers must be created after reading a parameter file (another issue that I omitted). In short, it would require more time than I initially though. Instead, we will create a copy of `emo_emo.c`, called `emo_test.c`, and modify it as we need. 

Let's first check `emo_moea.c` (some lines were removed for clarity):

```c
// emo_moea.c
int main(int argc, char **argv) {
    
    EMO_Population pop;
    EMO_Param param;
    EMO_MOEA moea;
    EMO_MOP mop;
    int r, nrun;

 	if(strcmp(argv[3], "default") == 0)
		myMOP_alloc(&mop);
  
    EMO_Param_alloc_from_file(&param, &mop, argv[1], argv[2], argv[3]);
    EMO_MOEA_alloc(&moea, &param, &pop, &mop, argv[1]);

  	for(r = 1; r <= nrun; r++) {
        EMO_Stop_start(param.stop);

        EMO_Population_init(&pop, param.rand, &mop);

        EMO_MOEA_run(&moea, &param, &pop, &mop);

        EMO_Population_write(&pop, NULL, &mop, param.prefix, r, 0);
        EMO_Param_save(&param, &pop, &mop, r);

        EMO_Rand_next_seed(param.rand, 0);
  	}

    EMO_MOEA_free(&moea);
    EMO_Param_free(&param);
    EMO_Population_free(&pop);

  	return 0;
}
```

Roughly speaking, we read a configuration file using `EMO_Param_alloc_from_file` to set the parameters of the algorithm and test problem. We then create the algorithm (`moea`), problem (`mop`), population (`pop`). The main loop evaluates the algorithm `nrun` times. Within the loop, the search process is performed using `EMO_MOEA_run`. Finally, the memory is deallocated and the program finishes. 

In the following, we describe only the functions that will be employed in `emo_test.c`:

| Function                    | Description                                                  |
| --------------------------- | ------------------------------------------------------------ |
| `EMO_Param_alloc_from_file` | This function reads a configuration file that contains the parameters for the algorithm and problem. From these parameters, you must pay attention to `psize` and `nobj` for the sake of this post. |
| `EMO_MOEA_alloc`            | This function configures `moea`, that is an instance of `EMO_MOEA`. This function defines the pointers to functions of `moea`, such as `moea->palg` and `moea->alloc`. It is important to note that `moea->alloc` handles the initialization of the population `pop`, and it depends on the algorithm itself. In other words, the initialization of `pop` regarding `NSGA2` may differ to that corresponding to other algorithm. In this post, we will use the initialization procedure of `NSGA2` since it only requires to call `EMO_Population_alloc()`. |
| `EMO_Stop_start`            | It defines some flags for controllig the execution, such as the number of epochs and getting the time of the machine. |
| `EMO_Population_alloc`      | Initializes a `EMO_Population *pop`. Here, `pop` contains two pointers, that is, `pop->var` for the decision vectors, and `pop->obj` for the objective vectors. This function allocates memory for these two pointers. However, the decision and objective vectors only contain zeros. |
| `EMO_Population_init`       | After calling `EMO_Population_alloc`, you can call `EMO_Population_init` for assigning the decision vectors a random initial value. |
| `EMO_Population_evaluate`   | This function evaluates the decision vectors stored in `pop-var` and saves the resulting objective vectors in `pop->obj`. |
|                             |                                                              |
|                             |                                                              |
|                             |                                                              |

Now, let's review how to define a new problem. Fortunately for us, there are two placeholder for this purpose in `emo_moea.c`:

```c
void myMOP_eval(EMO_MOP *mop, double *f, double *x) {
	
    // update objective vectors f[i]
   	f[0] = x[0] * x[0];
  	f[1] = pow(x[0] - 1.0, 2.0);
}

void myMOP_alloc(EMO_MOP *mop) {
    // define the number of variables, number of objectives,
    // the pointer to the function that actually encodes the test functions
    // and other properties
  	mop->nvar = 2;
  	mop->nobj = 2;
    mop->f = myMOP_eval;
}
```

This is the `main` function of `emo_test.c`:

```c
int main(int argc, char **argv) {
    
    EMO_Population pop;
    EMO_Param param;
    EMO_MOEA moea;
    EMO_MOP mop;
    int i, j, k;
    int rows, cols, start, size;
    char mop_name[100];
    char input_config[100];
    char input_matrix[100];
    char output_matrix[100];
    char algorithm[100];

    if(argc != 4) {
        //printf("\nSyntax: %s MOEA parameter_file {MOP, default} runs\n\n", argv[0]);
        printf("\nSyntax: %s mop_name parameter_file input_matrix\n\n", argv[0]);
        EMO_Dictionary_print(stdout, EMO_MOEA_list, "MOEA");
        EMO_Dictionary_print(stdout, EMO_Benchmark_list, "\nMOP");
        EMO_Dictionary_print(stdout, EMO_Benchmark_listc, "\nConstrained MOP");
        printf("\nWhen the default option is selected, the MOEA solves the problem defined in %s.c:myMOP_eval\n\n", argv[0]);
        return 1;
    }
    
    // get params
    sprintf(mop_name, "%s", argv[1]);
    sprintf(input_config, "%s", argv[2]);
    sprintf(input_matrix, "%s", argv[3]);
    sprintf(algorithm, "%s", "NSGA2");
    
    // debug
    printf("mop:           %s\n", mop_name);
    printf("config:        %s\n", input_config);
    printf("input matrix:  %s\n", input_matrix);

    if(strcmp(mop_name, "default") == 0) {
        //myMOP_alloc(&mop);
        //maf1_alloc(&mop);
        //maf2_alloc(&mop);
        //maf3_alloc(&mop);
        //maf4_alloc(&mop);
        //maf5_alloc(&mop);
        inv_dtlz1_alloc(&mop);
    }
        
  
    // read parameters from file
    // This function assigns pop_size in param
    EMO_Param_alloc_from_file(&param, &mop, algorithm, input_config, mop_name);

    // allocate population
    EMO_MOEA_alloc(&moea, &param, &pop, &mop, algorithm);

    // init additional flags
    EMO_Stop_start(param.stop);

    // create initial population randomly
    // evaluate initial population
    EMO_Population_init(&pop, param.rand, &mop);

    // read input file
    rows = 0;           // use zero, its value will be defined in EMO_File_read
    cols = 0;           // use zero, its value will be defined in EMO_File_read
    start = 0;
    double *data = NULL;
    data = EMO_File_read(data, &rows, &cols, input_matrix, start);
    printf("reading input %s\n", input_matrix);

    // copy population from file
    // snippet from common.c:EMO_Population_init()
    for(i = 0; i < pop.mu; i++) {
        k = i * mop.nvar;

        for(j = 0; j < mop.nvar; j++)
            pop.var[k + j] = data[k + j];
    }
    
    // evaluate population
    start = 0;
    size = pop.mu;
    EMO_Population_evaluate(&pop, &mop, start, size);
    
    // write objs file
    rows = pop.mu;
    cols = mop.nobj;
    sprintf(output_matrix, "output_objs_%s_m_%d.txt", mop_name, mop.nobj);
    EMO_File_write(pop.obj, NULL, rows, cols, output_matrix, "%.30f ", 0);
    printf("output: %s\n", output_matrix);
    
    // deallocate memory
    EMO_MOEA_free(&moea);
    EMO_Param_free(&param);     // free MOP
    EMO_Population_free(&pop);
    
    return 0;
}
```

and this is one of the new test problems added into `emo_test.c`:

```c
/* ---- maf1 ---- */

void maf1_eval(EMO_MOP *mop, double *f, double *x) {
    // This code is based on MAF01.java
    // See https://github.com/ranchengcn/IEEE-CEC-MaOO-Competition

    // variables
    double g=0, subf1=1, subf3;
    int i, j;
    int m_objs = mop->nobj;
    int n_vars = mop->nvar;
    
    // evaluate g(xm)
    for ( j = m_objs-1 ; j < n_vars; j++ )
        g += (pow(x[j] - 0.5, 2));
    subf3 = 1 + g;
        
    
    // evaluate objectives
    f[m_objs-1] = x[0]*subf3;
    for (i = m_objs-2 ; i > 0 ; i-- ) {
        subf1 *= x[m_objs-i-2];
        f[i] = subf3*(1-subf1*(1-x[m_objs-i-1]));
    }
    f[0] = (1 - subf1*x[m_objs-2]) * subf3;

    
    // debug
    for (i=0; i<m_objs; i++) {
        
        if (i == m_objs - 1)
            printf("%.5f", f[i]);
        else
            printf("%.5f ", f[i]);
    }
    printf("\n");
}

void maf1_alloc(EMO_MOP *mop) {
    // allocate maf1 assuming m=10 objs and n=19 vars
    
    int i;

    /* MOP's name */
    mop->name = (char *) malloc(sizeof(char) * 200);
    strcpy(mop->name, "maf1");

    /* Number of objectives */
    mop->nobj = 10;
    
    /* Number of decision variables */
    mop->nvar = mop->nobj + 10 - 1;         // m + k - 1

    /* Box constraints */
    mop->xmin = (double *) calloc(sizeof(double), mop->nvar);
    mop->xmax = (double *) calloc(sizeof(double), mop->nvar);

    for(i = 0; i < mop->nvar; i++) {
        mop->xmin[i] = 0.0;
        mop->xmax[i] = 1.0;
    }

    /* Required */
    mop->npos = 0;
    mop->feval = 0;
    mop->coding = EMO_REAL;

    /* MOP without inequality constraints */
    mop->ncon = 0;
    mop->f = maf1_eval;
}
```

You can compile and test the code as follows:

```
cd emo_project/demo
make emo_test
./emo_test default input/Param_10D_test.cfg input_maf_m_10.txt
```

This is part of the output:

```
reading input input_maf_m_10.txt
2.10813 2.10811 2.10813 2.10812 2.10767 1.99432 2.08713 1.92462 1.28146 1.14557
2.30499 2.30498 2.30042 2.29725 2.24690 2.16583 2.25825 2.28864 1.94598 0.63174
1.78524 1.78555 1.78520 1.78165 1.54103 1.60619 1.53378 1.54943 1.11286 1.59023
1.78581 1.78597 1.78597 1.78482 1.78519 1.78451 1.77998 1.77656 1.38051 0.42452
1.46915 1.46908 1.46929 1.46897 1.46206 1.46163 1.45123 1.13722 0.76054 1.07450
2.10813 2.10811 2.10813 2.10812 2.10767 1.99432 2.08713 1.92462 1.28146 1.14557
2.30499 2.30498 2.30042 2.29725 2.24690 2.16583 2.25825 2.28864 1.94598 0.63174
1.78524 1.78555 1.78520 1.78165 1.54103 1.60619 1.53378 1.54943 1.11286 1.59023
1.78581 1.78597 1.78597 1.78482 1.78519 1.78451 1.77998 1.77656 1.38051 0.42452
1.46915 1.46908 1.46929 1.46897 1.46206 1.46163 1.45123 1.13722 0.76054 1.07450
output: output_objs_default_m_10.txt
```

That is the objective vectors evaluated using the test problem. We print them to validate that the obtained outcome is the same as the expected one.

-----

**Note** We added six new problems in `emo_test.c`: `maf1-5`, and `inv-dtlz1`. Edit this block to select a test problem:

```c
if(strcmp(mop_name, "default") == 0) {
        //myMOP_alloc(&mop);
        maf1_alloc(&mop);
        //maf2_alloc(&mop);
        //maf3_alloc(&mop);
        //maf4_alloc(&mop);
        //maf5_alloc(&mop);
        //inv_dtlz1_alloc(&mop);
    }
```

The input matrices of the test problems depends on the problem itself. For testing `maf1-5`, use this command:

```
./emo_test default input/Param_10D_test.cfg input_maf_m_10.txt
```

For testing `inv-dtlz1`, use this command:

```
./emo_test default input/Param_10D_test.cfg input_inv_dtlz1_m_10.txt
```

-----

You can modify `myMOP_alloc()` and `myMOP_eval()` for adding new problems in the framework. After validating that the problems are correctly implemented, we can add them in `benchmark.c` as described below.







## Adding a problem in `benchmark.c`

The framework provides two front-end functions for allocating and deallocating test problems:

```c
// benchmark.c
void EMO_Benchmark_alloc(EMO_MOP *mop, EMO_Param *param, const char *problem)
void EMO_Benchmark_free(EMO_MOP *mop)
```

Roughly speaking:

- `EMO_Benchmark_alloc` maintains a list of available test problems. Also, this function manages the configuration of `mop`.
- `EMO_Benchmark_free` deallocates the memory reserved for the test problems. If your test problem requires additional structures or allocates memory via `malloc` or `calloc`, you can deallocate them through this function. Since our test problems does not require additional structures, we can skip this function.

Let's check `EMO_Benchmark_alloc`. This function defines three types:

```c
// for evaluating contrained problems
typedef void (*Evalc)(EMO_MOP *mop, double *, double *, double *); 

// for evaluating uncontrained problems
typedef void (*Eval) (EMO_MOP *mop, double *, double *);

// for defining the ranges of the decision variables (both contrained and uncontrained problems)
typedef void (*Range)(EMO_MOP *);
```

In the following, we will focus our discussion on unconstrained test problems for brevity. From the previous code, `Eval` can be regarded as a type. In fact, we define a vector `fdic` as follows:

```c
const Eval fdic[] = {
                   EMO_Benchmark_fon1,  EMO_Benchmark_fon2,  EMO_Benchmark_kur, 
                   EMO_Benchmark_lau,   EMO_Benchmark_lis,   EMO_Benchmark_mur,
                   EMO_Benchmark_pol,   EMO_Benchmark_qua, 
                   EMO_Benchmark_ren1,  EMO_Benchmark_ren2,
    				...
                   EMO_WFG_wfg7_minus,  EMO_WFG_wfg8_minus,  EMO_WFG_wfg9_minus
				};
```

Each element of `fdic` follows this header:

```c
void EMO_Benchmark_NAME(EMO_MOP *mop, double *f, double *x)
```

That is, it is the same signature defined by `Eval`. 







**Summary**

In order to add a new problem, edit `benchmark.c` as follows:

- [x] Add the name of the problem in `EMO_Benchmark_list`. I recommend add it on the top:

```c
const char *EMO_Benchmark_list[] = {  
    							"MAF1",       	// new problem
    							"FON1",  "FON2",  "KUR", 
                                ...
                                   };
```

- [x] Add the implementation of the problem:

```c
void EMO_Benchmark_maf1(EMO_MOP *mop, double *f, double *x) { ... }
```

- [x] Add a function to define the range of the decision variables. Notice that you can use the same function for different problems. For instance, `EMO_Benchmark_dtlz_range` is employed for all the DTLZ problems:

```c
void EMO_Benchmark_maf_range(EMO_MOP *mop) {
    int i;

 	for(i = mop->nvar - 1; i > -1; i--) {
   		mop->xmin[i] = 0.0;
    	mop->xmax[i] = 1.0;
  	}
}
```

- [x] Update `fdic`. Add the function of the new problem into the corresponding list. Remember the position of the new problem in the list. I recommend to add it on the top :

```c
  /* Vector of objective function evaluation */
const Eval fdic[] = {
    				EMO_Benchmark_maf1,    // new problem at the beginning
                   	EMO_Benchmark_fon1,  EMO_Benchmark_fon2,  EMO_Benchmark_kur, 
    				...
					};
```

- [x] Update `range_dic`. Again, I recommend to add the range function on the top for preserving the relationship between `fdic` and `range_dic`:

```c
/* Functions that specify the range of variables */
const Range range_dic[] = { 
					EMO_Benchmark_maf_range	// new range function
                 	EMO_Benchmark_fon1_range,  EMO_Benchmark_fon2_range, EMO_Benchmark_kur_range, 
    				...
 				 	};
```

- [x] Update `obj` to determine the number of objectives of the new problem. If the number of objectives is scalable, use `0`:

```c
/* Number of objectives for each test function, 0 means scalable */
const int obj[] = {
    				0,  		// maf1
                   	2, 2, 2,    // fon1, fon2, kur
      				...
  				  };
```

- [x] Update `var` to determine the number of variables of the new problem. Like DTLZ, use `-1` for MaF (the number of variables is determined automatically as described below). Although the number of variables of DTLZ and MaF is technically scalable, you must use `-1`. Remember, the number of variables on these problems is defined as `n = m + k -1`. Although `n` can be scaled, it depends on the number of objectives ( `m`). In that sense, `n` is fixed. For that reason, use `-1`:

```c
/* Number of variables: 0 means scalable, -1 means fixed */
const int var[] = {
    				-1, 
                   	2, 0, 3,    // fon1, fon2, kur
                  };
```

- [x] Update `default_var` to determine the number of variables of the new problem (if the problem is scalable). Otherwise, use `0`. This value is employed for the variable `k` of DTLZ, LAME, and our new MaF benchmark. That variable can be accessed through `mop->npos`. All the MaF test problems use `k=10`, so add this value on the top for each problem:

```c
/* Default number of variables (only for scalable problems): 0 means not applicable */
/* For DTLZ, MaF, and LAME test problems, entries represent the k values for NVAR = NOBJ + k - 1 */
const int default_var[] = { 
    				10, 			// maf1, k=10
                    0, 10, 0,   // fon1, fon2, kur
    				...
                   };
```

That's it. Finally, some important variables:

```c
mop->nobj = nobj; 		// number of objectives
mop->nvar = nvar;		// number of decision variables
mop->ncon = ncon;		// number of constraints
mop->npos = npos;  		// variable k for DTLZ, MaF, and LAME
```

----

**Note** The previous steps also apply for constrained. In that case, the name of the variables contains a `c` at the end. For instance, `fdic` is replaced with `fdicc`, `range_dic` is replaced with `range_dicc`, and so on.

----

**Note** We said that `default_var` holds the value of `k` for DTLZ, MaF, and LAME problems. This snippet determines the number of variables regarding `k`:

```c
/* Default variable values */ 
if(nvar == 0 || var[i] > 0) {
    
    // i is the index of the problem in EMO_Benchmark_list
    
    // since var[i] is -1, nvar is default_var[i], which holds the value of k
    // ie, nvar is equal to k
	nvar = (var[i] <= 0)? default_var[i] : var[i];

    // n = m + k - 1
    if(strncmp(mop->name, "DTLZ", 4) == 0 ||
       strncmp(mop->name, "LAME", 4) == 0 || 
       strncmp(mop->name, "MAF", 3) == 0) /* Fixed number of variables */
        nvar += nobj - 1;

	...
}
```

In the implementation of DTLZ, notice that `k` is computed as follows:

```
c = mop->nvar - mop->nobj + 1;
```















## How to create a population from an input file





## How to evaluate a  problem using an input matrix

The following function is employed to evaluate a population in a test problem:

```c
// common.c
void EMO_Population_evaluate(EMO_Population *pop, EMO_MOP *mop, int start, int size)
```

This block shows how the evaluation is performed on unconstrained problems:

```c
void EMO_Population_evaluate(EMO_Population *pop, EMO_MOP *mop, int start, int size) {
  int i, j, c, o, v, end;

  end = start + size;

  if(mop->ncon == 0) {
    for(i = start; i < end; i++) {
      o = i * mop->nobj;
      v = i * mop->nvar;
      mop->f(mop, pop->obj + o, pop->var + v);
      mop->feval++;
    }
  }
  ...   
}
```

Roughly speaking, we are iterarin

```c
for(i = start; i < end; i++) {
    o = i * mop->nobj;       	// number of objectives
    v = i * mop->nvar;			// number of decision variables
    mop->f(mop, pop->obj + o, pop->var + v);	// actual evaluation
    mop->feval++;								// increment number of evaluations
}
```

Notice that we access to both `obj` and `var` vectors through `pop` as follows:

```c
pop->obj
pop->var
```

In addition, observe that the evaluation is performing through `mop` as follows:

```
mop->f(mop, ..., ...)
```

You may wonder *why does `mop->f()` requires `mop` as a parameter?* As explained below, `f` is a pointer to the actual test problem. All the test problems follow this signature:

```c
void EMO_Benchmark_MOPNAME(EMO_MOP *mop, double *f, double *x);
```

A few examples:

```c
// benchmark.c
void EMO_Benchmark_zdt1(EMO_MOP *mop, double *f, double *x) { ... }
void EMO_Benchmark_dtlz1(EMO_MOP *mop, double *f, double *x) { ... } 
void EMO_Benchmark_ebn(EMO_MOP *mop, double *f, double *x) { ... }
```

All of them require a pointer`mop` for accessing to the attributes of the problem, such as number of variables (`mop->nvar`) and number of objectives (`mop->nobj`).

----

**Note** You can check the code for `mop`, that is an instance of `typedef struct EMO_MOP`:

```c
// common.h
typedef struct EMO_MOP {
  int nvar;            /* number of decision variables  */
  int nobj;            /* number of objective functions */
  int ncon;            /* number of constraints */
  double *xmin, *xmax; /* valid ranges of decision variables */

  void (*f)(struct EMO_MOP *mop, double *, double *);  /* pointer to the evaluation function */

  char *name;          /* name of the problem */
  int coding;          /* representation: real or binary */
  
  ...
      
} EMO_MOP;
```

----

Thus, in order to evaluate a test problem, we must first create an instance of the problem.



## test



```c
// vars
EMO_Population pop;
EMO_Param param;
EMO_MOEA moea;
EMO_MOP mop;

// required
EMO_Param_alloc_from_file(&param, &mop, argv[1], argv[2], argv[3]);

// the allocation of pop depends on the type of moea,
// that's why this function is needed
EMO_MOEA_alloc(&moea, &param, &pop, &mop, argv[1]); 

// init some flags
EMO_Stop_start(param.stop);

// create a random init pop
// evals initial pop
// it calls EMO_Population_evaluate
EMO_Population_init(&pop, param.rand, &mop);

// copy population from file
for(i = 0; i < pop->size; i++) {
    k = i * mop->nvar;

    for(j = 0; j < mop->nvar; j++)
        pop->var[k + j] = data[k + j];		// revisar esta linea
}

// evaluate
// use start=0, size=pop->size
EMO_Population_evaluate(pop, mop, 0, pop->size);

// save objs
// write file
int rows = pop->mu;
int cols = mop->nobj;
char output = sprintf("output_%s.pof", argv[3]);
EMO_File_write(pop->obj, NULL, rows, cols, output, "%f ", 0);
printf("output: %s", output);

// deallocate
EMO_MOEA_free(&moea);
EMO_Param_free(&param);  /* free MOP */
EMO_Population_free(&pop);
```

```
make emo_test
```



```
./emo_test MOMBI3 input/Param_02D.cfg zdt1 1
```



nueva firma

```
./emo_test mop params input output
```

```
./emo_test zdt1 input/Param_02D.cfg input_zdt1_m_2.txt output_zdt1_m_2.txt
```









## How to add a new problem?

In order to define a problem, the framework provides access to two functions for allocating and removing problems:

```c
// benchmark.h
void EMO_Benchmark_alloc(EMO_MOP *mop, EMO_Param *param, const char *problem);
void EMO_Benchmark_free(EMO_MOP *mop);
```

`EMO_Benchmark_alloc` stores the available problems in a vector of functions:

```c
void EMO_Benchmark_alloc(EMO_MOP *mop, EMO_Param *param, const char *problem) {  

  typedef void (*Evalc)(EMO_MOP *mop, double *, double *, double *); 
  typedef void (*Eval) (EMO_MOP *mop, double *, double *);
  typedef void (*Range)(EMO_MOP *);
  int i, d, nvar, nobj, ncon, npos;
  char *str;

  /* Vector of objective function evaluation */
  const Eval fdic[] = {
                   /* ... */ 
                   EMO_Benchmark_dtlz1, EMO_Benchmark_dtlz2, EMO_Benchmark_dtlz3, 
                   EMO_Benchmark_dtlz4, EMO_Benchmark_dtlz5, EMO_Benchmark_dtlz6, 
                   EMO_Benchmark_dtlz7,
                   EMO_Benchmark_dtlz2_convex, EMO_Benchmark_dtlz3_convex,
                   EMO_Benchmark_dtlz4_convex,
                   EMO_Benchmark_dtlz1_minus, EMO_Benchmark_dtlz2_minus, EMO_Benchmark_dtlz3_minus, 
                   EMO_Benchmark_dtlz4_minus, EMO_Benchmark_dtlz5_minus, EMO_Benchmark_dtlz6_minus, 
                   EMO_Benchmark_dtlz7_minus,
                   /* ... */
                  }; 
                   
	/* more code */
}
```

The test functions follow a similar pattern. Take a look at DTLZ1:

```c
/* Test problem DTLZ1 (Deb-Thiele-Laumanns-Zitzler)

   Real variables = mop->nobj + k - 1, k = 5 
   Bin variables = 0
   Objectives = (scalable) 
   constraints = 0
   Values of xi in [0,1]
   PFTrue is linear, separable, multimodal
   POS is at x_{M}^{*} = 0, sum_{m=1}^{M} fm = 0.5
*/
void EMO_Benchmark_dtlz1(EMO_MOP *mop, double *f, double *x) {
  double g, v;
  int i, j, t, c;

  g = 0.0;
  c = mop->nvar - mop->nobj + 1;

  for(i = mop->nvar - c; i < mop->nvar; i++)
    g += pow(x[i] - 0.5, 2.0) - cos(20.0 * PI * (x[i] - 0.5));

  g = 100.0 * (c + g);
  v = 0.5 * (1.0 + g);

  for(i = 0; i < mop->nobj; i++) {
    f[i] = v;
    t = mop->nobj - i - 1; 

    for(j = 0; j < t; j++) 
      f[i] *= x[j];

    if(t < mop->nobj - 1) 
      f[i] *= (1.0 - x[t]);
  }
}
```

So basically, we have access to an instance of `EMO_MOP` to get the properties of the problem. `EMO_MOP` is an structure defined in `common.h`:

```c
// common.h

typedef struct EMO_MOP {
  int nvar;            /* number of decision variables  */
  int nobj;            /* number of objective functions */
  int ncon;            /* number of constraints */

  int npos;            /* number of position related parameters (WFG test problems) */
  double gamma;        /* Lame super-spheres */

  double *xmin, *xmax; /* valid ranges of decision variables */

  void (*f)(struct EMO_MOP *mop, double *, double *);  /* pointer to the evaluation function */
  void (*fc)(struct EMO_MOP *mop, double *, double *g, double *x); /* pointer to the evaluation function with constraints */

  char *name;          /* name of the problem */
  int coding;          /* representation: real or binary */
  int L;               /* chromosome length (binary representation) */
  unsigned long feval; /* number of objective function evaluations */

                       /* Auxiliary variables */
  double *t;           /* theta for DTLZ5, DTLZ6 and transition vector for WFG test problems */
  double *x;           /* underlying parameters */
  double *h;           /* shape */
  double *S;           /* constant */
  double *A;           /* constant */
  double *y;           /* temporary vector */
  double *w;           /* weight vector */
  double (*g)(double );  /* g function in test problems based on Lame Superspheres */
} EMO_MOP;
```

In practice, you will use an instance of `EMO_MOP` for accessing to the number of objectives and number of variables as follows:

```c
int nvar = mop->nvar;    // number of variables
int nobj = mop->nobj;    // number of objectives
```

In addition, you can also access to the minimum and maximum values of each objective function:

```c
double min_i_obj = mop->xmin[i];
double max_i_obj = mop->xmax[i];
```

We also have access to the design vector to being evaluated (`x`), and the objective vector (`f`). Notice that the result will be stored in `f`, meaning that this function modifies its input.

In order to define a new test function, you need to proceed as follows:

- Create a new function to evaluate the problem itself.
- Create a new function to define the range of the objectives of the problem.
- Configure the new problem (determine if the problem is scalable, define its range, constraints, and so on).

Here, we are going to add a new test problem called `maf1` [ref]. This problem is scalable with respect to the number of variables and objectives, and the range of each objective is `[0, 1]` and it has no constraints. In the following, we describe how to add this problem in the benchmark.



**Create a new function to evaluate the problem**

First, we need to create the new function in `benchmark.c`. It should have this header:

```c
void EMO_Benchmark_FUNCTION_NAME(EMO_MOP *mop, double *f, double *x)
```

This is the function of the new problem:

```c
/* Test problem MaF1

   Real variables = mop->nobj + k - 1, k = 10 
   Bin variables = 0
   Objectives = (scalable) 
   constraints = 0
   Values of xi in [0,1]
*/
void EMO_Benchmark_maf1(EMO_MOP *mop, double *f, double *x) {
  int i;

  for(i = 0; i < mop->nobj; i++)
    f[i] = i;  // dummy update
    
}
```



**Create a new function to define the range of the objectives**

Then, we need to define a function to establish the range of each objective function. It must have this header:

```c
void EMO_Benchmark_dtlz_range(EMO_MOP *mop)
```

In this example, the function for `maf1` is as follows:

```c
void EMO_Benchmark_maf_range(EMO_MOP *mop) {
  int i;

  for(i = mop->nvar - 1; i > -1; i--) {
    mop->xmin[i] = 0.0;
    mop->xmax[i] = 1.0;
  }
}
```

Note that we used `maf` rather than `maf1`. Like `dtlz`, the range of all objectives is the same, `[0, 1]`, regardless of the problem.







**Configure the new problem**

This is the hardest part. Here, we need to:

- Add the new problem name into a vector of available problems called `EMO_Benchmark_list`.
- Add the new problem function into a vector of available problems called `fdic`.
- Add the range function into a vector called `range_dic`.
- Determine whether the problem is scalable. If not, define the number of objectives and variables. To do so, we must modify three vectors, `obj`, `var`, and `default_var`.

All these changes are made in `EMO_Benchmark_alloc`. This function has this header:

```c
void EMO_Benchmark_alloc(EMO_MOP *mop, EMO_Param *param, const char *problem)
```

Before adding a new problem into this function, it is worth noting that **the order of the problem in the configuration vectors is important**. For instance, if the new problem is added into `fdic[i]`, then the corresponding entries of the other vectors should match `i`.

Let's modify the mentioned vectors to add `maf1`. For simplicity, we add `maf1` in the first entry of the configuration vectors. First, add the name of the problem  (use uppercase):

```c
/* Dictionary of test functions (global variables) */
const char *EMO_Benchmark_list[] = { 
    					"MAF1",
    					// other mops
}
```

Then, add the problem function, range function, and configuration of the problem:

```c
void EMO_Benchmark_alloc(EMO_MOP *mop, EMO_Param *param, const char *problem) {  

	/* Vector of objective function evaluation */
  	const Eval fdic[] = {
                   	EMO_Benchmark_maf1,
        			// other mops
    }
    
    /* Functions that specify the range of variables */
  	const Range range_dic[] = { 
                   	EMO_Benchmark_maf_range,  
        			// other range functions
	}
    
    /* Number of objectives for each test function, 0 means scalable */
  	const int obj[] = {  
                   	0,    // maf1
        			// other mops
    }
    
    /* Number of variables: 0 means scalable, -1 means fixed */
  	const int var[] = {  
                   	0,    // maf1
        			// other mops
        
    }
    
    /* Default number of variables (only for scalable problems): 0 means not applicable */
  	/* For DTLZ and LAME test problems, entries represent the k values for NVAR = NOBJ + k - 1 */
    /* For MAF test problems, entries represent the k values for NVAR = NOBJ + k - 1 */
  	const int default_var[] = { 
                 	10,   // maf1
        			// other mops
    }      
```

----

Additionally, we must determine the default number of variables (objectives) when the number of variables (objectives) is scalable. To this end, we need to modify this block:

```c
/* Find the function's name in dictionary */
i = EMO_Dictionary_find(EMO_Benchmark_list, mop->name);

if(i != -1) {
mop->f = fdic[i];

/* Default objective values */
if(nobj == 0 || obj[i] > 0) {
	nobj = (obj[i] <= 0)? 3 : obj[i]; /* Scalable number of objectives */

	if(nobj != obj[i] && nobj != 0 && obj[i] > 0) /* Verify that is scalable w.r.t. the number of 	     objectives */
    	EMO_Debug_printf(param->dbg, "Benchmark: warning, function %s is not scalable with respect to the number of objectives, using the default value: %d.", mop->name, nobj);
}

/* Default variable values */ 
if(nvar == 0 || var[i] > 0) {
  nvar = (var[i] <= 0)? default_var[i] : var[i]; /* Scalable number of variables */

  if(strncmp(mop->name, "DTLZ", 4) == 0 || strncmp(mop->name, "LAME", 4) == 0) /* Fixed number of variables */
    nvar += nobj - 1;

  if(strncmp(mop->name, "WFG", 3) == 0) /* Fixed number of variables in WFG test suite */
    nvar = 24;

  if(nvar != 0 && var[i] > 0)  /* Verify that is scalable w.r.t. the number of variables */
    EMO_Debug_printf(param->dbg, "Benchmark: warning, function %s is not scalable with respect to the number of variables, using the default value: %d.", mop->name, nvar);
}

if(nvar != 0 && var[i] <= 0) {
  if(strncmp(mop->name, "DTLZ", 4) == 0) { /* Fixed number of variables */
    nvar = default_var[i] + nobj - 1; 
    EMO_Debug_printf(param->dbg, "Benchmark: warning, fixed number of variables (%d) in %s test problem.", nvar, mop->name);
  }

  /* Number of position-related parameters in WFG test suite */
  if(strncmp(mop->name, "WFG", 3) == 0 && npos == 0) {
    npos = (nobj == 2)? 4 : 2 * nobj - 2;
    EMO_Debug_printf(param->dbg, "Benchmark: warning, number of position parameters in %s is not specified, using the default value %d.", mop->name, npos);
  }
}
}

```

Let's explain each part separately. First, we look for the name of the problem in the list:

```c
void EMO_Benchmark_alloc(EMO_MOP *mop, EMO_Param *param, const char *problem) {  
	
  	/* Function's name is converted to uppercase */
  	mop->name = EMO_toupper(problem);
    
	/* Find the function's name in dictionary */
	i = EMO_Dictionary_find(EMO_Benchmark_list, mop->name);
 
    // ...
}
```

As it can be seen, `i` holds the index of `mop->name` in the list of problems. Use this index for accessing the configuration vectors (`fdic`, `range_dic`, `obj`, `var`, `default_var`). If `i` equals`-1`, then the problem is not in the list. Next, the number of objectives is defined:

```c
//* Initialization */
nvar = nobj = ncon = npos = mop->gamma = 0;

/* Get the number of objectives from the configuration file */ 
if(!EMO_Param_get_int(param, &nobj, "nobj")) {
    printf("Error, nobj is not defined in the configuration file.\n");
    exit(1);
}

// CHECAR QUE HACE ESTA PARTE

if(i != -1) {
    
    /* Get the problem function */
    mop->f = fdic[i];

    /* Default objective values */
    if(nobj == 0 || obj[i] > 0) {
    	nobj = (obj[i] <= 0)? 3 : obj[i]; /* Scalable number of objectives */

    /* Verify that is scalable w.r.t. the number of objectives */
    if(nobj != obj[i] && nobj != 0 && obj[i] > 0) 
        EMO_Debug_printf(param->dbg, 
                         "Benchmark: warning, function %s is not scalable \
                         with respect to the number of objectives, \
                         using the default value: %d.", 
                         mop->name, nobj);
    }
    
}
```

Then, define the number of variables according to `nobj`.

```c
if(i != -1) {
    
    mop->f = fdic[i];

    /* Default objective values */
    /* ... see above ...*/

    // CHECAR QUE HACE
    
    /* Default variable values */ 
    if(nvar == 0 || var[i] > 0) {
        
        /* Scalable number of variables */
        nvar = (var[i] <= 0)? default_var[i] : var[i]; 

        /* Fixed number of variables */
        if(strncmp(mop->name, "DTLZ", 4) == 0) 
            nvar += nobj - 1;

        /* Verify that is scalable w.r.t. the number of variables */
        if(nvar != 0 && var[i] > 0)  
            EMO_Debug_printf(param->dbg, 
                             "Benchmark: warning, function %s is not scalable \
                             with respect to the number of variables, \
                             using the default value: %d.", 
                             mop->name, nvar);
    }

    if(nvar != 0 && var[i] <= 0) {
        
        /* Fixed number of variables */
        if(strncmp(mop->name, "DTLZ", 4) == 0) { 
            nvar = default_var[i] + nobj - 1; 
            EMO_Debug_printf(param->dbg, 
                             "Benchmark: warning, fixed number of variables (%d) \
                             in %s test problem.", 
                             nvar, mop->name);
        }
    }
}
```

We must modify the previous block as follows:

```c
if(i != -1) {
    
    mop->f = fdic[i];

    /* Default objective values */
    /* ... see above ...*/

    // CHECAR QUE HACE
    
    /* Default variable values */ 
    if(nvar == 0 || var[i] > 0) {
        
        /* Scalable number of variables */
        /* Get param k for DTLZ, MAF*/
        nvar = (var[i] <= 0)? default_var[i] : var[i]; 

        /* Fixed number of variables */
        if(strncmp(mop->name, "DTLZ", 4) == 0) 
            nvar += nobj - 1;
        
        
        // ADD THIS BLOCK FOR MAF1-6
        /* Fixed number of variables */
        if(strncmp(mop->name, "MAF", 3) == 0) 
            nvar += nobj - 1;

        /* Verify that is scalable w.r.t. the number of variables */
        if(nvar != 0 && var[i] > 0)  
            EMO_Debug_printf(param->dbg, 
                             "Benchmark: warning, function %s is not scalable \
                             with respect to the number of variables, \
                             using the default value: %d.", 
                             mop->name, nvar);
    }

    if(nvar != 0 && var[i] <= 0) {
        
        /* Fixed number of variables */
        if(strncmp(mop->name, "DTLZ", 4) == 0) { 
            nvar = default_var[i] + nobj - 1; 
            EMO_Debug_printf(param->dbg, 
                             "Benchmark: warning, fixed number of variables (%d) \
                             in %s test problem.", 
                             nvar, mop->name);
        }
        
        // ADD THIS BLOCK
        /* Fixed number of variables */
        if(strncmp(mop->name, "MAF", 3) == 0) { 
            nvar = default_var[i] + nobj - 1; 
            EMO_Debug_printf(param->dbg, 
                             "Benchmark: warning, fixed number of variables (%d) \
                             in %s test problem.", 
                             nvar, mop->name);
        }
    }
}
```

----

The new problem `MAF1` is now available in the benchmark. Now, let's evaluate it.



## How to test a new problem?

In this section, we describe how to evaluate a problem. Specifically, we compare the outcome of the implementation in C and Python of the same problem to validate the implementation. Look at this function:

```c
void EMO_Population_evaluate(EMO_Population *pop, EMO_MOP *mop, int start, int size) {
    int i, j, c, o, v, end;

    end = start + size;

    if(mop->ncon == 0) {
        for(i = start; i < end; i++) {
            o = i * mop->nobj;   						// pointer aritmetic
            v = i * mop->nvar;							// pointer aritmetic
            mop->f(mop, pop->obj + o, pop->var + v);	// evaluate i-th individual
            mop->feval++;                               // increment the number of function evals
        }
    }
    else {
        /* constrained problem here*/
    }
}
```

This function is employed to evaluate a population in a unconstrained problem. As it can be seen, we need to call `mop->f` to invoke the problem function. We will create a new file called `eval_mop.c` to evaluate a problem. Copy `emo_moea.c` as follows:

```
cd emo_project/demo
cp emo_moea.c eval_mop.c
```

The executable `eval_mop` will be invoked as follows:

```
./eval_mop mop_name input_filepath
```

where:

- `mop_name` is the name of the problem.
- `input_filepath` is the population to be evaluated in the problem.

**Example**

```
./eval_mop dtlz1 input.pos
```





------





**Continua con la lectura de archivo con `EMO_File_read`** Usa `common.c` como ejemplo

```c
// common.c

/* Initialization from .pos and .pof files */
int EMO_Population_init_from_file(EMO_Population *pop, EMO_MOP *mop, const char *prefix, int start) {
  int size1, size2, i, j;
  char *str;

  str = (char *) malloc(sizeof(char) * (strlen(prefix) + 5));

  sprintf(str, "%s.pos", prefix);
  printf("Reading population from file %s.\n", str);
  size1 = pop->mu;
  EMO_File_read(pop->var, &size1, &mop->nvar, str, start);
  printf("var: mu %d, size1 read %d, var %d, obj %d\n", pop->mu, size1, mop->nvar, mop->nobj);

```

```c



EMO_File_read(pop->var, &size1, &mop->nvar, str, start);

// header
// double *EMO_File_read(double *data,  // pointer to store the content of the file
						int *row, 
						int *col,
						const char *str, 
						int start);

// read file
int rows = 100;
int cols = 10;
char filename = "input.txt";
int start = 0;
double *data = EMO_File_read(NULL, &rows, &cols, filename, start);
 
```







Modify `emo_project/demo/Makefile` to add `eval_mop.c` into the compilation procedure:

```makefile
# Makefile
OBJS_NOMPI	= emo_moea emo_indicator emo_stat emo_ndset emo_ndsort eval_mop 

# ...

# add this rule
eval_mop:		eval_mop.c
			$(CC) $(CFLAGS) eval_mop.c -o eval_mop -lemo -lm
```









## TODO

- [ ] Cómo crear  un nuevo mop?
- [ ] Como agregar una nueva fev?
- [ ] `eval_mop` recibe `input_filepath`, pero en el codigo esta fijo
- [ ] Modificar `eval_mop` para que use esta firma

```
./eval_mop dtlz1 input.pos output.pos
```





- [ ] 
- [ ] 
- [ ] Terminar `eval_mop.c` (usaremos este script para validar los mops con mi codigo python, ie para garantizar que devuelvan la misma salida)
- [ ] Crear repo para `maf` en gitlab
- [ ] Averigar como integrarlos en `emo_project`





**Implementación**

- [x] Implementar inv-DTLZ1
- [x] Implementar MaF1
- [x] Implementar MaF2
- [x] Implementar MaF3
- [x] Implementar MaF4
- [x] Implementar MaF5
- [ ] Implementar VADS
- [ ] Implementar IPBI
- [ ] Implementar las funciones de escalarizacion exclusivas de mombi
- [x] Agregar los mops al benchmark de emo_project
- [ ] Probar la integracion y validar los mops para m=3,5,8,10 (ahora el valor de n_vars dependera de m_objs y no sera fijo como en emo_test.c)



**Para el experimento de mombi**

Vamos a probar varios casos

- `a = (ws, te, asf, pbi, ipbi, vads)`
- `b = (ipbi*, pbi-5)`
- `c = (ws, ewc, wpo, wn, che, asf, aasf, pbi)` creo que solo me faltaba implementar una de ellas, pero igual hay que validarlas





**Validación**

- [ ] Crear un script para validar los problemas usados en nuestro paper (DTLZ1/2/3, inv-DTLZ1, MaF1-5) para validar que ambas implementaciones, python y c, devuelvan la misma salida.
- [ ] Crear un script para validar las fevs usados en nuestro paper (ws, te, asf, pbi, ipbi, vads) para validar que ambas implementaciones, python y c, devuelvan la misma salida.









- [ ] `create_random_pop.py`
- [x] Crear archivos de configuracion `Param_*D_test.cfg`
- [x] Crear `create_input_matrices.py`para generar las matrices de entrada
- [ ] Agregar copia de `rocket` en `emo_project`
- [ ] Modificar `emo_test.py` para que cargue la copia de `rocket`
- [x] Modificar `emo_test.py` para que tome los mismos parametros que `emo_test.c`
- [ ] Crear `validate_mops.sh` para evaluar `emo_test.c` y `emo_test.py`, algo asi:



```bash
#!/usr/bin/env bash

for m_objs in 3 5 8 10 ; do
	
	for mop_name in maf1 maf2 maf3 maf4 maf5 inv_dtlz1 ; do
	
		# evaluate implementations
		./emo_test "$mop_name" input/Param_"$m_objs"D_test.cfg input_"$mop_name"_m_"$m_objs".txt
		python3 emo_test.py "$mop_name" input_"$mop_name"_m_"$m_objs".txt
 
 		# compare results
 		colordiff output_objs_"$mop_name"_m_"$m_objs".txt output_objs_"$mop_name"_m_"$m_objs"_python.txt
	
	done

done
```

Una mejor opcion es comaprar ambdas matrices desde python usanso asclose o algo asi